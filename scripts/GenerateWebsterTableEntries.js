var fs = require('fs');
var sqlite3 = require('sqlite3').verbose();
var websterJson = require('./webster-dictionary.json');
var db = new sqlite3.Database('../app/src/main/assets/databases/scriptures_v1.1.db');

var generateInsertSql = () => {
    let sql_search = "SELECT COUNT(rowid) AS count FROM fts_paragraphs WHERE fts_paragraphs MATCH ?;";
    return new Promise((resolve, reject) => {
        for(let t in websterJson) {
            if (websterJson.hasOwnProperty(t)) {

                db.get(sql_search, [t], (err, row) => {
                    if (row && row['count'] > 1) {
                        console.log((queries.length + 1) + ': ' + t);
                        let formattedText = websterJson[t]
                            .replace(/'/g, "''") // Escape apostrophes
                            .replace(/(?<=\s)(\d{1}?\.{1}? )(?=[A-Za-z]{1,})/g, "\n\n$1") // Spacing the definitions
                            .trim();
                        let sql = `('${t}', '${formattedText}'),`;
                        //console.log(sql);
                        queries.push(sql);

                        if (queries.length >= 5973) {
                            return resolve();
                        }
                    }
                });
            }
        }
    })
}

let queries = [
    "BEGIN TRANSACTION;",
    "INSERT INTO webster (term, definition) VALUES"
];

generateInsertSql()
.then(() => {
    db.close();
    queries.push("COMMIT;");

    console.log('length: ' + queries.length);

    fs.writeFile("./webster_definitions.sql", queries.join('\n'), (err) => {
        if(err) return console.log(err);
        console.log("The file was saved!");
    });
})
