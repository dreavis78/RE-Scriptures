var sqlite3 = require('sqlite3').verbose();
var cheerio = require('/usr/local/lib/node_modules/cheerio');
var fs = require('fs');
var db = new sqlite3.Database('./scriptures_v1.2.db');
//var db = new sqlite3.Database('../app/src/main/assets/databases/scriptures_v1.1.3.db');

var sql = "SELECT volume_id, book_id, chapter, content " +
          "FROM chapters " +
          "ORDER BY chapter_id";
db.all(sql, [], function(err, rows) {
	if (err) console.log(err);

    var numRows = rows.length,
	    inserts = [
		"BEGIN TRANSACTION;",
        "REPLACE INTO paragraphs (volume_id, book_id, chapter_id, position, paragraph) VALUES "
	];

	for(var row in rows) {
		var chapter = rows[row],
			$ = cheerio.load(chapter.content, {
			normalizeWhiteSpace: true,
			xmlMode: true
		});


        var specialBooks = ["title", "version", "assembly1835", "section_corr", "appendixa", "gov_principles"];

        if (specialBooks.indexOf(chapter.book_id) !== -1) {
            $.root().each(function(i, elem) {
                var paragraphNum = i,
                text = $(this).text();

                console.log(`Processing ${chapter.book_id} ${chapter.chapter} with ${paragraphNum} paragraphs`);
                if (!text) return false;

                text = text.replace(/(?:\r\n|\r|\n)/g, ' ').replace(/\t/g, " ").replace(/\s+/g, " ").replace(/'/g, "''").trim();
                var insert = `('${chapter.volume_id}','${chapter.book_id}','${chapter.chapter}',${paragraphNum}, '${text}'),`;
                inserts.push(insert);
            });
        } else {
            var query = $('li[id]');

            // Some intro and foreword pages use <p> tags instead
            if (query.length === 0) {
                query = $('ol > p, blockquote > p, div > p');
            }

            console.log(`Processing ${chapter.book_id} ${chapter.chapter} with ${query.length} paragraphs`);

            query.each(function(i, elem) {
                var li = $(this),
                    paragraphNum = li.attr('id') || (i + 1),
                    text = li.text();

                if (!text) return false;

                text = text.replace(/(?:\r\n|\r|\n)/g, ' ').replace(/\t/g, " ").replace(/\s+/g, " ").replace(/'/g, "''").trim();
                var insert = `('${chapter.volume_id}','${chapter.book_id}','${chapter.chapter}',${paragraphNum}, '${text}'),`;
                inserts.push(insert);
            });

        }
	}

    inserts[inserts.length-1] = inserts[inserts.length-1].replace(/\),$/, ");");
	inserts.push("COMMIT;");

	fs.writeFile("./paragraphs.sql", inserts.join('\n'), function(err) {
		if(err) return console.log(err);

		console.log("The file was saved!");
	});
});

db.close();
