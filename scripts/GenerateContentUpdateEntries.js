var promise = require('bluebird');
var cheerio = require('cheerio');
var fs = require('fs');
var _ = require('lodash');
var uuid = require('uuid/v4');
var he = require('he');

console.log(process.argv[2]);

if (!process.argv[2]) {
    throw new Error('no directory specified');
}

let dir = process.argv[2];
if (!fs.existsSync(dir)) {
    throw new Error('directory does not exist');
}

//let dir = './0003_bofm',
    let workId = dir.split('_')[1],
    queries = [
      "BEGIN TRANSACTION;",
      "REPLACE INTO chapters (volume_id, book_id, chapter, content) VALUES"
    ];
    // template;
    console.log("workId: " + workId);

var titles = [
    {"id": "title", "shorthand": "Title Page"},
    {"id": "ocforeword", "shorthand": "Foreword"},
    {"id": "occanonization", "shorthand": "Canonization"},
    {"id": "ocpreface", "shorthand": "Preface"},
    {"id": "genesis", "shorthand": "Genesis"},
    {"id": "exodus", "shorthand": "Exodus"},
    {"id": "leviticus", "shorthand": "Leviticus"},
    {"id": "numbers", "shorthand": "Numbers"},
    {"id": "deuteronomy", "shorthand": "Deuteronomy"},
    {"id": "joshua", "shorthand": "Joshua"},
    {"id": "judges", "shorthand": "Judges"},
    {"id": "ruth", "shorthand": "Ruth"},
    {"id": "1samuel", "shorthand": "1 Samuel"},
    {"id": "2samuel", "shorthand": "2 Samuel"},
    {"id": "1kings", "shorthand": "1 Kings"},
    {"id": "2kings", "shorthand": "2 Kings"},
    {"id": "1chronicles", "shorthand": "1 Chronicles"},
    {"id": "2chronicles", "shorthand": "2 Chronicles"},
    {"id": "ezra", "shorthand": "Ezra"},
    {"id": "nehemiah", "shorthand": "Nehemiah"},
    {"id": "esther", "shorthand": "Esther"},
    {"id": "job", "shorthand": "Job"},
    {"id": "psalm", "shorthand": "Psalms"},
    {"id": "proverbs", "shorthand": "Proverbs"},
    {"id": "ecclesiastes", "shorthand": "Ecclesiastes"},
    {"id": "isaiah", "shorthand": "Isaiah"},
    {"id": "jeremiah", "shorthand": "Jeremiah"},
    {"id": "lamentations", "shorthand": "Lamentations"},
    {"id": "ezekiel", "shorthand": "Ezekiel"},
    {"id": "daniel", "shorthand": "Daniel"},
    {"id": "hosea", "shorthand": "Hosea"},
    {"id": "joel", "shorthand": "Joel"},
    {"id": "amos", "shorthand": "Amos"},
    {"id": "obadiah", "shorthand": "Obadiah"},
    {"id": "jonah", "shorthand": "Jonah"},
    {"id": "micah", "shorthand": "Micah"},
    {"id": "nahum", "shorthand": "Nahum"},
    {"id": "habakkuk", "shorthand": "Habakkuk"},
    {"id": "zephaniah", "shorthand": "Zephaniah"},
    {"id": "haggai", "shorthand": "Haggai"},
    {"id": "zechariah", "shorthand": "Zechariah"},
    {"id": "malachi", "shorthand": "Malachi"},

    {"id": "ncforeword", "shorthand": "Foreword"},
    {"id": "nccanonization", "shorthand": "Canonization"},
    {"id": "ntpreface", "shorthand": "Preface"},
    {"id": "matthew", "shorthand": "Matthew"},
    {"id": "mark", "shorthand": "Mark"},
    {"id": "luke", "shorthand": "Luke"},
    {"id": "john", "shorthand": "John"},
    {"id": "acts", "shorthand": "Acts of The Apostles"},
    {"id": "romans", "shorthand": "Romans"},
    {"id": "1corinthians", "shorthand": "1 Corinthians"},
    {"id": "2corinthians", "shorthand": "2 Corinthians"},
    {"id": "galatians", "shorthand": "Galatians"},
    {"id": "ephesians", "shorthand": "Ephesians"},
    {"id": "philippians", "shorthand": "Philippians"},
    {"id": "colossians", "shorthand": "Colossians"},
    {"id": "1thessalonians", "shorthand": "1 Thessalonians"},
    {"id": "2thessalonians", "shorthand": "2 Thessalonians"},
    {"id": "1timothy", "shorthand": "1 Timothy"},
    {"id": "2timothy", "shorthand": "2 Timothy"},
    {"id": "titus", "shorthand": "Titus"},
    {"id": "philemon", "shorthand": "Philemon"},
    {"id": "hebrews", "shorthand": "Hebrews"},
    {"id": "1jacob", "shorthand": "Jacob"},
    {"id": "1peter", "shorthand": "1 Peter"},
    {"id": "2peter", "shorthand": "2 Peter"},
    {"id": "1john", "shorthand": "1 John"},
    {"id": "2john", "shorthand": "2 John"},
    {"id": "3john", "shorthand": "3 John"},
    {"id": "judas", "shorthand": "Judas"},
    {"id": "revelation", "shorthand": "Revelation"},

    {"id": "bompreface", "shorthand": "Preface"},
    {"id": "bomintro", "shorthand": "Introduction"},
    {"id": "title", "shorthand": "Dedication"},
    {"id": "tow", "shorthand": "The Testimonies of Witnesses"},
    {"id": "1nephi", "shorthand": "1 Nephi"},
    {"id": "2nephi", "shorthand": "2 Nephi"},
    {"id": "jacob", "shorthand": "Jacob"},
    {"id": "enos", "shorthand": "Enos"},
    {"id": "jarom", "shorthand": "Jarom"},
    {"id": "omni", "shorthand": "Omni"},
    {"id": "words", "shorthand": "Words of Mormon"},
    {"id": "mosiah", "shorthand": "Mosiah"},
    {"id": "alma", "shorthand": "Alma"},
    {"id": "helaman", "shorthand": "Helaman"},
    {"id": "3nephi", "shorthand": "3 Nephi"},
    {"id": "4nephi", "shorthand": "4 Nephi"},
    {"id": "mormon", "shorthand": "Mormon"},
    {"id": "ether", "shorthand": "Ether"},
    {"id": "moroni", "shorthand": "Moroni"},

    {"id": "tcforeword", "shorthand": "Foreword"},
    {"id": "tccanonization", "shorthand": "Canonization"},
    {"id": "tcpreface", "shorthand": "Preface"},
    {"id": "tcintro", "shorthand": "Introduction"},
    {"id": "epigraph", "shorthand": "Epigraph"},
    {"id": "section", "shorthand": "Sections"},
    {"id": "jshistory", "shorthand": "Joseph Smith History"},
    {"id": "lecture", "shorthand": "Lectures on Faith"},
    {"id": "abraham", "shorthand": "The Book of Abraham"},
    {"id": "toj", "shorthand": "The Testimony of John"},
    {"id": "appendix", "shorthand": "Appendix"},
    {"id": "glossary", "shorthand": "Glossary of Terms"},

    {"id": "podcast", "shorthand": "Denver Snuffer Podcast"},
    {"id": "fyom", "shorthand": "Forty Years in Mormonism"},
    {"id": "gethsemane", "shorthand": "Gethsemane"},
    {"id": "fellowship-diversity", "shorthand": "Fellowship Diversity"},
    {"id": "who_is_christ", "shorthand": "Who is Christ?"},
    {"id": "contradicting_joseph", "shorthand": "Contradicting Joseph"},
    {"id": "faith", "shorthand": "Faith"},
    {"id": "repentance", "shorthand": "Repentance"},
    {"id": "baptism", "shorthand": "Baptism"},
    {"id": "baptism_of_fire", "shorthand": "Baptism of Fire"},
    {"id": "embracing_truth", "shorthand": "Embracing Truth"},
    {"id": "tithing", "shorthand": "Tithing"},
    {"id": "sacrament", "shorthand": "Sacrament"},
    {"id": "resurrection_morning", "shorthand": "Resurrection Morning"},
    {"id": "our_dispensation", "shorthand": "Our Dispensation"},
    {"id": "the_heavens", "shorthand": "The Heavens"},
    {"id": "jacobs_ladder", "shorthand": "Jacob's Ladder"},
    {"id": "the_remnant", "shorthand": "The Remnant"},
    {"id": "prayer_1", "shorthand": "Prayer, Part 1"},
    {"id": "prayer_2", "shorthand": "Prayer, Part 2"},
    {"id": "angels_1", "shorthand": "Angels, Part 1"},
    {"id": "angels_2", "shorthand": "Angels, Part 2"},
    {"id": "angels_3", "shorthand": "Angels, Part 3"},
    {"id": "new_jerusalem", "shorthand": "New Jerusalem"},
    {"id": "growing_zion", "shorthand": "Growing Zion"},
    {"id": "visions", "shorthand": "Visions"},
    {"id": "sacrifice", "shorthand": "Sacrifice"},
    {"id": "defending_zion", "shorthand": "Defending Zion"},
    {"id": "generation", "shorthand": "Generation"},
    {"id": "adam-ondi-ahman", "shorthand": "Adam-ondi-Ahman"},
    {"id": "patriarchal_blessings", "shorthand": "Patriarchal Blessings"},
    {"id": "how_the_restoration_has_fallen", "shorthand": "How the Restoration Has Fallen!"},
    {"id": "priestcraft", "shorthand": "Priestcraft"},
    {"id": "temple_1", "shorthand": "Temple, Part 1"},
    {"id": "temple_2", "shorthand": "Temple, Part 2"},
    {"id": "dances_with_wolves", "shorthand": "Dances with Wolves"},
    {"id": "temple_3", "shorthand": "Temple, Part 3"},
    {"id": "temple_4", "shorthand": "Temple, Part 4"},
    {"id": "temple_5", "shorthand": "Temple, Part 5"},
    {"id": "temple_6", "shorthand": "Temple, Part 6"},
    {"id": "babylon", "shorthand": "Babylon"},
    {"id": "interpreting_scripture", "shorthand": "Interpreting Scripture"},
    {"id": "the_journey", "shorthand": "The Journey"},
    {"id": "cycles_of_creation", "shorthand": "Cycles of Creation"},
    {"id": "gathering_truth", "shorthand": "Gathering Truth"},
    {"id": "breaking_the_chains", "shorthand": "Breaking the Chains"},
    {"id": "denying_the_power", "shorthand": "Denying the Power"},
    {"id": "charity_1", "shorthand": "Charity, Part 1"},
    {"id": "charity_2", "shorthand": "Charity, Part 2"},
    {"id": "prophecy", "shorthand": "Prophecy"},
    {"id": "ordinances", "shorthand": "Ordinances"},
    {"id": "celebrating_christ", "shorthand": "Celebrating Christ"},
    {"id": "discernment_1", "shorthand": "Discernment, Part 1"},
    {"id": "discernment_2", "shorthand": "Discernment, Part 2"},
    {"id": "discernment_3", "shorthand": "Discernment, Part 3"},
    {"id": "abraham_1", "shorthand": "Abraham, Part 1"},
    {"id": "abraham_2", "shorthand": "Abraham, Part 2"},
    {"id": "abraham_3", "shorthand": "Abraham, Part 3"},
    {"id": "abraham_4", "shorthand": "Abraham, Part 4"},
    {"id": "nephi_1", "shorthand": "Nephi, Part 1"},
    {"id": "nephi_2", "shorthand": "Nephi, Part 2"},
    {"id": "third_root", "shorthand": "Third Root"},
    {"id": "witness_in_all_the_world", "shorthand": "Witness in All the World"},
    {"id": "every_word_1", "shorthand": "Every Word, Part 1"},
    {"id": "every_word_2", "shorthand": "Every Word, Part 2"},
    {"id": "noah", "shorthand": "Noah"},
    {"id": "discernment_epilogue", "shorthand": "Discernment, Epilogue"},
    {"id": "allegory_of_the_olive_tree", "shorthand": "Allegory of the Olive Tree"},
    {"id": "calling_and_election_1", "shorthand": "Calling and Election, Part 1"},
    {"id": "calling_and_election_2", "shorthand": "Calling and Election, Part 2"},
    {"id": "effective_study_1", "shorthand": "Effective Study, Part 1"},
    {"id": "effective_study_2", "shorthand": "Effective Study, Part 2"},
    {"id": "effective_study_3", "shorthand": "Effective Study, Part 3"},
    {"id": "as_a_little_child", "shorthand": "As a Little Child"},
    {"id": "good_questions", "shorthand": "Good Questions"},
    {"id": "hyrum_smith", "shorthand": "Hyrum Smith"},
    {"id": "ministry_of_the_fathers", "shorthand": "Ministry of the Fathers"},
    {"id": "patriarchs", "shorthand": "Patriarchs"},
    {"id": "authentic_christianity_1", "shorthand": "Authentic Christianity, Part 1"},
    {"id": "authentic_christianity_2", "shorthand": "Authentic Christianity, Part 2"},
    {"id": "hope_1", "shorthand": "Hope, Part 1"},
    {"id": "hope_2", "shorthand": "Hope, Part 2"},
    {"id": "peter", "shorthand": "Peter"},
    {"id": "hope_3", "shorthand": "Hope, Part 3"},
    {"id": "fasting", "shorthand": "Fasting"},

//    {"id": "fyom_1", "shorthand": "Be of Good Cheer, Boise ID"},
//    {"id": "fyom_2", "shorthand": "Faith, Idaho Falls ID"},
//    {"id": "fyom_3", "shorthand": "Repentance, Logan UT"},
//    {"id": "fyom_4", "shorthand": "Covenants, Centerville UT"},
//    {"id": "fyom_5", "shorthand": "Priesthood, Orem UT"},
//    {"id": "fyom_6", "shorthand": "Zion, Grand Junction CO"},
//    {"id": "fyom_7", "shorthand": "Christ, Ephraim UT"},
//    {"id": "fyom_8", "shorthand": "A Broken Heart, Las Vegas NV"},
//    {"id": "fyom_9", "shorthand": "Marriage and Family, St George UT"},
//    {"id": "fyom_10", "shorthand": "Preserving the Restoration, Mesa AZ"},
];

// List all files in a directory in Node.js recursively in a synchronous fashion
var walkSync = (queries, dir) => {
    return new Promise(async (resolve, reject) => {
      var fs = fs || require('fs'),
          bookDirs = fs.readdirSync(dir);

      let queries = [];
      try {
        for (let bookDir of bookDirs) {
            queries = queries.concat(await processDir(dir, bookDir));
        }
        return resolve(queries);
      } catch (err) {
        reject(err);
      }
    });
};

var processDir = (dir, bookDir) => {
  return new Promise(async (resolve, reject) => {
      console.log('processDir: ' + bookDir);
      var [order, book] = bookDir.split('_');
      let filelist = fs.readdirSync(dir + '/' + bookDir + '/');
      let queries = [];
      try {
        for (let file of filelist) {
            queries.push(await processFile(dir, bookDir, file, order));
        };
        return resolve(queries);
      } catch (err) {
        reject(err);
      }
  });
};

var processFile = (dir, bookDir, file, order) => {
  return new Promise((resolve, reject) => {
      console.log('processFile: ' + file);
      var [order, book] = bookDir.split('_');

      if (book === 'glossary') {
          var [chapter, book, extension] = file.split(/[. _]+/);
          console.log("chapter: " + chapter + ", book: " + book + ", extension: " + extension);
      } else if (book === 'podcast' || book === 'fyom') {
          var [index, chapter, extension] = file.split(/[. ]+/);
          console.log("index: " + index + ", chapter: " + chapter + ", extension: " + extension);
      } else {
          var [index, chapter, book, extension] = file.split(/[. _]+/);
          console.log("index: " + index + ", chapter: " + chapter + ", extension: " + extension);

          // Handle front matter in it's own book
          if (parseInt(chapter) === 0) {
              chapter = book;
              book = `${workId}frontmatter`.replace(/\//,"");
              console.log('book: ' + book + ", chapter: " + chapter);
          }
      }

//      console.log(order + ", " + book);
//      console.log(index + ", " + chapter + ", " + extension);
      var fileContents = fs.readFileSync(dir + '/' + bookDir + '/' + file, "utf8");
      //console.log(he.decode(fileContents));
      var formattedText = formatText(he.decode(fileContents));

      try {
        return resolve(generateInsertSql(parseInt(order, 10), book, chapter, file, formattedText));
      } catch (err) {
        reject(err);
      }
  });
}

var generateInsertSql = (order, book, chapter, file, text) => {
    console.log("generateInsertSql");
    console.log(book + " , " + chapter);
//    var title = _(titles).find({id: book}).shorthand;
    var sql = `('${workId}','${book}','${chapter}','${text}'),`;
//    console.log(sql);
    return sql;
}

var formatText = (text) => {
    var $ = cheerio.load(text, {
          normalizeWhiteSpace: true,
          xmlMode: true,
          decodeEntities: false
      });

      // Remove anchor tags
      const anchors = [];
      $('a').each(function (i, elem) {
        $(this).replaceWith($(this).html());
        anchors[i] = $(this).html();
      });

      var selector = $('chapter');
      if (!selector.html()) {
        selector = $('div');
      }

      var html = selector.html().replace(/[']/g, "''").trim();

      return '<ol class="simple-text">' + html + '</ol>';
}

// Walk through files
walkSync(queries, dir).then( (_queries_) => {
    queries = queries.concat(_queries_);
    queries[queries.length-1] = queries[queries.length-1].replace(/\),$/, ");");
    queries.push("COMMIT;");
    fs.writeFile("./chapters_" + workId + ".sql", queries.join('\n'), function(err) {
        if(err) return console.log(err);
        console.log("The file was saved!");
    });
});
