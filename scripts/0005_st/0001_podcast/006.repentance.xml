<?xml version="1.0" encoding="utf-8"?>
<div class="simple-text">
    <h3 class="chap">Repentance</h3>

<p>What is repentance? What do I need to repent of? How do I repent and what happens when I do?</p>

<p>Today Denver addresses the reality of repentance and what it is and what is required with clarity and simplicity. What if repentance is related to the acquisition of intelligence? Or in other words, light and truth. What if repentance requires us to worship Christ by making a living sacrifice by ministering to others with love and compassion? Or in other words, to be more like Him in word, thought and deed.</p>

<h3><a href="http://denversnufferpodcast.com/transcripts/006_Repentance_Transcript.pdf">Transcript:</a></h3>

<div class="page" title="Page 1">

<div class="section">

<div class="layoutArea">

<div class="column">

<p><strong>QUESTION: What is repentance, what do I need to repent of, how do I repent, and what happens when I do?</strong></p>

</div>

</div>

</div>

</div>

<div class="page" title="Page 1">

<div class="section">

<div class="layoutArea">

<div class="column">

<p>DENVER: Doctrine and Covenants section 93:1 says, “Verily, thus saith the Lord: It shall come to pass that every soul who forsaketh his sins and cometh unto me, and calleth on my name, and obeyeth my voice, and keepeth my commandments, shall see my face and know that I am.”</p>

<p>“Every soul who forsaketh his sins.” You’re not going to get past your sins until God forgives you, but you need to awaken to the fact that you possess them and turn from them. Because turning from them is repentance– turning to face Him. You can still have a load that needs to be dropped because we are all heavy laden with sin. But, forsaking your sins means that you would prefer Him over everything else there is. So turn and face Him.</p>

<p>“Cometh unto me.” Well, the only way you can leave that load behind, is to get down in prayer seeking Him and asking Him to free you from the load and to allow you, as Alma recounts in his 36th chapter of the book of Alma, the terrible agony that he felt in calling upon God to be redeemed. And then when God answered, the pain, the distress that he had was equalled by the joy and the exhilaration he felt on the other side of that, being cleansed.</p>

<p>“Calleth on my name.” You have to do that. “And obeyeth my voice.” That would include not merely the things that were given to us by Joseph Smith that you may be neglecting, but obeying His voice in what He tells you here and now. Because your agenda is different from mine. Your needs are different from mine. Your responsibilities are different from mine. You have your own family, you have your own ward, you have your own neighbors, you have your own issues. Fathers and sons, mothers and daughters– you’re part of a community somewhere, and inside of that, all of you need to listen to the voice of God. Because He loves everyone. God loves all of us, and the agenda that you have, and the people you can affect, and the relief that you can administer, and the needs that go in front of your eyes day by day– are uniquely yours. And the relief that you can grant to those around you– that’s yours. It was given to you by God as a gift.</p>

<p>Don’t harden your heart. The fact is, we all are broken. And we are all in need of repair. Come to Him. Because the only repairman that exists in the universe, inside of this matrix, is Christ, whose assignment it is to repair, and to redeem, and to heal us.</p>

</div>

</div>

</div>

</div>

<div class="page" title="Page 2">

<div class="section">

<div class="layoutArea">

<div class="column">

<p>Obey His voice, no matter how much it may disagree with the flow of that, that goes on all around you. “Obeyeth my voice and keepeth my commandments”– “my commandments,” given to the prophet Joseph Smith, entrusted to you, should be respected by you. Given by the voice of the spirit to you, asking you to help those around you because the relief that people need sometimes can only come from one source, and that is you, under the inspiration of the spirit, relieving the burdens of those around you.</p>

<p>Why do you think God cares about the widows, and the orphans, and the poor, and the infirm? Be like your Master. Do what you can for those around you who are infirm. They are here in abundance– the broken-hearted, the families that are in need. If you want to be saved, help the Lord save others– not by preaching and clamoring and demanding that they view the world like you do, but by giving them a hand. Your most powerful sermon can be in the effort that you make and the time that you take to let people know that you care about them.</p>

<p>If you would like to repent of your sins, take a look around at those in need and do what you can for them. Because you’ve begun the first step. When your heart is like Him, then you open up so that He can enter in. And when you’re heart is unlike Him, well there’s no room except if He break it, which He will do. You do these things, “you shall see my face and know that I am.” Know , not believe, but know.</p>

<p>This is still that paragraph 9 of Lectures on Faith–7th Lecture. (It looks like it’s only a third of the way down– it’s a long paragraph). It poses the question: “Where shall we find a saved being? for if we can find a saved being, we may ascertain, without much difficulty, what all others must be, in order to be saved—we think, that it will not be a matter of dispute, that two beings, who are unlike each other, cannot both be saved; for whatever constitutes the salvation of one, will constitute the salvation of every creature which will be saved: and if we find one saved being in all existence, we may see what all others must be, or else not be saved. We ask, then, where is the prototype? or where is the saved being? We conclude as to the answer of this question there will be no dispute among those who believe the bible, that it is Christ: all will agree in this that he is the prototype or standard of salvation, or in other words, that he is a saved being.”</p>

<p>Skipping down a couple of lines, “If he were any thing different from what he is he would not be saved; for his salvation depends on his being precisely what he is and nothing else.” So according to the Lectures on Faith , if you would be saved, you have to be exactly, precisely what Christ is, and nothing else.</p>

<p>Now you’ve been told all your life that that’s an impossibility. Well it’s an impossibility in one sense, and it’s a mandatory requirement in another sense. It’s an impossibility because, as it turns&nbsp;out, we all err.&nbsp; All of us err– we always have. And that’s what the Atonement was designed to fix. Because He picks that burden up and He carries it for us. But the fact that He will carry that burden for us doesn’t relieve us from the moment that He’s taken that away, from then going forward to do good. You can be Christlike. You can administer relief to those around you. You can, as He said, clothe the naked, feed the hungry, visit those who are in prison. That ministration, that service, elevates the servant. Their heart gets moved with compassion. Your heart needs to be like Christ’s– moved with compassion for others. And the way you do that is imitative at first, and then it is informed by the experience later. What begins as imitation, and merely that, finds room within to have genuine compassion for the needs of others.</p>

</div>

</div>

</div>

</div>

<div class="page" title="Page 3">

<div class="section">

<div class="layoutArea">

<div class="column">

<p>Take a look at Doctrine and Covenants section 93:36: “The glory of God is intelligence, or in other words light and truth. Light and truth forsake that evil one” [D&amp;C 93:36-37]. What if instead of repentance being related to your misdeeds, which are so plentiful and persistent and will continue– what if instead it is related to the acquisition of light and truth, that is intelligence? What if repentance requires you to take whatever it is that you have that is a foolish error, a vain tradition, a false notion, and replace it with the truth?</p>

<p>My suspicion is that whatever it is that is troubling you, it will trouble you considerably less if you begin to fill yourself with light and truth, until at last you arrive at a point where you look back upon your sins and you say “I have no more disposition for that, because I frankly know enough not to do that anymore, and because I prefer the light and because I prefer God’s intelligence and glory over that which I used to trade, to substitute for it.”</p>

<p>You see, repentance may have a whole lot more to do with your own feeble education in the things of God than it does have to do with the time you spend wasted looking at some vile picture or other. People struggle with some very difficult, very challenging things. You need to try and overcome that by the light within you.</p>

<p>“The glory of God is intelligence.” Be intelligent. The fact of the matter is that you can fill yourself with the mind of God. And if you fill yourself with the mind of God, you’re going to find yourself in a position where you, like the scriptures recite, have no more disposition to do evil, but to only do good continually. That repentance is as a consequence of the things that you know. That repentance comes as a consequence of the light and truth within you. But the problem is not that God has built within you the desires, appetites, and passions which He does not intend to have you fill. He intends for you to eat. He intends for you to sleep. He intends for you to reproduce. He intends for all of the appetites and passions put within you to be intelligently organized and gratified in a sacred manner in which the purposes of God are advanced. And you find within yourself holiness in everything you do, love and understanding in everything you do.</p>

</div>

</div>

</div>

</div>

<div class="page" title="Page 4">

<div class="section">

<div class="layoutArea">

<div class="column">

<p>Repentance is the process of figuring out exactly how and why God made all the things available to you that He made available to you– each one to be used with prudence and with skill. We’re just trying to figure out how it is we move from wherever we are back to a state of being repentant. And that requires you to exercise your effort to learn and obtain glory from God which is intelligence, or in other words, light and truth– not darkness, dimness, error, missteps, incomplete and inadequate information. You’re going to have to face it, and you’re going to have to face it with some amount of courage. Because we all labor with a good deal of tradition that had been inflicted upon our minds and upon our hearts. And things that we may love, if they don’t conform to the glory of God, intelligence or light and truth, they have to be discarded, too. Because what God wants to do is to bring you back into a state of reconciliation with Him, which comes only from bravely facing light and truth– the glory of God, the power of Godliness, if you will.</p>

<p>“If any should ask why all these sayings? the answer is to be found from what is before quoted from John’s epistle, that when he (the Lord) shall appear, the saints will be like him: and if they are not holy, as he is holy, and perfect as he is perfect, they cannot be like him; for no being can enjoy his glory without possessing his perfections and holiness, no more than they could reign in his kingdom without his power” [LoF 7:10].</p>

<p>When He appears, you need to be like Him. Lay down the burden of guilt. Lay down the burden of sin. Stop focusing on that stuff and become like Him. And you become like Him by doing His works. And you do His works by serving others, by ministering to the needs of others. And when you do that, it is a natural byproduct of that process, ordained by laws established before the foundation of the world, that light and truth will grow within you. You will have compassion when you minister with compassion to the needs of others. Your heart will open to, and receive within it, light and truth. When your conduct reflects the same conduct as a merciful, and holy, and just God whom you claim to worship, worship Him by imitating Him. Worship Him by doing His works. Worship Him by making a living sacrifice. Set aside the junk that occupies you and go do something that is holy for someone else.</p>

<p>However mundane and trivial it may seem to you, when you relieve the suffering of other people, something changes in you. You become different. You become better. You become more like our Lord. Because when you give– whatever it is you give away– you get more in return. But make sure that what you give goes to relieve the suffering of others. Relieve the suffering of others.</p>

</div>

</div>

</div>

</div>

<div class="page" title="Page 5">

<div class="section">

<div class="layoutArea">

<div class="column">

<p>You’re going to have to finish that path. You’re going to have to rise up. If you expect to be in His presence when He returns, then you’re going to have to be like Him. Because if you are not like Him, you will not be able to endure His presence.</p>

<p>Take it seriously. Study it through. Seek to be like Him whom you worship. It is possible. Not while you’re carrying a load of sins that trouble you and worry you and distract you, but that’s what the Lord will remove from you. He can take all of that away, but it is entirely up to you to choose, then to do something to draw nearer to Him. You have to choose to be like him. Although He may remove all of the stains upon you, you have to go forward and not stain yourself again, because you can’t stop you from doing that.</p>

<p>You’re free to choose. Therefore, choose the better part. The Atonement isn’t like Tinkerbell spreading some magic dust that will make you rise up. The Atonement will erase your sins and mistakes, but you must rise up. You must acquire those virtues. The glory of God is intelligence, and repentance requires you to acquire that intelligence, that glory of God. And you acquire it by the things that you do in His name and for His sake. And those that are here with you in need, they represent Him. And when you do it to even the least of them, He will credit that as having been done for Him. And no good deed will be gone unnoticed with Him. He even notices when the sparrows fall. So is He not going to notice when your knee bends with compassion, praying for His mercy for someone that has offended you? And when you pray for those who have offended you, do you think for one moment that that doesn’t change your own heart?</p>

<p>Paragraph 18: “How are they to obtain the knowledge of God?…(for there is a great difference between believing in God and knowing him: knowledge implies more than faith. And notice, that all things that pertain to life and godliness, were given through the knowledge of God;) the answer is given, through faith they were to obtain this knowledge; and having power by faith to obtain the knowledge of God, they could with it obtain all other things which pertain to life and godliness” [LoF 7:18].</p>

<p>It is knowledge that saves. Consequently, it is knowledge that you need to repent and obtain. “Knowledge saves a man,” said Joseph Smith. “A man is saved no faster than he gets knowledge,” said Joseph Smith. Knowledge and salvation, knowledge and repentance– they are all related. If you have it, it is given to make you a minister, a servant, someone the Lord might be able to employ in order to raise up others. Because if you can’t elevate others, then you’ve failed in your effort to be like Him. He came to serve. You serve, too. That’s the purpose of the gospel– to give you knowledge. Therefore, the way to get knowledge is to repent, is to search into, lay hold upon and obtain for yourself knowledge that saves. You have to sacrifice and you have to serve the Lord, and you have to have Him and Him only as the reason for what you do, what you say, how you act. Because He’s the one that’s going to judge you.</p>

</div>

</div>

</div>

</div>

<div class="page" title="Page 6">

<div class="section">

<div class="layoutArea">

<div class="column">

<p>If knowledge saves, then it follows that repentance requires us to learn something. “You must begin with the first and go on until you learn all the principles of exaltation” [ King Follett Discourse , April 7, 1844].</p>

<p>I want to remind you that it is knowledge which defines the millennial glory of man. “They shall not hurt nor destroy in all my holy mountain: for the earth shall be full of the knowledge of the Lord, as the waters cover the sea” [Isaiah 11:9]. Would you like to stand in that day? Would you like to survive that burning which is to come? Then the way to obtain that, and the means to preserve yourself through that, is to obtain that knowledge which saves.</p>

<p>What must you do in order to qualify to be among them? Does anyone other than you have the ability to prepare you? These are the things which God intends to have happen. The culmination of all the prophecies are going to wrap up in a time following the ministration of the prophet Joseph Smith. Soon to come– not yet, but soon. And here we are. What are you going to do about it? The prophecies cannot be fulfilled unless those who are free to choose, choose to repent and to do something about what great things lay in store, and therefore you need to know how great things the Lord intends to do. You need to take this as the beginning point and go on and discover for yourself how great things the Lord intends to do.</p>

<p>God’s work is the same yesterday, today and forever. When God speaks through Joseph and we forget him, then we have no right to expect, collectively, that He’s going to move anything forward for us. The first order of repentance is to remember what God gave to us through Joseph. You do that, and then you’ll find God’s perfectly willing to pick it up and move it forward. You don’t do that, and God will simply wait for you to get around to discharge the duty that’s devolving upon you.</p>

<p>Alma the Younger, a fairly expert source on the subject of repentance– this is in chapter 42 of Alma: “And now, my son, I desire that ye should let these things trouble you no more, and only let your sins trouble you, with that trouble which shall bring you down unto repentance” [verse 29]. Don’t trouble yourself unless it is motivational. To have change (and repentance simply means “change”)– to add repentance actually means you turn from the way, the direction you’re facing. Whatever the direction is you happen to be facing, change from that and face God. When you turn to God, when you face Him, when that is the object of your focus and attention, then you’ve repented.</p>

<p>“O my son, I desire that ye should deny the justice of God no more. Do not endeavor to excuse yourself in the least point because of your sins, by denying the justice of God; but do you let the justice of God, and his mercy, and his long-suffering have full sway in your heart; and let it&nbsp;bring you down to the dust in humility” [Alma 42:30]. Therefore, when we look at the voices that would like to call attention to whatever it is that they’re trying to draw your attention to, in this world, one of the things that Alma suggests might be helpful are those voices that happen to be saying that there’s something amiss, there’s something that deserves your attention to repent, to change the course you’re on, to turn and face God, and to allow the only One who can offer salvation, to offer salvation.</p>

</div>

</div>

</div>

</div>

<div class="page" title="Page 7">

<div class="section">

<div class="layoutArea">

<div class="column">

<p>Our Savior was, and is, first and foremost, a teacher. “By His knowledge,” Isaiah and Nephi wrote, “He shall justify many” [Mosiah 14:11; Isaiah 53:11]. By His knowledge. He possesses things which we do not yet comprehend. He possesses things which He would like us to comprehend. How then are we to comprehend the things which only He can teach? By permitting Him to do so; by coming to Him, to recognize that the challenge you face in your life requires you invariably to lay aside those things that pull you away, and that you always turn and face the Lord. That’s what repentance means. It means to turn and face the Lord. And you know when you face Him the first time, you’re just not going to be that good, or that different than you were the moment before. But if you’ll face Him, He’ll work with you.</p>

<p>It does not matter how badly damaged you are. That’s irrelevant. He fixed the apostle Paul. If you don’t think the apostle Paul suffered from pride, then you don’t understand the malignancy of pride. He fixed Alma the Younger and the sons of Mosiah, whose deliberate purpose was to overthrow the things of God. I don’t care what you’ve done, the malignancies of those men are highlighted in scripture in order to assure you that you can all be reclaimed. Turn and face Him, and then walk with Him. He does all the guiding, and most of the heavy lifting.</p>

<p>And what is it that you must repent of? The absence of knowledge about God.</p>

<p>You don’t know enough yet to be saved. The plan of salvation is the plan of education, the plan of knowledge about God and the principles of Godliness, and the basis upon which all of you can live together and be of one heart and one mind. Because much of what you think matters, doesn’t matter one whit to the Lord. And you know what? When you’re anxiously engaged in the right cause, you’d be surprised how much of our deepest concerns are merely trivial. The things of the heart are what matters– the things upon which we are capable of becoming one in love toward one another.</p>

<p>It is His doctrine that all mankind should repent and be baptized in His name for the remission of sins. If you do so, He will be faithful and forgive. Repentance means to turn from whatever else is distracting you and face God. Heed Him. Follow Him, and obey His will. Repentance substitutes virtue for sin, trades weakness for strength, and remakes us heart, mind and spirit, into a new creature, a son or daughter of God.</p>

</div>

</div>

</div>

</div>

<div class="page" title="Page 8">

<div class="section">

<div class="layoutArea">

<div class="column">

<p>—–</p>

<p>The foregoing remarks are excerpts from Denver’s 40 Years in Mormonism Series, Talk #3, entitled “Repentance” given in Logan, UT on September 29th, 2013; his talk entitled “The Mission of Elijah Revisited,” given in Spanish Fork, UT on October 14th, 2011, a fireside talk on “The Temple,” given in Ogden, UT on October 28th, 2012, a fireside talk entitled “That We Might Become One,” given in Clinton, UT on January 14th, 2018, and his conference talk entitled “The Doctrine of Christ,” given in Boise, ID on September 11th, 2016.</p>

</div>

</div>

</div>

</div>

</div>
