<?xml version="1.0" encoding="utf-8"?>
<div class="simple-text">
    <h3 class="chap">The Remnant</h3>

<p>In today’s episode Denver answers the questions, “Who are the remnant groups?” and “What are the prophecies concerning them in our dispensation?”</p>

<h3><a href="http://denversnufferpodcast.com/transcripts/016_The-Remnant_Transcript.pdf">Transcript:</a></h3>

<p>Why does the remnant, which will build Zion, tear in pieces and trample underfoot the Gentiles? Why do they do that? Start thinking about the image of Babylon that is going to be “torn in pieces and trodden under foot.” You do not need anything other than the truth to “tear in pieces” the Gentile’s kingdom. And it will be trodden under foot by the truth.</p>

<p><em>“And in that day there shall be a root of Jesse, which shall stand for an ensign of the people; to it shall the Gentiles seek: and his rest shall be glorious. And it shall come to pass in that day, that the Lord shall set his hand again the second time to recover the remnant of his people, which shall be left” </em>(2 Nephi 21:10-11). Well this shall shortly come to pass. Not then, not that day, but by and by.</p>

<p>You know, when a branch is spoken of, if you look at John 15:1-6– I’m not going to do that because our time is far spent– but Christ gives a sermon about Him being the “true vine” and how you cannot bear fruit unless you’re connected to the true vine. Once again, that is a genealogical term. That is a family of God term. That is a son of God term. And He intends to make many sons of God.</p>

<p>This is what has not yet been fulfilled that is soon to come. <em>“And it shall come to pass afterward, </em>[beginning at verse 28 of chapter 2 of Joel] <i>that I will pour out my spirit upon all flesh; and your sons and your daughters shall prophesy, your old men shall dream dreams, your young men shall see visions: And also upon the &nbsp;servants</i><i style="font-size: 1.125rem;">&nbsp;and upon the handmaids in those days will I pour out my spirit. I will shew wonders in the heavens and in the earth, blood, and fire, and pillars of smoke. The sun shall be turned into darkness, the moon into blood, before the great and the terrible day of the Lord. And it shall come to pass, that whosoever shall call on the name of the Lord shall be delivered: for in mount Zion and in Jerusalem shall be deliverance, as the Lord hath said, and in the remnant whom the Lord shall call”&nbsp;</i>(Joel 2:28-32).</p>

<p>Now one of the things that we perhaps take for granted, but we have to give credit to Joseph Smith for doing, is distinguishing between all these references to Jerusalem and these references to Zion. Until Joseph spoke, it was assumed that that was the same thing. It’s not. And we know that there will be Zion, on the one hand, and we know that there will be Jerusalem, a gathering there, on the other hand. And we know that this prophecy concerning your sons, your daughters, prophecy, dreams, visions– all of this being poured out– this is something that is yet to happen, but it is soon to come to pass. We have a great deal to look forward to that the angel Moroni, instructing Joseph Smith, covered with him on that night when he first appeared to him, promising that these things were going to come to pass, and they still had not yet come in.</p>

<p>Going back to 3 Nephi 21: <em>“But if they will repent and hearken unto my words, and harden not their hearts, I will establish my church among them, and they shall come in unto the covenant and be numbered among this remnant of Jacob, unto whom I have given this land for their inheritance…”</em>(verse 22). They are the Gentiles. They are Ephraim. You.</p>

<p><em>“And they shall assist my people, the remnant of Jacob, and also as many as the house of Israel as shall come, that they may build a city, which shall be called the New Jerusalem. Then shall they assist my people that they may be gathered in, who are scattered upon all the face of the land, into the New Jerusalem. And then shall the power of heaven come down among them; and I will also be in their midst”</em>(verses 23-25). The word “powers of heaven” is talking about the angels. In this circumstance, the “power” is singular. Not only because they are one, but because among them will be our Lord Himself. And where He is, there is the power of Heaven. And it is singular.</p>

<p><em>“And then shall the work of the Father commence at that day, even when this gospel shall be preached among the remnant of this people”</em>(verse 26). Because when you gather to hear what you’re going to hear there, this is the final stage in the Father’s work. This is the work that requires holiness to accompany even its teaching.</p>

<p><em>“Verily I say unto you, in that day shall the work of the Father commence among all the dispersed of my people, yea, even the tribes which have been lost, which the Father hath led away out of Jerusalem. Yea, the work shall commence among all the dispersed of my people, with the Father to prepare the way whereby they may come unto me, that they may call on the Father in my name. Yea, and then shall the work commence, with the Father among all nations in preparing the way whereby His people may be gathered home to the land of their inheritance. And they shall go out from all nations; and they shall not go out in haste, nor go by flight, for I will go before them, saith the Father, and I will be their rearward” </em>(3 Nephi 21:26-29).</p>

<p>Again and again the notion that this work can be hastened is denounced. Again and again it’s to be a place in which the preparations are done first. Again and again it suggests that before we can gather together we must have that heart, that mind which can make us one.</p>

<p>Turn back and look at the results– <em>Doctrine and Covenants</em>45, beginning at verse 65: <em>“And with one heart and with one mind, gather you up your riches that ye may purchase an inheritance which shall hereafter be appointed unto you. And it shall be called the New Jerusalem, a land of peace, a city of refuge, a place of safety for the saints of the Most High God; And the glory of the Lord shall be there, and the terror of the Lord also shall be there, insomuch that the wicked will not come unto it, and it shall be called Zion”</em>(verses 65-67). They won’t come unto it because to go there is like Acts chapter 5– you cannot endure that environment if you’re abiding a telestial law. You’d be consumed.</p>

<p><em>“And it shall come to pass among the wicked, that every man that will not take his sword against his neighbor must needs flee unto Zion for safety. And there shall be gathered unto it out of every nation under heaven; and it shall be the only people that shall not be at war one with another. And it shall be said among the wicked: Let us not go up to battle against Zion, for the inhabitants of Zion are terrible; wherefore we cannot stand. And it shall come to pass that the righteous shall be gathered out from among all nations, and shall come to Zion, singing with songs of everlasting joy”&nbsp;</em>(D&amp;C 45:68-71).</p>

<p>Think about what it would take to transplant various populations, from various locations (not in haste), with everything having been prepared in advance. And in our currently fragmented society, unless you’re willing to experiment with your own effort to live the law of tithing by organizing yourselves and governing yourself, miscellaneous groups will never make it. But people of God will.</p>

<p>Every dispensation of the gospel has left only a remnant behind. Christ’s work is designed to preserve a remnant and at the end, gather all remnants together again. The restoration that was given to the prophet Joseph Smith has likewise put itself in a position where now it can only produce a remnant– but one that will be preserved and not abandoned.</p>

<p>In 3 Nephi 21 the Lord talked about some things that become exceptionally relevant in light of what we’ve covered today:</p>

<p><em>“And verily I say unto you, I give unto you a sign, that ye may know the time when these things shall be about to take place—that I shall gather in, from their long dispersion, my people, O house of Israel, and shall establish again among them my Zion”&nbsp;</em>(verse 1).</p>

<p>This is addressing all of those various remnants wherever that they may be found so long as they are some residue of the house of Israel.</p>

<p><em>“And behold, this is the [thing] which I will give unto you for a sign—for verily I say unto you that when these things which I declare unto you, and which I shall declare unto you hereafter of myself, and by the power of the Holy Ghost which shall be given unto you of the Father, shall be made known unto the Gentiles…”&nbsp;</em>See, the Gentiles had to first receive some things. <em>“…that they </em>[the Gentiles] <em>may know concerning this people who are a remnant of the house of Jacob, and concerning this my people who shall be scattered by them </em>[the Gentiles]; <em>Verily, verily, I say unto you, when these things shall be made known unto them </em>[some constituent group of Gentiles] <em>of the Father, and shall come forth of the Father, from them unto you”&nbsp;</em>(verses 2-3).</p>

<p>It can’t come from any source other than from the Father. The Father and Christ being One, the authority to administer and to deliver it coming from Them, the power to baptize being brought forth from some remnant of the gentiles who care to bear it.</p>

<p><em>“For it is wisdom in the Father that they </em>[the Gentiles] <em>should be established in this land, and be set up as a free people by the power of the Father, that these things might come forth from them unto a remnant of your seed, that the covenant of the Father may be fulfilled which he hath covenanted with his people, O house of Israel”&nbsp;</em>(verse 4).</p>

<p>“O house of Israel” is much more. “O house of Israel” is that same inclusive of all bits and remnants wherever they may be found. I talked about covenants when we were in Centerville and about the fulfillment of the covenants. All of the covenants which apply to people scattered everywhere– all of those included within the previous remnants– they need to be gathered into one constituent group.</p>

<p><em>“Therefore, when these works and the works which shall be wrought among you hereafter shall come forth from the Gentiles….”&nbsp;</em>Not their book, their <em>works</em>. Not their book, the <em>works&nbsp;</em>bringing to pass the doctrine of Christ, establishing repentance, declaring and baptizing by the authority of Christ, having people visited by fire and the Holy Ghost– these are the works. These are the works.</p>

<p><em>“…shall come forth from the Gentiles unto your seed which shall dwindle in unbelief because of iniquity; For thus it behooveth the Father that it should come forth from the Gentiles, that he may show forth his power unto the Gentiles…”</em>That’s what He needs now to do. That’s what He intends to do, if you will receive it.</p>

<p><em>“For this cause that the Gentiles, if they will not harden their hearts, that they may repent and come unto me and be baptized in my name and know of the true points of my doctrine, that they may be numbered among my people, O house of Israel” </em>(verses 5-6).</p>

<p>You can’t get there except through the power of the doctrine and the power of the ordinance that God has given, in the way that it has been given, performed with the exactness, fidelity, and language that has been given to us by Christ Himself.</p>

<p><em>“And when these things come to pass that thy seed shall begin to know these things—it shall be a sign unto them, that they may know that the work of the Father hath already commenced unto the fulfilling of the covenant which he made unto the people who are of the house of Israel”&nbsp;</em>(verse 7). All of them– it’s a witness that His work has commenced.</p>

<p><em>“And my people who are a remnant of Jacob shall be among the Gentiles, yea, in the midst of them as a lion among the beasts of the forest, as a young lion among the flocks of sheep, who, if he go through both treadeth down and teareth in pieces, and none can deliver. Their hand shall be lifted up upon their adversaries, and all their enemies shall be cut off. Yea, wo be unto the Gentiles except they repent; for it shall come to pass in that day, saith the Father, that I will cut off thy horses out of the midst of thee, and I will destroy thy chariots; And I will cut off the cities of thy land, and throw down all thy strongholds; And I will cut off witchcrafts out of thy land, and thou shalt have no more soothsayers; Thy graven images I will also cut off…thou shalt no more worship the works of thy hands; And I will pluck up thy groves out of the midst of thee; so will I destroy thy cities. And it shall come to pass that all lyings, and deceivings, and envyings, and strifes, and priestcrafts, and whoredoms, shall be done away. For it shall come to pass, saith the Father, that at that day whosoever will not repent and come unto my Beloved Son, them will I cut off from among my people, O house of Israel </em>[that’s all remnants gathered together]<em>;</em><em>And I will execute vengeance and fury upon them, even as upon the heathen, such as they have not heard. But if they </em>[speaking of the Gentiles]<em>will repent and hearken unto my words, and harden not their hearts, I will establish my church among them, and they shall come in unto the covenant and be numbered among this the remnant of Jacob, unto whom I have given this land for their inheritance” </em>(verses 12-22).</p>

<p>Because every time there’s a covenant there is always a land. And this is the land that God covenants He will give, and the people to whom He will give it are those that come back and receive the covenant, including the Gentiles in whose ears this first shall sound, if they will come. It will require a covenant. It will require adoption. It will require sealing. It was what Joseph looked forward to have happen at some point in the future during the days of his prophecy.</p>

<p><em>“And they shall assist my people, the remnant of Jacob, and also as many of the house of Israel as shall come, that they may build a city, which shall be called the New Jerusalem. And then shall they assist my people that they may be gathered in, who are scattered upon all the face of the land, in unto the New Jerusalem. And then shall the power of heaven…”</em>In this case, it is the singular, it’s not the “powers,” because when you have Him present with you, you have all the authority, <em>“…then shall the power of heaven come down among them; and I also will be in the midst. And then shall the work of the Father commence at that day…”&nbsp;</em>(verses 23-26). Christ will come. Once the covenant has been renewed, the city of Zion will follow. The Lord’s presence will come, and then the final stage begins.</p>

<p><em>“…even when this gospel shall be preached among the remnant of this people. Verily I say unto you, at that day shall the work of the Father commence among all the dispersed of my people, yea, even the tribes which have been lost, which the Father hath led away out of Jerusalem. Yea, the work shall commence among all the dispersed of my people, with the Father to prepare the way whereby they may come unto me, that they may call on the Father in my name. Yea, and then shall the work commence, with the Father among all nations in preparing the way whereby his people may be gathered home to the land of their inheritance. And they shall go out from all nations; they shall not go out in haste, nor go by flight, for I will go before them, saith the Father, and I will be their rearward”&nbsp;</em>(verses 26-29).</p>

<p>It’s not going to happen in haste, and the work of the Father that will commence in those nations, to commence the possibility for the gathering, will involve destroying a great deal of political, social, and military obstructions that prevent the gathering, that prevent even the preaching to those that would gather if they could hear. But the work of the Father– and it’s always masculine when it comes to destruction– the work of the Father is going to bring this to an end. All the scattered remnants will be brought back again. The original unified family of God will be restored again. The fathers will have our hearts turned to them because in that day, once it’s permitted to get that far, we will be part of that family again.</p>

<p>Now, having said all that, let me read to you some things which the Lord said concerning this moment. Because He’s talking about an event that will happen. This is from Matthew 22, beginning at verse 2:</p>

<p><em>“The kingdom of heaven is like unto a certain king, which made a marriage for his son, And sent forth his servants to call them that were bidden to the wedding: and they would not come. Again, he sent forth other servants, saying, Tell them which are bidden, Behold, I have prepared my dinner: my oxen and my fatlings are killed, and all things are ready: come unto the marriage. But they made light of it, and went their ways, one to his farm, another to his merchandise: And the remnant took his servants, and entreated them spitefully, and slew them. But when the king heard thereof, he was wroth: and he sent forth his armies, and destroyed those murderers, and burned up their city. Then saith he to his servants, The wedding is ready, but they which were bidden were not worthy. Go ye therefore into the highways, and as many as ye shall find, bid to the marriage. So those servants went out into the highways, and gathered together all as many as they found, both bad and good: and the wedding was furnished with guests. </em><em>And when the king came in to see the guests, he saw there a man which had not on a wedding garment: And he saith unto him, Friend, how camest thou in hither not having a wedding garment? And he was speechless. Then said the king to the servants, Bind him hand and foot, and take him away, and cast him into outer darkness; there shall be weeping and gnashing of teeth. For many are called, but few are chosen” </em>(Matthew 22:2-14).</p>

<p>Now, several things about this. This is one of those places in scripture in which remnant is used in a negative way. A remnant. God invites all to come to the wedding feast of His son. This is when the kingdom is going to be established in the last days. He invites all to come and from among all of those people who had been invited, there’s a remnant of those who still hold onto the restoration, and they are the worst of all. They have the hardest hearts. They are the ones who will not come. And after the Lord deals with them, then He goes out and invites everyone to come. Everyone, come in. And included among those that are invited in are as many as they found both bad and good. They’re all invited to come in, and there is no excluding the bad, speaking after the judgments of this world. Bad people get invited in. And when they come, and when they arrive, it’s not whether they are a bad person or a good person that determines whether they get to stay or not; it’s the presence or absence of a wedding garment. Those that are invited will not come. They’ll even abuse those who try to take them in. But there are plenty of folks in the byways who are only kept from the truth because they don’t know where to find it.</p>

<p>This is your responsibility. This is your work to do. This is the day in which these things need to be done.</p>

<p>The work is beginning again. I suppose it was necessary that what began in Joseph’s time had to run down to the condition that it’s in at present– that it had to become a leaky ruin of a farm that Joseph himself no longer even wanted, before it was possible for the Lord to say “at this moment we turn a new leaf.” But my word, can’t you see the signs of the times? Can’t you look about and see that the whole world is waxing old like a garment? Can’t you see that there is, right now, a balance of things that are kept at bay only to preserve the possibility that a remnant might be claimed? God promised He would do this.</p>

<p><em>“For behold, I say unto you that as many of the Gentiles as will repent are the covenant people of the Lord; and as many of the Jews as will not repent shall be cast off; for the Lord covenanted with none save it be with them that repent and believe in his Son, who is the Holy One of Israel… For the time speedily cometh that the Lord God shall cause a great division among the people, </em>&nbsp;<em>and the wicked will he destroy; and he will spare his people, yea, even if it so be that he must destroy the wicked by fire.” </em>That’s in 2 Nephi chapter 30.</p>

<p>Zion will include people who are willing to receive revelations from God and obey commandments. God does this to bless His people. <em>“Blessed are they whose feet stand upon the land of Zion, who have obeyed my gospel; for they shall receive for their reward the good things of the earth, and it shall bring forth in its strength. And they shall also be crowned with blessings from above, yea, and with commandments not a few, and with revelations in their time—they that are faithful and diligent before me.” </em>That’s from <em>Doctrine and Covenants&nbsp;</em>Section 59.</p>

<p>Christ said that: <em>“It behoveth the Father that it should come forth from the gentiles.”&nbsp;</em>He says: <em>“The gentiles, if they will not harden their hearts, that they may repent and…be baptized in my name and know the true points of my doctrine, that&nbsp;</em>[the Gentiles] <em>may be numbered among my people”&nbsp;</em>(3 Nephi 21:6).</p>

<p>In the dedicatory prayer to the Kirtland Temple, Joseph Smith dedicated the temple and identified the Latter-day Saints as “we who are identified with the Gentiles.” If we enter into, as Gentiles, a covenant, so that we know the true points of Christ’s doctrine, then the Gentiles who do so may “be numbered among my people.”</p>

<p>If we’ll <em>“…repent and hearken to my words and harden not</em>[our] <em>hearts, I will establish my church among them and they shall come in unto the covenant and be numbered among this…” </em>[remnant]<em>“this, the remnant of Jacob, unto whom I have given this land for an inheritance” &nbsp;</em>(3 Nephi 21:22). It’s talking about the Gentiles, but it’s talking about establishing His word, which is a prerequisite to establishing His people.</p>

<p><em>“…numbered among this the remnant of Jacob, unto whom I have given this land for their inheritance; And they shall assist my people, the remnant of Jacob, and also as many of the house of Israel as shall come, that they may build a city, which shall be called the New Jerusalem”&nbsp;</em>(verses 22-23).</p>

<p>Take another look at 2 Nephi 21, and in particular, pay attention to how the words and the covenant play into the fulfillment of the prophecies and the reclaiming of the Gentiles to become part of His covenant and then those who likewise inherit as their possession this land.</p>

<p>QUESTION:</p>

<p><em>“It is not enough to receive my covenant, but you must also abide it. And all who abide it, whether on this land or any other land, will be mine and I will watch over them and protect them in the day of harvest, and gather them in as a hen gathers her chicks under her wings. I will number you among the remnant of Jacob, no longer outcasts, and you will inherit the promises of Israel. You shall be my people and I will be your God and the sword will not devour you. And unto those who will receive will more be given until they know the mysteries of God in full”&nbsp;</em>(Answer and Covenant, p. 7).</p>

<p>DENVER:</p>

<p><em>“Now, hear the words of the Lord to those who receive this covenant this day: All you who have turned from your wicked ways and repented of your evil doings, of lying and deceiving, and of all whoredoms, and of secret abominations, idolatries, murders, priestcrafts, envying, and strife, and from all wickedness and abominations, and have come unto me, and been baptized in my name, and have received a remission of your sins, and received the Holy Ghost, are now numbered with my people who are of the house of Israel. I say to you: </em></p>

<p><em>Teach your children to honor me. </em></p>

<p><em>Seek to recover the lost sheep remnant of this land and of Israel and no longer forsake them. Bring them unto me and teach them of my ways, to walk in them. </em></p>

<p><em>And I, the Lord your God, will be with you and will never forsake you and I will lead you in the path which will bring peace to you in the troubling season now fast approaching. </em></p>

<p><em>I will raise you up and protect you, abide with you, and gather you in due time, and this shall be a land of promise to you as your inheritance from me. </em></p>

<p><em>The Earth will yield her increase, and you will flourish upon the mountains and upon the hills, and the wicked will not come against you because the fear of the Lord will be with you. </em></p>

<p><em>I will visit my house, which the remnant of my people shall build, and I will dwell therein, to be among you, and no one need to say, Know ye the Lord, for you all shall know me, from the least to the greatest. </em></p>

<p><em>I will teach you things that have been hidden from the foundation of the world and your understanding will reach unto Heaven. </em></p>

<p><em>And you shall be called the children of the Most High God, and I will preserve you against the harvest. </em></p>

<p><em>And the angels sent to harvest the world will gather the wicked into bundles to be burned, but will pass over you as my peculiar treasure”&nbsp;</em>(Answer and Covenant, p. 11-12).</p>

<p>Accepting the covenant is not the final step, our choices will determine whether we are bitter or natural fruit. That will decide our fate. Just as the ancient allegory foretold [Jacob 5], the covenant makes us servants and laborers in the vineyard (verse 61). We are required to, this is from the covenant: <em>“Seek to recover the lost sheep remnant of this land and of Israel and no longer forsake them. Bring them unto [the Lord] and teach them of [His] ways to walk in them</em>.”</p>

<p>If we fail to labor to recover them, we break the covenant. We must labor for this “last time” in the Lord’s vineyard. There is an approaching, final pruning of the vineyard (verse 62). The first to be grafted in are Gentiles so that the last may be first, the lost sheep remnant next, and then Israelites, so that the first may be last (verse 63). But grafting is required for all, even the remnants, because God works with His people through covenant-making.</p>

<p>We have an opportunity. We have a bonafide, actual, author from God, to allow us to be that generation in which the promises get fulfilled. But we have the freedom of choice that allows us to elect to be stubborn, to be contentious, to be agents of destruction, and to discourage and break the hearts of those who would willingly accept the challenge to repent and follow God.</p>

<p>Now it’s also possible, in fact it’s probable, that at some point what the Lord will do is gather out a remnant of a remnant– gather out a few. And how many are essential in order for the promises to be fulfilled? I’m certain there is a minimum. I’m fairly confident that the minimum can be counted on your two hands. But there is no maximum. We’re not going to just have eight people on the ark. There can be more. There could be many more. The upper limit is practically limitless. There is a minimum, but heavens, why would anyone want that?</p>

<p>Continuing with the Heavenly Mother’s declarations in Proverbs 8: <em>“I love them that love me and those that seek me early shall find me. Riches and honor are with me, yea, durable riches and righteousness. My fruit is better than gold, yea, than fine gold and my revenue than choice silver”&nbsp;</em>(verse 17-19).</p>

<p>Of all our Mother’s “fruit” the most valuable to fallen man is without doubt the Redeemer, Jesus Christ. The account of how Jesus Christ came into the world begins with a virgin and an angel. There is more to this than Christians have noticed. The prophecy relied on to identify the birthplace of Christ in Bethlehem continues with a description of His Mother. It was prophesied that only when “…<em>she which travaileth hath brought forth; then the remnant of his brethren shall return unto the children of Israel</em>” (Micah 5:3). Because of the labor and travail of His Mother, the prophecy of Israel returning to God was fulfilled. She made His entry into this world possible. The redemption of the remnant is as much the consequence of Her as of Her Son.</p>

<p>What was Mary’s role? Who was she? Is it possible she was “the mother of God” before she came into mortality? These are important questions that ought to be asked. If we can learn the answers they would indeed be glorious.</p>

<p>—–</p>

<p>The foregoing are excerpts from:</p>

<ul>

<li>Denver’s talk entitled “The Mission of Elijah Reconsidered,” given in Spanish Fork, UT on October 14th, 2011;</li>

<li>Denver’s&nbsp;<em>40 Years in Mormonism Series,</em>Talks 1, 2, 6 and 10, given during 2013 and 2014;</li>

<li>Denver’s conference talk entitled “Things to Keep Us Awake at Night,” given in St. George, UT on March 19th, 2017;</li>

<li>Denver’s “Opening Remarks” and the presentation of “Answer and Covenant,” both given at the Covenant of Christ Conference in Boise, ID on September 3rd, 2017;</li>

<li>Denver’s&nbsp;fireside talk entitled “That We Might Become One,” given in Clinton, UT on January 14th, 2018;</li>

<li>Denver’s conference talk entitled “Our Divine Parents,” given in Gilbert, AZ on March 25th, 2018.</li>

</ul>

<p>&nbsp;</p>

</div>
