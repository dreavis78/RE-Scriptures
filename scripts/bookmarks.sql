BEGIN TRANSACTION;
DROP TABLE IF EXISTS `bookmarks`;
CREATE TABLE IF NOT EXISTS `bookmarks` (
	`bookmark_id` INTEGER NOT NULL,
	`name`       TEXT NOT NULL,
	`volume_id`	 TEXT NOT NULL,
	`book_id`	 TEXT NOT NULL,
	`chapter`	 TEXT NOT NULL,
	`paragraph`	 INTEGER NOT NULL,
	PRIMARY KEY(`bookmark_id`)
);
COMMIT;
