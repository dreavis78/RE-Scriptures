BEGIN TRANSACTION;
DROP TABLE IF EXISTS `volumes`;
CREATE TABLE IF NOT EXISTS `volumes` (
	`volume_id`	TEXT NOT NULL UNIQUE,
	`name`	TEXT NOT NULL,
	`volume_order`	INTEGER NOT NULL,
	PRIMARY KEY(`volume_id`)
);
INSERT INTO `volumes` (volume_id,name,volume_order) VALUES ('ot','The Old Covenants',1),
 ('nt','The New Testament',2),
 ('bofm','The Book of Mormon',3),
 ('tc','Teachings & Commandments',4),
 ('st','Supplemental Teachings',5);
COMMIT;
