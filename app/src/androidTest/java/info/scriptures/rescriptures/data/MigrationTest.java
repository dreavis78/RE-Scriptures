package info.scriptures.rescriptures.data;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.room.testing.MigrationTestHelper;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import info.scriptures.rescriptures.data.ScriptureDatabase;
import info.scriptures.rescriptures.data.entity.BookmarkEntity;
import info.scriptures.rescriptures.util.LiveDataTestUtil;
import timber.log.Timber;

import static info.scriptures.rescriptures.data.ScriptureDatabase.MIGRATION_2_3;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class MigrationTest {

    private static final String DATABASE_NAME = "scriptures_v1.3.db";

    @Rule
    public MigrationTestHelper testHelper =
        new MigrationTestHelper(InstrumentationRegistry.getInstrumentation(),
                Objects.requireNonNull(ScriptureDatabase.class.getCanonicalName()),
            new FrameworkSQLiteOpenHelperFactory());

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    SupportSQLiteDatabase db;


    @Test
    public void testMigrationFrom2To3() throws IOException {
        db = testHelper.createDatabase(DATABASE_NAME, 2);
        insertBookmark(1, "Test Bookmark 1", "bofm", "ether", "2", 3, db);
        insertBookmark(2, "Test Bookmark 2", "nt", "mark", "3", 5, db);
        db.close();

        testHelper.runMigrationsAndValidate(DATABASE_NAME, 3, true, MIGRATION_2_3);
    }

    @Test
    public void testBookmarksPreservedAfterMigration() throws InterruptedException {
        LiveData<List<BookmarkEntity>> bmLiveData = getMigratedRoomDatabase().bookmarkModel().loadBookmarks();
        List<BookmarkEntity> bmList = LiveDataTestUtil.getValue(bmLiveData);

        BookmarkEntity bm1 = bmList.stream().filter(b -> b.getId() == 1).findFirst().orElse(null);
        assertNotNull(bm1);
        assertEquals(bm1.getId(), 1);
        assertEquals(bm1.getName(), "Test Bookmark 1");
        assertEquals(bm1.getVolumeId(), "bofm");
        assertEquals(bm1.getBookId(), "ether");
        assertEquals(bm1.getChapter(), "2");
        assertEquals(bm1.getParagraph(), 3);

        BookmarkEntity bm2 = bmList.stream().filter(b -> b.getId() == 2).findFirst().orElse(null);
        assertNotNull(bm2);
        assertEquals(bm2.getId(), 2);
        assertEquals(bm2.getName(), "Test Bookmark 2");
        assertEquals(bm2.getVolumeId(), "nt");
        assertEquals(bm2.getBookId(), "mark");
        assertEquals(bm2.getChapter(), "3");
        assertEquals(bm2.getParagraph(), 5);
    }

    private ScriptureDatabase getMigratedRoomDatabase() {
        ScriptureDatabase database = Room.databaseBuilder(ApplicationProvider.getApplicationContext(),
                ScriptureDatabase.class, DATABASE_NAME)
                .addMigrations(MIGRATION_2_3)
                .allowMainThreadQueries()
                .build();
        testHelper.closeWhenFinished(database);
        return database;
    }

    private void insertBookmark(int id, String name, String volume_id, String book_id, String chapter, int paragraph, SupportSQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put("bookmark_id", id);
        values.put("name", name);
        values.put("volume_id", volume_id);
        values.put("book_id", book_id);
        values.put("chapter", chapter);
        values.put("paragraph", paragraph);

        db.insert("bookmarks", SQLiteDatabase.CONFLICT_REPLACE, values);
    }


}
