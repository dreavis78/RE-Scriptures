package info.scriptures.rescriptures.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;
import java.util.List;

import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.entity.BookmarkEntity;

public class BookmarkViewModel extends AndroidViewModel {

    private final ScriptureRepository repository;
    private final LiveData<List<BookmarkEntity>> bookmarks;

    // Bookmarks on the current chapter
    private List<BookmarkEntity> activeBookmarks = new ArrayList<>();

    private BookmarkViewModel(Application application, ScriptureRepository repository) {
        super(application);
        this.repository = repository;
        bookmarks = repository.loadBookmarks();
    }

    public LiveData<List<BookmarkEntity>> getBookmarks() { return bookmarks; }

    public void addBookmark(String name) {
        BookmarkEntity newBookmark = new BookmarkEntity();
        newBookmark.setName(name);
        newBookmark.setVolumeId(repository.getSelectedVolume());
        newBookmark.setBookId(repository.getSelectedBook());
        newBookmark.setChapter(repository.getSelectedChapter());
        newBookmark.setParagraph(repository.getSelectedParagraph());
        repository.addBookmark(newBookmark);
    }

    public void removeBookmark(int id) {
        repository.removeBookmark(id);
    }

    /*
     * Creates list of bookmarks on the current chapter, to be used by the
     * isBookmarked method later as the user scrolls.
     */
    public void filterActiveBookmarks(String bookId, String chapter) {
        this.activeBookmarks.clear();
        if (bookmarks.getValue() != null) {
            for (BookmarkEntity bookmarkEntity : bookmarks.getValue()) {
                if (bookmarkEntity.getBookId().equals(bookId) &&
                        bookmarkEntity.getChapter().equals(chapter)
                ) {
                    this.activeBookmarks.add(bookmarkEntity);
                }
            }
        }
    }

    public boolean isBookmarked(int paragraph) {
        for (BookmarkEntity bookmarkEntity : this.activeBookmarks) {
            if ( bookmarkEntity.getParagraph() == paragraph ) {
                return true;
            }
        }

        return false;
    }

    public static class BookmarkViewModelFactory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;
        private final ScriptureRepository repository;

        public BookmarkViewModelFactory(@NonNull Application application, ScriptureRepository repository) {
            this.application = application;
            this.repository = repository;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            //noinspection unchecked
            return (T) new BookmarkViewModel(application, repository);
        }
    }
}
