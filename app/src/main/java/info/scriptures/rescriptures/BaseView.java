package info.scriptures.rescriptures;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
