package info.scriptures.rescriptures.util;

import android.app.Activity;
import android.util.Base64;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import info.scriptures.rescriptures.R;
import info.scriptures.rescriptures.ui.reader.ReaderPresenter;
import timber.log.Timber;

public class ScriptureWebViewClient extends WebViewClient {

    private Activity activity;
    private WebView webView;
    private ReaderPresenter presenter;
    private int paragraph;

    private float percentScrolled;
    private final int SCROLL_DELAY = 700;

    public ScriptureWebViewClient(Activity activity, WebView webView, ReaderPresenter presenter) {
        this.activity = activity;
        this.webView = webView;
        this.presenter = presenter;
        this.paragraph = 0;
        this.percentScrolled = 0.0f;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        injectCSS();
        finish();

        if (this.percentScrolled > 0.0f) {
            restoreScrollState(view);
        }
        else if (this.paragraph > 0) {
            webView.evaluateJavascript("javascript:(function() {" +
                    "setTimeout(() => {" +
                    "   scrollToParagraph(" + this.paragraph + ");" +
                    "}, 400);" +
                "})()", null);
        }
    }

    @Override
    public void onLoadResource(WebView view, String text) {
        super.onLoadResource(view, text);
    }

    public void setParagraph(int paragraph) {
        this.paragraph = paragraph;
    }

    public void setScrollPosition(float percentScrolled) {
        this.percentScrolled = percentScrolled;
    }

    public void scrollToParagraph() {
        webView.evaluateJavascript("scrollToParagraph(" + this.paragraph + ")", s -> this.paragraph = 0);
    }

    // Inject CSS method: read style.css from assets folder
    // Append stylesheet to document head
    private void injectCSS() {
        try {
            StringBuilder buffer = new StringBuilder();
            InputStream inputStream = this.activity.getAssets().open("style.css");
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String str;

            while ((str=in.readLine()) != null) {
                buffer.append(str);
            }
            in.close();
            inputStream.close();

            String fontFamily = ReadingSettingsService.getFont();
            String fontSize = Integer.toString(ReadingSettingsService.getTextSize());
            String colorScheme = ReadingSettingsService.getColorScheme();
            String backgroundColor, textColor;
            String bodyClass;
            switch(colorScheme) {
                case ThemeUtil.THEME_DARK:
                    backgroundColor = String.format("#%06X", (0xFFFFFF & this.activity.getResources().getColor(R.color.darkSchemeBackground)));
                    textColor = String.format("#%06X", (0xFFFFFF & this.activity.getResources().getColor(R.color.darkSchemeText)));
                    bodyClass = "dark";
                    break;
                case ThemeUtil.THEME_LIGHT:
                    backgroundColor = String.format("#%06X", (0xFFFFFF & this.activity.getResources().getColor(R.color.lightSchemeBackground)));
                    textColor = String.format("#%06X", (0xFFFFFF & this.activity.getResources().getColor(R.color.lightSchemeText)));
                    bodyClass = "light";
                    break;
                case ThemeUtil.THEME_SEPIA:
                default:
                    backgroundColor = String.format("#%06X", (0xFFFFFF & this.activity.getResources().getColor(R.color.sepiaSchemeBackground)));
                    textColor = String.format("#%06X", (0xFFFFFF & this.activity.getResources().getColor(R.color.sepiaSchemeText)));
                    bodyClass = "sepia";
            }

            String bodyStyles = this.activity.getString(
                    R.string.reader_styles,
                    fontFamily,
                    fontSize,
                    backgroundColor,
                    textColor
            );
            String css = bodyStyles + buffer.toString();

            String encoded = Base64.encodeToString(css.getBytes(), Base64.NO_WRAP);
            webView.evaluateJavascript("javascript:(function() {" +
                    "var parent = document.getElementsByTagName('head').item(0);" +
                    "var style = document.createElement('style');" +
                    "style.type = 'text/css';" +
                    // Tell the browser to BASE64-decode the string into your script !!!
                    "style.innerHTML = window.atob('" + encoded + "');" +
                    "parent.appendChild(style)" +
                    "})()", null);

            webView.evaluateJavascript("javascript:(function() {" +
                    "document.body.className = '" + bodyClass +"';" +
                    "})()", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void restoreScrollState(WebView view) {
        // Delay the scrollTo to make it work
        view.postDelayed(() -> {
            float webViewSize = view.getContentHeight() - view.getTop();
            float positionInWV = webViewSize * this.percentScrolled;
            int positionY = Math.round(view.getTop() + positionInWV);
            view.scrollTo(0, positionY);
            this.percentScrolled = 0.0f;
        }, SCROLL_DELAY);
    }

    private void finish() {
        webView.evaluateJavascript("javascript:(function() {" +
                "document.getElementsByTagName('body').item(0).style.display = '';" +
                "})()", null);
    }
}
