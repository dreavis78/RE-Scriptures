package info.scriptures.rescriptures.util;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.Editable;
import android.text.Layout;
import android.text.style.AlignmentSpan;
import android.text.style.LeadingMarginSpan;

import org.xml.sax.Attributes;

import info.scriptures.rescriptures.R;
import timber.log.Timber;

import static android.provider.Settings.Global.getString;

public class ScriptureTagHandler implements HtmlParser.TagHandler {
    private String tagId;
    private String align;

    @Override
    public boolean handleTag(final boolean opening, final String tag, Editable output, final Attributes attributes) {
        if (tag.equals("li")) {
            if (opening) {
                tagId = HtmlParser.getValue(attributes, "id");
            } else {
                handleListTag(output, tagId);
            }
        } else {
            if (opening && attributes != null) {
                align = attributes.getValue("align");
            } else {
                if (align != null && align.equals("center")) {
                    handleCentering(output);
                    align = null;
                }
            }
        }
        return false;
    }

    private void handleListTag(Editable output, String id) {

        output.append("\n");
        String[] split = output.toString().split("\n");

        int lastIndex = split.length - 1;
//        int start = output.length() - split[lastIndex].length() - 1;
        int start = 0;
        output.setSpan(new LeadingMarginSpan.Standard(15), start, output.length(), 0);
//        output.setSpan(new ScriptureLeadingMarginSpan(15), start, output.length(), 0);
    }

    private void handleCentering(Editable output) {
        output.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, output.length(), 0);
    }

    class ScriptureLeadingMarginSpan extends LeadingMarginSpan.Standard {

        public ScriptureLeadingMarginSpan(int every) {
            super(every);
        }

        @Override
        public int getLeadingMargin(boolean first) {
            return " ".length() * 50;
        }

        @Override
        public void drawLeadingMargin(Canvas c, Paint p, int x, int dir, int top, int baseline, int bottom, CharSequence text, int start, int end, boolean first, Layout layout) {
//                Paint.Style orgStyle = p.getStyle();
//                p.setStyle(Paint.Style.FILL);
//                c.drawText("* ", 0, bottom - p.descent(), p);
//                p.setStyle(orgStyle);
        }
    }
}
