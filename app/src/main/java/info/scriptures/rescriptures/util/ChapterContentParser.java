package info.scriptures.rescriptures.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;

import timber.log.Timber;

public class ChapterContentParser {

    public List<Element> parseTagsFromString(String html) {
        Document doc = Jsoup.parseBodyFragment(html);
        Element ol = doc.getElementsByClass("simple-list").first();
        if (ol == null) {
            ol = doc.getElementsByClass("simple-text").first();
        }
        Elements children = ol.children();
        for(Element el: children) {
            centerHeaderElement(el);
            appendVerseNumber(el);
        }
        return children;
    }

    private void appendVerseNumber(Element el) {
        StringBuilder sb;
        Elements listItems = el.select("li[id]");

        for (Element li : listItems) {
            String id = li.id();
            sb = new StringBuilder();
            sb.append(id);
            sb.append("&nbsp;&nbsp;&nbsp;&nbsp;");
            li.prepend(sb.toString());
        }
    }

    private void centerHeaderElement(Element el) {
        el.select("[align]").attr("style", "text-align: center");
    }
}
