package info.scriptures.rescriptures.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import info.scriptures.rescriptures.R;

public class ReadingSettingsService {

    private static SharedPreferences prefs;
    private static Resources res;

    public static void init(Context context) {
        if (prefs == null) {
            prefs = PreferenceManager.getDefaultSharedPreferences(context);
            res = context.getResources();
        }
    }

    public static String getFont() {
        return prefs.getString("font", res.getString(R.string.pref_default_font));
    }

    public static String getColorScheme() {
        return prefs.getString("colorScheme", res.getString(R.string.pref_default_color_scheme));
    }

    public static int getTextSize() {
        int textSizeValue = prefs.getInt("textSize", res.getInteger(R.integer.pref_default_text_size));
        int minVal = res.getInteger(R.integer.pref_min_text_size);
        int range = res.getInteger(R.integer.pref_max_text_size) - minVal;
        return Math.round(minVal + (range * ((float) textSizeValue / 100)));
    }

    public static int getMargin() {
        int marginValue = prefs.getInt("margin", res.getInteger(R.integer.pref_default_margin));
        int minVal = res.getInteger(R.integer.pref_min_margin);
        int range = res.getInteger(R.integer.pref_max_margin) - minVal;
        return Math.round(minVal + (range * ((float) marginValue / 100)));
    }

    public static int getLineSpacing() {
        int lineSpacingValue = prefs.getInt("lineSpacing", res.getInteger(R.integer.pref_default_line_spacing));
        int minVal = res.getInteger(R.integer.pref_min_line_spacing);
        int range = res.getInteger(R.integer.pref_max_line_spacing) - minVal;
        return Math.round(minVal + (range * ((float) lineSpacingValue / 100)));
    }

    public static String getVoiceGender() {
        return prefs.getString("voice_gender", res.getString(R.string.pref_tts_female_gender));
    }

    public static String getVoiceLocale() {
        return prefs.getString("voice_locale", res.getString(R.string.pref_tts_us_locale));
    }

    public static String getVoiceSpeed() {
        return prefs.getString("voice_speed", res.getString(R.string.pref_default_voice_speed));
    }
}
