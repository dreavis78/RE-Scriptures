package info.scriptures.rescriptures.util;

import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.view.View;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import info.scriptures.rescriptures.ui.reader.ReaderActivity;
import timber.log.Timber;
//import timber.log.Timber;

public class PlayAudioTask extends AsyncTask<String, Void, Integer> {
    private WeakReference<ReaderActivity> reference;

    public PlayAudioTask(ReaderActivity context) {
        reference = new WeakReference<>(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        ReaderActivity activity = reference.get();
        activity.loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    protected final Integer doInBackground(String... params) {
        String[] chunks = formatToSpeech(params[0], Integer.parseInt(params[1]), TextToSpeech.getMaxSpeechInputLength() - 1);
        ReaderActivity activity = reference.get();

        for (int i = 0; i < chunks.length; i++ ) {
            int QueueType = (i == 0) ? TextToSpeech.QUEUE_FLUSH : TextToSpeech.QUEUE_ADD;
            activity.tts.speak(chunks[i], QueueType, null, i+"");
        }
        return chunks.length;
    }

    @Override
    protected void onPostExecute(Integer result) {
        ReaderActivity activity = reference.get();
        if (activity == null || activity.isFinishing()) return;

        activity.chunkSize = result;
    }

    private String[] formatToSpeech(String html, int startPosition, int maxLength) {
        Pattern pattern = Pattern.compile(".{1,299}(?:[.!?]\\s+|\\n|$)", Pattern.DOTALL);
        Document doc = Jsoup.parse(html);

        // Remove superscript tags & footnotes
        Elements xtraSelector = doc.select("sup");
        xtraSelector.addAll(doc.select(".footnotes"));
        for (Element e : xtraSelector) { e.remove(); }

        // Return list of paragraphs
        List<String> paragraphs = new ArrayList<>();
        Elements selector;

        if (startPosition > 0) {
            selector = doc.select("li#" + startPosition).prev().nextAll();
            if (selector.size() == 0) {
                // minus one for O-based index, and another minus one to include paragraph at startPosition
                selector = doc.select("p").eq(startPosition - 1).nextAll();
            }
        } else {
            selector = doc.select("h1,h2,h3,h4,p,li");
        }

        for (Element e : selector) {
            String p = e.text();
            if (p.length() > maxLength) {
                Matcher matcher = pattern.matcher(p);
                while (matcher.find()) {
                    paragraphs.add(e.text());
                }
            } else {
                paragraphs.add(e.text());
            }
        }

        return paragraphs.toArray(new String[]{});
    }
}
