package info.scriptures.rescriptures.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScriptureReferenceService {

    public static List<String> unnumbered_books = new ArrayList<>(Arrays.asList(
        "ocforeword",
        "occanonization",
        "ocpreface",
        "ncforeword",
        "nccanonization",
        "ntpreface",
        "bompreface",
        "bomintro",
        "title",
        "tow",
        "tcforeword",
        "tccanonization",
        "tcpreface",
        "tcintro",
        "epigraph",
        "glossary",
        "podcast"
    ));

    public static List<String> unnumbered_chapters = new ArrayList<>(Arrays.asList(
        "preface",
        "excludedrevelations",
        "timeline",
        "fac1",
        "fac2",
        "fac3"
    ));

    public static String getPrettyScriptureReference(
        String book, String chapter, int paragraph ) {
        StringBuilder s = new StringBuilder();
        s.append(getLabel(book, chapter));
        s = getParagraph(book, chapter, paragraph, s);

        return s.toString();
    }

    private static String getLabel(String book, String chapter) {
        if (book.equals("glossary")) {
            return "Glossary - " + ShorthandTitlesService.getShorthandLabel(book, chapter);
        } else if (book.equals("podcast")) {
            return "Podcast - " + ShorthandTitlesService.getShorthandTitle(book, chapter);
        } else if (book.equals("appendix")) {
            return ShorthandTitlesService.getShorthandLabel(book, chapter);
        } else {
            return ShorthandTitlesService.getShorthandTitle(book, chapter);
        }
    }

    private static StringBuilder getParagraph(String book, String chapter, int paragraph, StringBuilder s) {
        if (paragraph > 0) {
            if ( unnumbered_books.contains(book) || unnumbered_chapters.contains(chapter) ) {
                s.append(", para. ");
            } else {
                s.append(":");
            }

            s.append(paragraph);
        }

        return s;
    }
}
