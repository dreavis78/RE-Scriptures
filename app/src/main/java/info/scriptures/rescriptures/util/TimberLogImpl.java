package info.scriptures.rescriptures.util;

import info.scriptures.rescriptures.BuildConfig;
import timber.log.Timber;

public class TimberLogImpl {

    public static void init() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new ReleaseTimberTree());
        }
    }
}
