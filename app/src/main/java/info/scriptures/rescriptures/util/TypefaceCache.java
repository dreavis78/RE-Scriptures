package info.scriptures.rescriptures.util;

import android.graphics.Typeface;

import java.util.Hashtable;

public class TypefaceCache {
    private static final String SANS_SERIF = "sans-serif";
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>() {{
        put(Typeface.SERIF.toString(), Typeface.create(Typeface.SERIF, Typeface.NORMAL));
        put(Typeface.SANS_SERIF.toString(), Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL));
    }};

    public static Typeface get(String name) {
        if (name.equals(SANS_SERIF)) {
            return cache.get(Typeface.SANS_SERIF.toString());
        } else {
            return cache.get(Typeface.SERIF.toString());
        }
    }
}
