package info.scriptures.rescriptures.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class ShorthandTitlesService {

    private static Map<String, String> shorthandVolumes;
    static {
        shorthandVolumes = new HashMap<>();
        shorthandVolumes.put("oc", "Old Covenants");
        shorthandVolumes.put("nt", "New Testament");
        shorthandVolumes.put("bofm", "Book of Mormon");
        shorthandVolumes.put("tc", "Teachings & Commandments");
        shorthandVolumes.put("st", "Supplemental Teachings");
    }

    private static Map<String, String> shorthandLabels;
    static {
        shorthandLabels = new HashMap<>();
        shorthandLabels.put("ocforeword", "Foreword");
        shorthandLabels.put("occanonization", "Canonization");
        shorthandLabels.put("ocpreface", "Preface");
        shorthandLabels.put("genesis", "Genesis");
        shorthandLabels.put("exodus", "Exodus");
        shorthandLabels.put("leviticus", "Leviticus");
        shorthandLabels.put("numbers", "Numbers");
        shorthandLabels.put("deuteronomy", "Deuteronomy");
        shorthandLabels.put("joshua", "Joshua");
        shorthandLabels.put("judges", "Judges");
        shorthandLabels.put("ruth", "Ruth");
        shorthandLabels.put("1samuel", "1 Samuel");
        shorthandLabels.put("2samuel", "2 Samuel");
        shorthandLabels.put("1kings", "1 Kings");
        shorthandLabels.put("2kings", "2 Kings");
        shorthandLabels.put("1chronicles", "1 Chronicles");
        shorthandLabels.put("2chronicles", "2 Chronicles");
        shorthandLabels.put("ezra", "Ezra");
        shorthandLabels.put("nehemiah", "Nehemiah");
        shorthandLabels.put("esther", "Esther");
        shorthandLabels.put("job", "Job");
        shorthandLabels.put("psalm", "Psalms");
        shorthandLabels.put("proverbs", "Proverbs");
        shorthandLabels.put("ecclesiastes", "Ecclesiastes");
        shorthandLabels.put("isaiah", "Isaiah");
        shorthandLabels.put("jeremiah", "Jeremiah");
        shorthandLabels.put("lamentations", "Lamentations");
        shorthandLabels.put("ezekiel", "Ezekiel");
        shorthandLabels.put("daniel", "Daniel");
        shorthandLabels.put("hosea", "Hosea");
        shorthandLabels.put("joel", "Joel");
        shorthandLabels.put("amos", "Amos");
        shorthandLabels.put("obadiah", "Obadiah");
        shorthandLabels.put("jonah", "Jonah");
        shorthandLabels.put("micah", "Micah");
        shorthandLabels.put("nahum", "Nahum");
        shorthandLabels.put("habakkuk", "Habakkuk");
        shorthandLabels.put("zephaniah", "Zephaniah");
        shorthandLabels.put("haggai", "Haggai");
        shorthandLabels.put("zechariah", "Zechariah");
        shorthandLabels.put("malachi", "Malachi");

        shorthandLabels.put("ncforeword", "Foreword");
        shorthandLabels.put("nccanonization", "Canonization");
        shorthandLabels.put("ntpreface", "Preface");
        shorthandLabels.put("matthew", "Matthew");
        shorthandLabels.put("mark", "Mark");
        shorthandLabels.put("luke", "Luke");
        shorthandLabels.put("john", "John");
        shorthandLabels.put("acts", "Acts of The Apostles");
        shorthandLabels.put("romans", "Romans");
        shorthandLabels.put("1corinthians", "1 Corinthians");
        shorthandLabels.put("2corinthians", "2 Corinthians");
        shorthandLabels.put("galatians", "Galatians");
        shorthandLabels.put("ephesians", "Ephesians");
        shorthandLabels.put("philippians", "Philippians");
        shorthandLabels.put("colossians", "Colossians");
        shorthandLabels.put("1thessalonians", "1 Thessalonians");
        shorthandLabels.put("2thessalonians", "2 Thessalonians");
        shorthandLabels.put("1timothy", "1 Timothy");
        shorthandLabels.put("2timothy", "2 Timothy");
        shorthandLabels.put("titus", "Titus");
        shorthandLabels.put("philemon", "Philemon");
        shorthandLabels.put("hebrews", "Hebrews");
        shorthandLabels.put("1jacob", "Jacob");
        shorthandLabels.put("1peter", "1 Peter");
        shorthandLabels.put("2peter", "2 Peter");
        shorthandLabels.put("1john", "1 John");
        shorthandLabels.put("2john", "2 John");
        shorthandLabels.put("3john", "3 John");
        shorthandLabels.put("judas", "Judas");
        shorthandLabels.put("revelation", "Revelation");

        shorthandLabels.put("bompreface", "Preface");
        shorthandLabels.put("bomintro", "Introduction");
        shorthandLabels.put("title", "Dedication");
        shorthandLabels.put("tow", "The Testimonies of Witnesses");
        shorthandLabels.put("1nephi", "1 Nephi");
        shorthandLabels.put("2nephi", "2 Nephi");
        shorthandLabels.put("jacob", "Jacob");
        shorthandLabels.put("enos", "Enos");
        shorthandLabels.put("jarom", "Jarom");
        shorthandLabels.put("omni", "Omni");
        shorthandLabels.put("words", "Words of Mormon");
        shorthandLabels.put("mosiah", "Mosiah");
        shorthandLabels.put("alma", "Alma");
        shorthandLabels.put("helaman", "Helaman");
        shorthandLabels.put("3nephi", "3 Nephi");
        shorthandLabels.put("4nephi", "4 Nephi");
        shorthandLabels.put("mormon", "Mormon");
        shorthandLabels.put("ether", "Ether");
        shorthandLabels.put("moroni", "Moroni");

        shorthandLabels.put("tcforeword", "Foreword");
        shorthandLabels.put("tccanonization", "Canonization");
        shorthandLabels.put("tcpreface", "Preface");
        shorthandLabels.put("tcintro", "Introduction");
        shorthandLabels.put("epigraph", "Epigraph");
        shorthandLabels.put("section", "Sections");
        shorthandLabels.put("jshistory", "Joseph Smith History");
        shorthandLabels.put("lecture", "Lectures on Faith");
        shorthandLabels.put("abraham", "Abraham");
        shorthandLabels.put("toj", "Testimony of St. John");
        shorthandLabels.put("appendix", "Appendix");
        shorthandLabels.put("timeline", "Timeline of the Fathers");
        shorthandLabels.put("excludedrevelations", "Excluded Revelations");
        shorthandLabels.put("prerogative", "A Prophet's Prerogative");
        shorthandLabels.put("glossary", "Glossary of Terms");
        shorthandLabels.put("maps", "Maps");
        shorthandLabels.put("correlationtable", "Correlation Table");
        shorthandLabels.put("intro","Introduction");
        shorthandLabels.put("podcast", "Denver Snuffer Podcast");
        shorthandLabels.put("preface","Preface"); // Lectures on Faith Preface
        shorthandLabels.put("aaronic-priesthood", "Aaronic Priesthood");
        shorthandLabels.put("abomination", "Abomination");
        shorthandLabels.put("accountability-age-of", "Accountability, Age of");
        shorthandLabels.put("accountability", "Accountability");
        shorthandLabels.put("accuse", "Accuse");
        shorthandLabels.put("adam-ondi-ahman", "Adam-Ondi-Ahman");
        shorthandLabels.put("adam", "Adam");
        shorthandLabels.put("adoption", "Adoption");
        shorthandLabels.put("adultery", "Adultery");
        shorthandLabels.put("agency", "Agency");
        shorthandLabels.put("ancient-of-days", "Ancient of Days");
        shorthandLabels.put("angel-of-light", "Angel of Light");
        shorthandLabels.put("angel", "Angel");
        shorthandLabels.put("anger", "Anger");
        shorthandLabels.put("anti-christ", "Anti-Christ");
        shorthandLabels.put("apostasy", "Apostasy");
        shorthandLabels.put("apostle", "Apostle");
        shorthandLabels.put("archangels-the-four", "Archangels, the Four");
        shorthandLabels.put("ascension", "Ascension");
        shorthandLabels.put("ask-seek-knock", "Ask, Seek, Knock");
        shorthandLabels.put("ask", "Ask");
        shorthandLabels.put("atonement", "Atonement");
        shorthandLabels.put("attain-to-the-resurrection-of-the-dead", "Attain to the Resurrection of the Dead");
        shorthandLabels.put("authoritative", "Authoritative");
        shorthandLabels.put("awake-and-arise", "Awake and Arise");
        shorthandLabels.put("babylon", "Babylon");
        shorthandLabels.put("baptism-of-fire-and-the-holy-ghost", "Baptism of Fire and the Holy Ghost");
        shorthandLabels.put("baptism", "Baptism");
        shorthandLabels.put("beast", "Beast");
        shorthandLabels.put("become-as-a-little-child", "Become as a Little Child");
        shorthandLabels.put("becoming-one", "Becoming One");
        shorthandLabels.put("belief", "Belief");
        shorthandLabels.put("bishop", "Bishop");
        shorthandLabels.put("blessed", "Blessed");
        shorthandLabels.put("blessings-of-the-fathers", "Blessings of the Fathers");
        shorthandLabels.put("blessings", "Blessings");
        shorthandLabels.put("blood-crying-for-vengeance", "Blood Crying for Vengeance");
        shorthandLabels.put("book-of-mormon-as-covenant", "Book of Mormon as Covenant");
        shorthandLabels.put("book-of-the-lamb-of-god", "Book of the Lamb of God");
        shorthandLabels.put("bowels", "Bowels");
        shorthandLabels.put("branch", "Branch");
        shorthandLabels.put("broken-heart---contrite-spirit", "Broken Heart - Contrite Spirit");
        shorthandLabels.put("call-ed-ing", "Call/ed/ing");
        shorthandLabels.put("call-upon-god", "Call Upon God");
        shorthandLabels.put("calling-and-election", "Calling and Election");
        shorthandLabels.put("cast-out", "Cast Out");
        shorthandLabels.put("casting-pearls-before-swine", "Casting Pearls Before Swine");
        shorthandLabels.put("cephas", "Cephas");
        shorthandLabels.put("ceremony-of-recognition", "Ceremony of Recognition");
        shorthandLabels.put("chains-of-hell", "Chains of Hell");
        shorthandLabels.put("charity", "Charity");
        shorthandLabels.put("cherubim", "Cherubim");
        shorthandLabels.put("children-of-god", "Children of God");
        shorthandLabels.put("children-of-the-prophets", "Children of the Prophets");
        shorthandLabels.put("chosen-chosen-people", "Chosen Chosen People");
        shorthandLabels.put("chosen-vessel", "Chosen Vessel");
        shorthandLabels.put("christian", "Christian");
        shorthandLabels.put("church-of-the-firstborn", "Church of the Firstborn");
        shorthandLabels.put("church-of-the-lamb", "Church of the Lamb");
        shorthandLabels.put("church", "Church");
        shorthandLabels.put("coat-of-many-colors", "Coat of Many Colors");
        shorthandLabels.put("comforter-the-second", "Comforter, The Second");
        shorthandLabels.put("comforter-the", "Comforter, The");
        shorthandLabels.put("commandment", "Commandment");
        shorthandLabels.put("common-consent", "Common Consent");
        shorthandLabels.put("condemnation-to-remove", "Condemnation, To Remove");
        shorthandLabels.put("consecration", "Consecration");
        shorthandLabels.put("contention", "Contention");
        shorthandLabels.put("continuation-of-seed", "Continuation of Seed");
        shorthandLabels.put("covenant-everlasting", "Covenant, Everlasting");
        shorthandLabels.put("covenant", "Covenant");
        shorthandLabels.put("cry-unto-the-lord", "Cry Unto the Lord");
        shorthandLabels.put("curse-ing", "Curse/ing");
        shorthandLabels.put("damned", "Damned");
        shorthandLabels.put("day", "Day");
        shorthandLabels.put("deacon", "Deacon");
        shorthandLabels.put("deny-the-holy-ghost", "Deny the Holy Ghost");
        shorthandLabels.put("destroy", "Destroy");
        shorthandLabels.put("disciple", "Disciple");
        shorthandLabels.put("dispensation-of-the-fullness-of-time", "Dispensation of the Fullness of Time");
        shorthandLabels.put("dispensation", "Dispensation");
        shorthandLabels.put("disputation", "Disputation");
        shorthandLabels.put("dives", "Dives");
        shorthandLabels.put("doctrine-of-christ", "Doctrine of Christ");
        shorthandLabels.put("doctrine-of-the-two-ways", "Doctrine of the Two Ways");
        shorthandLabels.put("dominion", "Dominion");
        shorthandLabels.put("dominions", "Dominions");
        shorthandLabels.put("dragon", "Dragon");
        shorthandLabels.put("dreams", "Dreams");
        shorthandLabels.put("dwindle-in-unbelief", "Dwindle in Unbelief");
        shorthandLabels.put("elder", "Elder");
        shorthandLabels.put("elect", "Elect");
        shorthandLabels.put("elias-elijah-messiah", "Elias, Elijah, Messiah");
        shorthandLabels.put("elias", "Elias");
        shorthandLabels.put("end-the", "End, The");
        shorthandLabels.put("endowment", "Endowment");
        shorthandLabels.put("endure-to-the-end", "Endure To the End");
        shorthandLabels.put("eternal-life", "Eternal Life");
        shorthandLabels.put("eternity", "Eternity");
        shorthandLabels.put("eve", "Eve");
        shorthandLabels.put("exaltation", "Exaltation");
        shorthandLabels.put("excess", "Excess");
        shorthandLabels.put("extortion", "Extortion");
        shorthandLabels.put("eye-of-faith", "Eye of Faith");
        shorthandLabels.put("faith-hope-and-charity", "Faith, Hope, and Charity");
        shorthandLabels.put("faith", "Faith");
        shorthandLabels.put("fall-of-man", "Fall of Man");
        shorthandLabels.put("false-christ", "False Christ");
        shorthandLabels.put("false-prophet", "False Prophet");
        shorthandLabels.put("false-spirit", "False Spirit");
        shorthandLabels.put("false-traditions", "False Traditions");
        shorthandLabels.put("fasting", "Fasting");
        shorthandLabels.put("father-of-many-nations", "Father of Many Nations");
        shorthandLabels.put("fathers-the", "Fathers, The");
        shorthandLabels.put("fear", "Fear");
        shorthandLabels.put("fellowship", "Fellowship");
        shorthandLabels.put("first-presidency", "First Presidency");
        shorthandLabels.put("first-principles-of-the-gospel", "First Principles of the Gospel");
        shorthandLabels.put("follow-christ", "Follow Christ");
        shorthandLabels.put("for-ever", "For Ever");
        shorthandLabels.put("forgetting", "Forgetting");
        shorthandLabels.put("forgiveness", "Forgiveness");
        shorthandLabels.put("form-of-godliness", "Form of Godliness");
        shorthandLabels.put("froward", "Froward");
        shorthandLabels.put("fruit", "Fruit");
        shorthandLabels.put("full-of-love", "Full of Love");
        shorthandLabels.put("fullness-of-the-gospel", "Fullness of the Gospel");
        shorthandLabels.put("fullness-of-the-priesthood", "Fullness of the Priesthood");
        shorthandLabels.put("fullness-of-the-scriptures", "Fullness of the Scriptures");
        shorthandLabels.put("fullness", "Fullness");
        shorthandLabels.put("garments-washed-white", "Garments Washed White");
        shorthandLabels.put("gathering-of-israel", "Gathering of Israel");
        shorthandLabels.put("generation", "Generation");
        shorthandLabels.put("gentiles", "Gentiles");
        shorthandLabels.put("gift-of-the-holy-ghost", "Gift of the Holy Ghost");
        shorthandLabels.put("gift-of-tongues", "Gift of Tongues");
        shorthandLabels.put("gifts-of-the-spirit", "Gifts of the Spirit");
        shorthandLabels.put("glory", "Glory");
        shorthandLabels.put("god-of-abraham-god-of-isaac-god-of-jacob", "God of Abraham, God of Isaac, God of Jacob");
        shorthandLabels.put("godliness", "Godliness");
        shorthandLabels.put("gospel-of-jesus-christ", "Gospel of Jesus Christ");
        shorthandLabels.put("government-of-god", "Government of God");
        shorthandLabels.put("grace", "Grace");
        shorthandLabels.put("great-and-abominable-church", "Great and Abominable Church");
        shorthandLabels.put("great-knowledge-and-greater-knowledge", "Great Knowledge and Greater Knowledge");
        shorthandLabels.put("hades", "Hades");
        shorthandLabels.put("handmaid", "Handmaid");
        shorthandLabels.put("hardness-of-heart", "Hardness of Heart");
        shorthandLabels.put("hearts-turned-to-the-fathers", "Hearts Turned To the Fathers");
        shorthandLabels.put("heavenly-gift", "Heavenly Gift");
        shorthandLabels.put("heavenly-host", "Heavenly Host");
        shorthandLabels.put("heed-and-diligence", "Heed and Diligence");
        shorthandLabels.put("hell", "Hell");
        shorthandLabels.put("holiness", "Holiness");
        shorthandLabels.put("holy-ghost", "Holy Ghost");
        shorthandLabels.put("holy-order", "Holy Order");
        shorthandLabels.put("holy-spirit-of-promise", "Holy Spirit of Promise");
        shorthandLabels.put("holy-spirit", "Holy Spirit");
        shorthandLabels.put("honor", "Honor");
        shorthandLabels.put("hope", "Hope");
        shorthandLabels.put("host-s", "Host(s)");
        shorthandLabels.put("house-of-god", "House of God");
        shorthandLabels.put("house-of-israel", "House of Israel");
        shorthandLabels.put("house-of-order", "House of Order");
        shorthandLabels.put("how-great-things", "How Great Things");
        shorthandLabels.put("humility", "Humility");
        shorthandLabels.put("idol", "Idol");
        shorthandLabels.put("if-you-love-me", "If You Love Me");
        shorthandLabels.put("ignorance", "Ignorance");
        shorthandLabels.put("image-of-god", "Image of God");
        shorthandLabels.put("iniquity", "Iniquity");
        shorthandLabels.put("intelligence-s", "Intelligence/s");
        shorthandLabels.put("intercession", "Intercession");
        shorthandLabels.put("intro", "Intro");
        shorthandLabels.put("iron-rod", "Iron Rod");
        shorthandLabels.put("isle-of-the-sea", "Isle of the Sea");
        shorthandLabels.put("jacobs-ladder", "Jacob's Ladder");
        shorthandLabels.put("jacobs-wrestle-with-the-angel", "Jacob's Wrestle with the Angel");
        shorthandLabels.put("jesus-christ-as-the-father", "Jesus Christ as the Father");
        shorthandLabels.put("judge-ment", "Judge/ment");
        shorthandLabels.put("justification", "Justification");
        shorthandLabels.put("key-s", "Key(s)");
        shorthandLabels.put("keys-of-the-kingdom", "Keys of the Kingdom");
        shorthandLabels.put("kingdom-of-god", "Kingdom of God");
        shorthandLabels.put("kingdoms", "Kingdoms");
        shorthandLabels.put("know-ledge", "Know/ledge");
        shorthandLabels.put("know-the-lord", "Know the Lord");
        shorthandLabels.put("lake-of-fire-and-brimstone", "Lake of Fire and Brimstone");
        shorthandLabels.put("law-of-christ", "Law of Christ");
        shorthandLabels.put("law-of-moses", "Law of Moses");
        shorthandLabels.put("liberally", "Liberally");
        shorthandLabels.put("light-mindedness", "Light-mindedness");
        shorthandLabels.put("light-of-christ", "Light of Christ");
        shorthandLabels.put("light", "Light");
        shorthandLabels.put("living-water", "Living Water");
        shorthandLabels.put("lords-anointed", "Lord's Anointed");
        shorthandLabels.put("lords-supper", "Lord's Supper");
        shorthandLabels.put("love", "Love");
        shorthandLabels.put("lucifer", "Lucifer");
        shorthandLabels.put("maketh-flesh-his-arm", "Maketh Flesh His Arm");
        shorthandLabels.put("mansion", "Mansion");
        shorthandLabels.put("marriage", "Marriage");
        shorthandLabels.put("martyr", "Martyr");
        shorthandLabels.put("mary-the-mother-of-christ", "Mary, the Mother of Christ");
        shorthandLabels.put("meekness", "Meekness");
        shorthandLabels.put("melchizedek-priesthood", "Melchizedek Priesthood");
        shorthandLabels.put("melchizedek", "Melchizedek");
        shorthandLabels.put("mercy", "Mercy");
        shorthandLabels.put("ministering-angel", "Ministering Angel");
        shorthandLabels.put("more-sure-word-of-prophecy", "More Sure Word of Prophecy");
        shorthandLabels.put("mormon", "Mormon");
        shorthandLabels.put("most-holy", "Most Holy");
        shorthandLabels.put("mothers", "Mothers");
        shorthandLabels.put("mutual-agreement", "Mutual Agreement");
        shorthandLabels.put("mysteries-of-god", "Mysteries of God");
        shorthandLabels.put("naked-and-afraid", "Naked and Afraid");
        shorthandLabels.put("names-of-god-in-scripture", "Names of God In Scripture");
        shorthandLabels.put("nation", "Nation");
        shorthandLabels.put("natural-roots", "Natural Roots");
        shorthandLabels.put("new-earth", "New Earth");
        shorthandLabels.put("new-heaven", "New Heaven");
        shorthandLabels.put("new-jerusalem", "New Jerusalem");
        shorthandLabels.put("new-name", "New Name");
        shorthandLabels.put("noble-and-great", "Noble and Great");
        shorthandLabels.put("numbers-numbering-large", "Numbers/Numbering, Large");
        shorthandLabels.put("oath-and-covenant", "Oath and Covenant");
        shorthandLabels.put("office", "Office");
        shorthandLabels.put("olive-tree", "Olive Tree");
        shorthandLabels.put("one-eternal-round", "One Eternal Round");
        shorthandLabels.put("oracle", "Oracle");
        shorthandLabels.put("ordinance", "Ordinance");
        shorthandLabels.put("other-sheep", "Other Sheep");
        shorthandLabels.put("outcasts-of-israel", "Outcasts of Israel");
        shorthandLabels.put("patience", "Patience");
        shorthandLabels.put("patriarchal-blessings", "Patriarchal Blessings");
        shorthandLabels.put("patriarchal-priesthood", "Patriarchal Priesthood");
        shorthandLabels.put("pattern-for-understanding-truth", "Pattern for Understanding Truth");
        shorthandLabels.put("peacemaker", "Peacemaker");
        shorthandLabels.put("perfection", "Perfection");
        shorthandLabels.put("persecution", "Persecution");
        shorthandLabels.put("pestilence", "Pestilence");
        shorthandLabels.put("pharaoh", "Pharaoh");
        shorthandLabels.put("plan-of-salvation", "Plan of Salvation");
        shorthandLabels.put("possess-your-soul", "Possess Your Soul");
        shorthandLabels.put("power-of-god", "Power of God");
        shorthandLabels.put("power-of-godliness", "Power of Godliness");
        shorthandLabels.put("powers-of-heaven", "Powers of Heaven");
        shorthandLabels.put("pray-always", "Pray Always");
        shorthandLabels.put("pray-er", "Pray/er");
        shorthandLabels.put("priest", "Priest");
        shorthandLabels.put("priestcraft", "Priestcraft");
        shorthandLabels.put("priesthood-blessings-of-the", "Priesthood, Blessings of the");
        shorthandLabels.put("priesthood-power-in-the", "Priesthood, Power In the");
        shorthandLabels.put("priesthood", "Priesthood");
        shorthandLabels.put("principalities", "Principalities");
        shorthandLabels.put("principles-and-rules", "Principles and Rules");
        shorthandLabels.put("promised-land", "Promised Land");
        shorthandLabels.put("prophet", "Prophet");
        shorthandLabels.put("public-rites--ordinances", "Public Rites (Ordinances)");
        shorthandLabels.put("pure-in-heart", "Pure In Heart");
        shorthandLabels.put("rabbi", "Rabbi");
        shorthandLabels.put("raise-up-seed", "Raise Up Seed");
        shorthandLabels.put("rebaptism", "Rebaptism");
        shorthandLabels.put("redemption", "Redemption");
        shorthandLabels.put("reem", "Re'em");
        shorthandLabels.put("reins", "Reins");
        shorthandLabels.put("remember", "Remember");
        shorthandLabels.put("remnant", "Remnant");
        shorthandLabels.put("repent-ance", "Repent/ance");
        shorthandLabels.put("rest-of-the-lord", "Rest of the Lord");
        shorthandLabels.put("restoration-or-apostasy", "Restoration Or Apostasy");
        shorthandLabels.put("restoration-the", "Restoration, The");
        shorthandLabels.put("restoring-knowledge", "Restoring Knowledge");
        shorthandLabels.put("resurrection-burial", "Resurrection/ Burial");
        shorthandLabels.put("revelation", "Revelation");
        shorthandLabels.put("righteousness", "Righteousness");
        shorthandLabels.put("rights-belonging-to-the-fathers", "Rights Belonging To the Fathers");
        shorthandLabels.put("rock", "Rock");
        shorthandLabels.put("ruach", "Ruach");
        shorthandLabels.put("ruler", "Ruler");
        shorthandLabels.put("sabbath-day", "Sabbath Day");
        shorthandLabels.put("sacrament--lords-supper", "Sacrament (Lords Supper)");
        shorthandLabels.put("sacred-embrace", "Sacred Embrace");
        shorthandLabels.put("sacred-information", "Sacred Information");
        shorthandLabels.put("sacrifice", "Sacrifice");
        shorthandLabels.put("saint", "Saint");
        shorthandLabels.put("salem", "Salem");
        shorthandLabels.put("salvation", "Salvation");
        shorthandLabels.put("sanctification", "Sanctification");
        shorthandLabels.put("satan", "Satan");
        shorthandLabels.put("scales-of-darkness", "Scales of Darkness");
        shorthandLabels.put("school-of-the-prophets", "School of the Prophets");
        shorthandLabels.put("scripture", "Scripture");
        shorthandLabels.put("sealed-in-their-foreheads", "Sealed in Their Foreheads");
        shorthandLabels.put("sealing-power", "Sealing Power");
        shorthandLabels.put("second-comforter", "Second Comforter");
        shorthandLabels.put("seed-of-abraham", "Seed of Abraham");
        shorthandLabels.put("seed-of-christ", "Seed of Christ");
        shorthandLabels.put("seer", "Seer");
        shorthandLabels.put("self-reliance", "Self-Reliance");
        shorthandLabels.put("selflessness", "Selflessness");
        shorthandLabels.put("seraphim", "Seraphim");
        shorthandLabels.put("servant", "Servant");
        shorthandLabels.put("seventy", "Seventy");
        shorthandLabels.put("sheol", "Sheol");
        shorthandLabels.put("shew", "Shew");
        shorthandLabels.put("shewbread", "Shewbread");
        shorthandLabels.put("signs-of-the-times", "Signs of the Times");
        shorthandLabels.put("signs", "Signs");
        shorthandLabels.put("sin", "Sin");
        shorthandLabels.put("single-to-god", "Single To God");
        shorthandLabels.put("slow-of-speech", "Slow of Speech");
        shorthandLabels.put("small-means", "Small Means");
        shorthandLabels.put("son-of-perdition", "Son of Perdition");
        shorthandLabels.put("sons-and-daughters-of-god", "Sons and Daughters of God");
        shorthandLabels.put("soothsayers", "Soothsayers");
        shorthandLabels.put("soul-spirit-body-of-man", "Soul/Spirit/Body of Man");
        shorthandLabels.put("speak-with-the-tongue-of-angels", "Speak with the Tongue of Angels");
        shorthandLabels.put("spirit-matter", "Spirit Matter");
        shorthandLabels.put("spirit-of-christ", "Spirit of Christ");
        shorthandLabels.put("spirit-of-truth", "Spirit of Truth");
        shorthandLabels.put("stiffneckedness", "Stiffneckedness");
        shorthandLabels.put("still-small-voice", "Still, Small Voice");
        shorthandLabels.put("strange-act", "Strange Act");
        shorthandLabels.put("stretched-forth-his-hand", "Stretched Forth His Hand");
        shorthandLabels.put("studying-the-scriptures", "Studying the Scriptures");
        shorthandLabels.put("submissive-sion", "Submissive/sion");
        shorthandLabels.put("suddenly", "Suddenly");
        shorthandLabels.put("suffer-for-their-own-sins", "Suffer For Their Own Sins");
        shorthandLabels.put("surety-christ-as", "Surety, Christ As");
        shorthandLabels.put("sustain", "Sustain");
        shorthandLabels.put("symbols", "Symbols");
        shorthandLabels.put("synagogue", "Synagogue");
        shorthandLabels.put("take-away-our-reproach", "Take Away Our Reproach");
        shorthandLabels.put("take-the-name-of-the-lord-in-vain", "Take the Name of the Lord In Vain");
        shorthandLabels.put("taken-captive-by-the-devil", "Taken Captive By the Devil");
        shorthandLabels.put("teach-teacher", "Teach/Teacher");
        shorthandLabels.put("temple", "Temple");
        shorthandLabels.put("temptation", "Temptation");
        shorthandLabels.put("testify", "Testify");
        shorthandLabels.put("testimony-of-jesus", "Testimony of Jesus");
        shorthandLabels.put("testing-the-spirits", "Testing the Spirits");
        shorthandLabels.put("three-witnesses", "Three Witnesses");
        shorthandLabels.put("thrones", "Thrones");
        shorthandLabels.put("times-of-the-gentiles", "Times of the Gentiles");
        shorthandLabels.put("tithing", "Tithing");
        shorthandLabels.put("transgression", "Transgression");
        shorthandLabels.put("translation", "Translation");
        shorthandLabels.put("trust-in-man", "Trust In Man");
        shorthandLabels.put("truth", "Truth");
        shorthandLabels.put("twelve-apostles", "Twelve Apostles");
        shorthandLabels.put("unbelief", "Unbelief");
        shorthandLabels.put("under-the-earth", "Under the Earth");
        shorthandLabels.put("unity", "Unity");
        shorthandLabels.put("unpardonable-sin", "Unpardonable Sin");
        shorthandLabels.put("unspeakable", "Unspeakable");
        shorthandLabels.put("urim-and-thummim", "Urim and Thummim");
        shorthandLabels.put("veil", "Veil");
        shorthandLabels.put("virtue", "Virtue");
        shorthandLabels.put("voice-of-god", "Voice of God");
        shorthandLabels.put("washing-away-of-sin", "Washing Away of Sin");
        shorthandLabels.put("watch", "Watch");
        shorthandLabels.put("waxing-strong", "Waxing Strong");
        shorthandLabels.put("what-lack-i-yet", "What Lack I Yet?");
        shorthandLabels.put("what-which-is-right", "What/Which Is Right");
        shorthandLabels.put("white-and-delightsome", "White and Delightsome");
        shorthandLabels.put("white-garments", "White Garments");
        shorthandLabels.put("white-stone", "White Stone");
        shorthandLabels.put("whole", "Whole");
        shorthandLabels.put("wicked", "Wicked");
        shorthandLabels.put("will-of-the-father", "Will of the Father");
        shorthandLabels.put("willing-to-submit", "Willing To Submit");
        shorthandLabels.put("wisdom", "Wisdom");
        shorthandLabels.put("woe", "Woe");
        shorthandLabels.put("world", "World");
        shorthandLabels.put("worlds-without-end", "Worlds Without End");
        shorthandLabels.put("worthy-unworthy", "Worthy/Unworthy");
        shorthandLabels.put("wrath", "Wrath");
        shorthandLabels.put("wrest", "Wrest");
        shorthandLabels.put("zion", "Zion");

        shorthandLabels.put("fac1","Fascimile 1");
        shorthandLabels.put("fac2","Fascimile 2");
        shorthandLabels.put("fac3","Fascimile 3");

        shorthandLabels.put("podcast-gethsemane", "Gethsemane");
        shorthandLabels.put("podcast-fellowship-diversity", "Fellowship Diversity");
        shorthandLabels.put("podcast-who_is_christ", "Who is Christ?");
        shorthandLabels.put("podcast-contradicting_joseph", "Contradicting Joseph");
        shorthandLabels.put("podcast-faith", "Faith");
        shorthandLabels.put("podcast-repentance", "Repentance");
        shorthandLabels.put("podcast-baptism", "Baptism");
        shorthandLabels.put("podcast-baptism_of_fire", "Baptism of Fire");
        shorthandLabels.put("podcast-embracing_truth", "Embracing Truth");
        shorthandLabels.put("podcast-tithing", "Tithing");
        shorthandLabels.put("podcast-sacrament", "Sacrament");
        shorthandLabels.put("podcast-resurrection_morning", "Resurrection Morning");
        shorthandLabels.put("podcast-our_dispensation", "Our Dispensation");
        shorthandLabels.put("podcast-the_heavens", "The Heavens");
        shorthandLabels.put("podcast-jacobs_ladder", "Jacob's Ladder");
        shorthandLabels.put("podcast-the_remnant", "The Remnant");
        shorthandLabels.put("podcast-prayer_1", "Prayer, Part 1");
        shorthandLabels.put("podcast-prayer_2", "Prayer, Part 2");
        shorthandLabels.put("podcast-angels_1", "Angels, Part 1");
        shorthandLabels.put("podcast-angels_2", "Angels, Part 2");
        shorthandLabels.put("podcast-angels_3", "Angels, Part 3");
        shorthandLabels.put("podcast-new_jerusalem", "New Jerusalem");
        shorthandLabels.put("podcast-growing_zion", "Growing Zion");
        shorthandLabels.put("podcast-visions", "Visions");
        shorthandLabels.put("podcast-sacrifice", "Sacrifice");
        shorthandLabels.put("podcast-defending_zion", "Defending Zion");
        shorthandLabels.put("podcast-generation", "Generation");
        shorthandLabels.put("podcast-adam-ondi-ahman", "Adam-ondi-Ahman");
        shorthandLabels.put("podcast-patriarchal_blessings", "Patriarchal Blessings");
        shorthandLabels.put("podcast-how_the_restoration_has_fallen", "How the Restoration Has Fallen!");
        shorthandLabels.put("podcast-priestcraft", "Priestcraft");
        shorthandLabels.put("podcast-temple_1", "Temple, Part 1");
        shorthandLabels.put("podcast-temple_2", "Temple, Part 2");
        shorthandLabels.put("podcast-dances_with_wolves", "Dances with Wolves");
        shorthandLabels.put("podcast-temple_3", "Temple, Part 3");
        shorthandLabels.put("podcast-temple_4", "Temple, Part 4");
        shorthandLabels.put("podcast-temple_5", "Temple, Part 5");
        shorthandLabels.put("podcast-temple_6", "Temple, Part 6");
        shorthandLabels.put("podcast-babylon", "Babylon");
        shorthandLabels.put("podcast-interpreting_scripture", "Interpreting Scripture");
        shorthandLabels.put("podcast-the_journey", "The Journey");
        shorthandLabels.put("podcast-cycles_of_creation", "Cycles of Creation");
        shorthandLabels.put("podcast-gathering_truth", "Gathering Truth");
        shorthandLabels.put("podcast-breaking_the_chains", "Breaking the Chains");
        shorthandLabels.put("podcast-denying_the_power", "Denying the Power");
        shorthandLabels.put("podcast-charity_1", "Charity, Part 1");
        shorthandLabels.put("podcast-charity_2", "Charity, Part 2");
        shorthandLabels.put("podcast-prophecy", "Prophecy");
        shorthandLabels.put("podcast-ordinances", "Ordinances");
        shorthandLabels.put("podcast-celebrating_christ", "Celebrating Christ");
        shorthandLabels.put("podcast-discernment_1", "Discernment, Part 1");
        shorthandLabels.put("podcast-discernment_2", "Discernment, Part 2");
        shorthandLabels.put("podcast-discernment_3", "Discernment, Part 3");
        shorthandLabels.put("podcast-abraham_1", "Abraham, Part 1");
        shorthandLabels.put("podcast-abraham_2", "Abraham, Part 2");
        shorthandLabels.put("podcast-abraham_3", "Abraham, Part 3");
        shorthandLabels.put("podcast-abraham_4", "Abraham, Part 4");
        shorthandLabels.put("podcast-nephi_1", "Nephi, Part 1");
        shorthandLabels.put("podcast-nephi_2", "Nephi, Part 2");
        shorthandLabels.put("podcast-third_root", "Third Root");
        shorthandLabels.put("podcast-witness_in_all_the_world", "Witness in All the World");
        shorthandLabels.put("podcast-every_word_1", "Every Word, Part 1");
        shorthandLabels.put("podcast-every_word_2", "Every Word, Part 2");
        shorthandLabels.put("podcast-noah", "Noah");
        shorthandLabels.put("podcast-discernment_epilogue", "Discernment, Epilogue");
        shorthandLabels.put("podcast-allegory_of_the_olive_tree", "Allegory of the Olive Tree");
        shorthandLabels.put("podcast-calling_and_election_1", "Calling and Election, Part 1");
        shorthandLabels.put("podcast-calling_and_election_2", "Calling and Election, Part 2");
        shorthandLabels.put("podcast-effective_study_1", "Effective Study, Part 1");
        shorthandLabels.put("podcast-effective_study_2", "Effective Study, Part 2");
        shorthandLabels.put("podcast-effective_study_3", "Effective Study, Part 3");
        shorthandLabels.put("podcast-as_a_little_child", "As a Little Child");
        shorthandLabels.put("podcast-good_questions", "Good Questions");
        shorthandLabels.put("podcast-hyrum_smith", "Hyrum Smith");
        shorthandLabels.put("podcast-ministry_of_the_fathers", "Ministry of the Fathers");
        shorthandLabels.put("podcast-patriarchs", "Patriarchs");
        shorthandLabels.put("podcast-authentic_christianity_1", "Authentic Christianity, Part 1");
        shorthandLabels.put("podcast-authentic_christianity_2", "Authentic Christianity, Part 2");
        shorthandLabels.put("podcast-hope_1", "Hope, Part 1");
        shorthandLabels.put("podcast-hope_2", "Hope, Part 2");
        shorthandLabels.put("podcast-peter", "Peter");
        shorthandLabels.put("podcast-hope_3", "Hope, Part 3");
        shorthandLabels.put("podcast-fasting", "Fasting");
//        shorthandLabels.put("fyom_1", "Be of Good Cheer, Boise ID");
//        shorthandLabels.put("fyom_2", "Faith, Idaho Falls ID");
//        shorthandLabels.put("fyom_3", "Repentance, Logan UT");
//        shorthandLabels.put("fyom_4", "Covenants, Centerville UT");
//        shorthandLabels.put("fyom_5", "Priesthood, Orem UT");
//        shorthandLabels.put("fyom_6", "Zion, Grand Junction CO");
//        shorthandLabels.put("fyom_7", "Christ, Ephraim UT");
//        shorthandLabels.put("fyom_8", "A Broken Heart, Las Vegas NV");
//        shorthandLabels.put("fyom_9", "Marriage and Family, St George UT");
//        shorthandLabels.put("fyom_10", "Preserving the Restoration, Mesa AZ");
    }

    /*
     * For showing chapter labels in the Chapters navigation tab
     */
    public static String getShorthandLabel(String bookId, String chapter) {
        if (bookId.startsWith("podcast")) {
            return shorthandLabels.get("podcast-" + chapter);
        }

        if (bookId.startsWith("fyom")) {
            return shorthandLabels.get("fyom-" + chapter);
        }

        switch (bookId) {
            case "abraham":
                switch (chapter) {
                    case "fac1":
                    case "fac2":
                    case "fac3":
                        return shorthandLabels.get(chapter);
                    default:
                        return "Chapter " + chapter;
                }
            case "appendix":
            case "glossary":
            case "fac1":
            case "fac2":
            case "fac3":
                return shorthandLabels.get(chapter);
            case "ocforeword":
            case "occanonization":
            case "ocpreface":
            case "ncforeword":
            case "nccanonization":
            case "ntpreface":
            case "bompreface":
            case "bomintro":
            case "title":
            case "tow":
            case "tcforeword":
            case "tccanonization":
            case "tcpreface":
            case "tcintro":
            case "epigraph":
                return shorthandLabels.get(bookId);
            case "lecture":
                if (chapter.equals("preface")) {
                    return shorthandLabels.get(chapter);
                } else {
                    return "Lecture " + chapter;
                }
            case "section":
                return "Section " + chapter;
            default:
                return "Chapter " + chapter;
        }
    }

    /*
     * For showing the selected library item in the action bar of the Reader view
     */
    public static String getShorthandTitle(String bookId, String chapter) {
        if (bookId.startsWith("podcast")) {
            return shorthandLabels.get("podcast-" + chapter);
        }

        if (bookId.startsWith("fyom")) {
            return shorthandLabels.get("fyom-" + chapter);
        }

        switch (bookId) {
            case "abraham":
                switch (chapter) {
                    case "fac1":
                    case "fac2":
                    case "fac3":
                        return shorthandLabels.get(bookId) + " " + shorthandLabels.get(chapter);
                    default:
                        return shorthandLabels.get(bookId) + " " + chapter;
                }
            case "appendix":
            case "glossary":
            case "ocforeword":
            case "occanonization":
            case "ocpreface":
            case "ncforeword":
            case "nccanonization":
            case "ntpreface":
            case "bompreface":
            case "bomintro":
            case "title":
            case "tow":
            case "tcforeword":
            case "tccanonization":
            case "tcpreface":
            case "tcintro":
            case "epigraph":
                return shorthandLabels.get(bookId);
            case "lecture":
                if (chapter.equals("preface")) {
                    return shorthandLabels.get(chapter);
                } else {
                    return "Lecture " + chapter;
                }
            default:
                return shorthandLabels.get(bookId) + " " + (chapter != null ? chapter : "");
        }
    }

    /*
     * For showing selected volume on Books navigation tab
     */
    public static String getShorthandVolume(String volumeId) {
        return shorthandVolumes.get(volumeId);
    }

    /*
     * For showing the selected book on the Chapters navigation tab
     */
    public static String getShorthandBook(String bookId) {
        return shorthandLabels.get(bookId);
    }
   
}
