package info.scriptures.rescriptures.util;

import android.app.Activity;
import android.content.Intent;

import info.scriptures.rescriptures.R;

public class ThemeUtil {
    private static String sTheme;
    public final static String THEME_DARK = "dark";
    public final static String THEME_LIGHT = "light";
    public final static String THEME_SEPIA = "sepia";
    /**
     * Set the theme of the Activity, and restart it by creating a new Activity of the same type.
     */
    public static void changeToTheme(Activity activity, String theme)
    {
        sTheme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
    }
    /** Set the theme of the activity, according to the configuration. */
    public static void onActivityCreateSetTheme(Activity activity, String theme)
    {
        if (theme != null && theme.equals(THEME_DARK)) {
            activity.setTheme(R.style.DarkTheme);
        } else if (theme != null && theme.equals(THEME_LIGHT)) {
            activity.setTheme(R.style.LightTheme);
        } else {
            activity.setTheme(R.style.SepiaTheme);
        }
    }
}
