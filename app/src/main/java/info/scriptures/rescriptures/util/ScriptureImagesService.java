package info.scriptures.rescriptures.util;

import java.util.HashMap;
import java.util.Map;

import info.scriptures.rescriptures.R;

public class ScriptureImagesService {

    private static Map<String, Integer> imageIds;
    static {
        imageIds = new HashMap<>();
        imageIds.put("fac1", R.drawable.fac1);
        imageIds.put("fac2", R.drawable.fac2);
        imageIds.put("fac3", R.drawable.fac3);
        imageIds.put("map1", R.drawable.map1);
        imageIds.put("map2", R.drawable.map2);
        imageIds.put("map3", R.drawable.map3);
        imageIds.put("map4", R.drawable.map4);
        imageIds.put("map5", R.drawable.map5);
        imageIds.put("map6", R.drawable.map6);
        imageIds.put("map7", R.drawable.map7);
        imageIds.put("map8", R.drawable.map8);
        imageIds.put("map9", R.drawable.map9);
        imageIds.put("map10", R.drawable.map10);
        imageIds.put("timeline", R.drawable.timeline);
    }

    public static Integer getImageId(String imageName) {
        return imageIds.get(imageName);
    }
}
