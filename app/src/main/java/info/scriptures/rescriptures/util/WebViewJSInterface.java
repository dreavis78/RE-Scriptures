package info.scriptures.rescriptures.util;

import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import info.scriptures.rescriptures.ui.reader.ReaderActivity;

public class WebViewJSInterface {

    private WebView webView;
    private ReaderActivity activity;

    public WebViewJSInterface(ReaderActivity activity, WebView webView) {
        this.activity = activity;
        this.webView = webView;
    }

    @JavascriptInterface
    public void updateParagraph(int paragraph) {
        activity.updateParagraph(paragraph);
    }
}
