package info.scriptures.rescriptures;

import androidx.annotation.NonNull;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<R, V> {
   protected R repository;

   protected WeakReference<V> view;

   public void setRepository(R repository) {
      this.repository = repository;
      if (setupDone()) {
         updateView();
      }
   }

   public void bindView(@NonNull V view) {
      this.view = new WeakReference<>(view);
      if (setupDone()) {
         updateView();
      }
   }

   public void unbindView() {
      this.view = null;
   }

   protected V view() {
      if (view == null) {
         return null;
      } else {
         return view.get();
      }
   }

   protected abstract void updateView();

   protected boolean setupDone() {
      return view() != null && repository != null;
   }

}
