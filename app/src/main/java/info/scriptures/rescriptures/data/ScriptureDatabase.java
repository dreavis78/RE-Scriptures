package info.scriptures.rescriptures.data;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import android.content.Context;

import com.google.common.annotations.VisibleForTesting;

import info.scriptures.rescriptures.data.dao.BookDao;
import info.scriptures.rescriptures.data.dao.BookmarkDao;
import info.scriptures.rescriptures.data.dao.ChapterDao;
import info.scriptures.rescriptures.data.dao.ParagraphDao;
import info.scriptures.rescriptures.data.dao.VolumeDao;
import info.scriptures.rescriptures.data.entity.BookEntity;
import info.scriptures.rescriptures.data.dao.BookWithNumChaptersDao;
import info.scriptures.rescriptures.data.entity.BookmarkEntity;
import info.scriptures.rescriptures.data.entity.ChapterEntity;
import info.scriptures.rescriptures.data.entity.ParagraphEntity;
import info.scriptures.rescriptures.data.entity.VolumeEntity;

@Database(entities = {
        VolumeEntity.class,
        BookEntity.class,
        ChapterEntity.class,
        ParagraphEntity.class,
        BookmarkEntity.class}, version = 3)
public abstract class ScriptureDatabase extends RoomDatabase {

    private static ScriptureDatabase INSTANCE;

    @VisibleForTesting
    private static final String DATABASE_NAME = "scriptures_v1.2.db";

    public abstract VolumeDao volumeModel();

    public abstract BookDao bookModel();

    public abstract ChapterDao chapterModel();

    public abstract BookWithNumChaptersDao bookWithNumChaptersDao();

    public abstract ParagraphDao paragraphModel();

    public abstract BookmarkDao bookmarkModel();

    static ScriptureDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (ScriptureDatabase.class) {
                INSTANCE = buildDatabase(context.getApplicationContext());
            }
        }
        return INSTANCE;
    }

    @VisibleForTesting
    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE users "
//                    + " ADD COLUMN last_update INTEGER");
        }
    };

    private static ScriptureDatabase buildDatabase(final Context appContext) {
        return Room.databaseBuilder(appContext, ScriptureDatabase.class, DATABASE_NAME)
                .createFromAsset("databases/scriptures_v1.3.db")
                .addMigrations(MIGRATION_2_3)
                .build();
    }
}
