package info.scriptures.rescriptures.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import info.scriptures.rescriptures.model.Volume;

@Entity(tableName = "volumes")
public class VolumeEntity implements Volume {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "volume_id")
    private String id;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "volume_order")
    private int order;

    public VolumeEntity(String id, String name, int order) {
        this.id = id;
        this.name = name;
        this.order = order;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
