package info.scriptures.rescriptures.data.dao;

import android.database.sqlite.SQLiteException;

import org.jsoup.helper.StringUtil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.room.RawQuery;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteQuery;
import info.scriptures.rescriptures.data.entity.BookEntity;
import info.scriptures.rescriptures.data.entity.VolumeEntity;
import info.scriptures.rescriptures.data.searchFilters.SortOrder;
import info.scriptures.rescriptures.data.entity.ParagraphEntity;
import timber.log.Timber;

@Dao
abstract public class ParagraphDao {

    @RawQuery(observedEntities = ParagraphEntity.class)
    protected abstract LiveData<List<ParagraphEntity>> _searchByKeyword(SupportSQLiteQuery query) throws SQLiteException;

    public LiveData<List<ParagraphEntity>> searchByKeyword(String query, VolumeEntity workFilter, BookEntity bookFilter, String sortOrder, int limit, int offset) {
        LiveData<List<ParagraphEntity>> searchResults = new MutableLiveData<>();
        Boolean isPhrase = (query.startsWith("\"") && query.endsWith("\""));
        if (!isPhrase) {
            query = query.replaceAll("\"", "\"\"");
            String[] terms = query.split(" ");
            for(int i = 0, len = terms.length; i < len; i++) {
                String t = terms[i];
                if (t.startsWith("-")) {
                    terms[i] = "NOT " + t.substring(1);
                }
            }
            query = StringUtil.join(terms, " ");
        }

        query = query.replaceAll("'", "''");

        try {
            searchResults = _searchByKeyword(buildKeywordTermsSearchQuery(query, workFilter, bookFilter, sortOrder, limit, offset));
        } catch(SQLiteException e) {
            e.printStackTrace();
        }

        return searchResults;
    }

    private SimpleSQLiteQuery buildKeywordTermsSearchQuery(String query, VolumeEntity workFilter, BookEntity bookFilter, String sortOrder, int limit, int offset) {
        StringBuilder s = new StringBuilder();
        s.append("SELECT fp.rowid, p.* FROM fts_paragraphs fp, paragraphs p WHERE fts_paragraphs = ? ");

        if ( (workFilter != null && !workFilter.getId().contains("all"))) {
            s.append("AND p.volume_id = ? ");
        }

        if (bookFilter != null && !bookFilter.getBookId().contains("all")) {
            s.append("AND p.book_id = ? ");
        }

        s.append("AND p.rowid = fp.rowid ");

        if (sortOrder.equals(SortOrder.RELEVANCE)) {
            s.append("ORDER BY rank LIMIT ? OFFSET ?;");
        } else {
            s.append("ORDER BY fp.rowid LIMIT ? OFFSET ?;");
        }

        List<String> params = new ArrayList<>();
        params.add(query);

        if ( (workFilter != null && !workFilter.getId().contains("all")) ) {
            params.add(workFilter.getId());
        }

        if ( (bookFilter != null && !bookFilter.getBookId().contains("all")) ) {
            params.add(bookFilter.getBookId());
        }

        params.add(Integer.toString(limit));
        params.add(Integer.toString(offset));

        Timber.i(s.toString());
        Timber.i(Arrays.toString(params.toArray(new String[]{})));

        return new SimpleSQLiteQuery(s.toString(), params.toArray(new String[]{}));
    }
}
