package info.scriptures.rescriptures.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

@Dao
public interface BookWithNumChaptersDao {
    @Query("SELECT b.book_id, b.name, b.book_order, COUNT(c.book_id) AS num_chapters FROM books b, chapters c WHERE b.volume_id = :volumeId AND c.book_id = b.book_id GROUP BY b.book_id ORDER BY b.book_order;")
    public LiveData<List<BookWithNumChapters>> loadBooksByVolume(String volumeId);

    static class BookWithNumChapters {
        public String book_id;
        public String name;
        public Integer book_order;
        public Integer num_chapters;
    }
}
