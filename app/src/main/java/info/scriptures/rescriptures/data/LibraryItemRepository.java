package info.scriptures.rescriptures.data;

public class LibraryItemRepository {

    private String id;

    private String label;

    private Boolean isSelected;

    public LibraryItemRepository(String id, String label, Boolean isSelected) {
        this.id = id;
        this.label = label;
        this.isSelected = isSelected;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public Boolean isSelected() { return isSelected; }

}
