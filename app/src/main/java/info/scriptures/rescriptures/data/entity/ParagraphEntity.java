package info.scriptures.rescriptures.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import info.scriptures.rescriptures.model.Paragraph;

@Entity(tableName = "paragraphs")
public class ParagraphEntity implements Paragraph, Parcelable {

    public static final Parcelable.Creator<ParagraphEntity> CREATOR = new Parcelable.Creator<ParagraphEntity>() {
        public ParagraphEntity createFromParcel(Parcel in) {
            return new ParagraphEntity(in);
        }

        public ParagraphEntity[] newArray(int size) {
            return new ParagraphEntity[size];
        }
    };

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "paragraph_id")
    private Integer paragraphId;

    @NonNull
    @ColumnInfo(name = "volume_id")
    private String volumeId;

    @NonNull
    @ColumnInfo(name = "book_id")
    private String bookId;

    @NonNull
    @ColumnInfo(name = "chapter_id")
    private String chapter;

    @NonNull
    @ColumnInfo(name = "position")
    private Integer position;

    @NonNull
    @ColumnInfo(name = "paragraph")
    private String paragraph;
    public ParagraphEntity(
        int paragraphId,
        String volumeId,
        String bookId,
        String chapter,
        int position,
        String paragraph
    ) {
        this.volumeId = volumeId;
        this.bookId = bookId;
        this.chapter = chapter;
        this.position = position;
        this.paragraph = paragraph;
    }

    private ParagraphEntity(Parcel in) {
        paragraphId = in.readInt();
        volumeId = in.readString();
        bookId = in.readString();
        chapter = in.readString();
        position = in.readInt();
        paragraph = in.readString();
    }

    @NonNull
    public Integer getParagraphId() { return paragraphId; }

    public void setParagraphId(@NonNull Integer paragraphId) { this.paragraphId = paragraphId; }

    @Override
    @NonNull
    public String getVolumeId() {
        return volumeId;
    }

    public void setVolumeId(@NonNull String volumeId) {
        this.volumeId = volumeId;
    }

    @Override
    @NonNull
    public String getBookId() {
        return bookId;
    }

    public void setBookId(@NonNull String bookId) {
        this.bookId = bookId;
    }

    @Override
    @NonNull
    public String getChapter() {
        return chapter;
    }

    public void setChapter(@NonNull String chapter) {
        this.chapter = chapter;
    }

    @Override
    @NonNull
    public Integer getPosition() {
        return position;
    }

    public void setPosition(@NonNull Integer position) {
        this.position = position;
    }

    @Override
    @NonNull
    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(@NonNull String paragraph) {
        this.paragraph = paragraph;
    }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(volumeId);
        out.writeString(bookId);
        out.writeString(chapter);
        out.writeInt(position);
        out.writeString(paragraph);
    }
}
