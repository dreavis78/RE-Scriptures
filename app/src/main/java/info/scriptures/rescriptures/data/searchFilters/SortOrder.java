package info.scriptures.rescriptures.data.searchFilters;

public class SortOrder {
    public static final String RELEVANCE = "Relevance";
    public static final String TRADITIONAL = "Traditional";

    public static final String[] values = {
        SortOrder.RELEVANCE,
        SortOrder.TRADITIONAL
    };
}
