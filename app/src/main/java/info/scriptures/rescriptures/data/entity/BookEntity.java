package info.scriptures.rescriptures.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import info.scriptures.rescriptures.model.Book;

@Entity(tableName = "books")
public class BookEntity implements Book {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "book_id")
    private String bookId;

    @NonNull
    @ColumnInfo(name = "volume_id")
    private String volumeId;

    @NonNull
    @ColumnInfo(name = "name")
    private String bookName;

    @NonNull
    @ColumnInfo(name = "book_order")
    private int bookOrder;

    public BookEntity(){}
    public BookEntity(String id, String volumeId, String name, int order) {
        this.bookId = id;
        this.volumeId = volumeId;
        this.bookName = name;
        this.bookOrder = order;
    }

    @Override
    public String getBookId() {
        return bookId;
    }

    public void setBookId(String id) {
        this.bookId = id;
    }

    @Override
    public String getVolumeId() { return volumeId; }

    public void setVolumeId(String volumeId) { this.volumeId = volumeId; }

    @Override
    public String getBookName() {
        return bookName;
    }

    public void setBookName(String name) {
        this.bookName = name;
    }

    @Override
    public int getBookOrder() {
        return bookOrder;
    }

    public void setBookOrder(int order) {
        this.bookOrder = order;
    }
}
