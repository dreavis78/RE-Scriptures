package info.scriptures.rescriptures.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

@Dao
public interface BookDao {
    @Query("SELECT book_id FROM books WHERE volume_id = :volumeId ORDER BY book_order ASC LIMIT 1;")
    LiveData<String> getFirstBookInVolume(String volumeId);
}
