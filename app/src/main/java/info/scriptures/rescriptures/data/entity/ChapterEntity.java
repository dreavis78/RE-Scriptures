package info.scriptures.rescriptures.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import info.scriptures.rescriptures.model.Chapter;

@Entity(tableName = "chapters")
public class ChapterEntity implements Chapter, Parcelable {

    public static final Parcelable.Creator<ChapterEntity> CREATOR = new Parcelable.Creator<ChapterEntity>() {
        public ChapterEntity createFromParcel(Parcel in) {
            return new ChapterEntity(in);
        }

        public ChapterEntity[] newArray(int size) {
            return new ChapterEntity[size];
        }
    };

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "chapter_id")
    private int chapterId;

    @NonNull
    @ColumnInfo(name = "volume_id")
    private String volumeId;

    @NonNull
    @ColumnInfo(name = "book_id")
    private String bookId;

    @NonNull
    @ColumnInfo(name = "chapter")
    private String chapter;

    @NonNull
    @ColumnInfo(name = "content")
    private String content;

    public ChapterEntity(
         int chapterId,
         String volumeId,
         String bookId,
         String chapter,
         String content
    ) {
        this.volumeId = volumeId;
        this.bookId = bookId;
        this.chapter = chapter;
        this.content = content;
    }

    private ChapterEntity(Parcel in) {
        chapterId = in.readInt();
        volumeId = in.readString();
        bookId = in.readString();
        chapter = in.readString();
        content = in.readString();
    }

    @NonNull
    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(@NonNull int chapterId) {
        this.chapterId = chapterId;
    }

    @NonNull
    public String getVolumeId() {
        return volumeId;
    }

    public void setVolumeId(@NonNull String volumeId) {
        this.volumeId = volumeId;
    }

    @NonNull
    public String getBookId() {
        return bookId;
    }

    public void setBookId(@NonNull String bookId) {
        this.bookId = bookId;
    }

    @Override
    @NonNull
    public String getChapter() {
        return chapter;
    }

    public void setChapter(@NonNull String chapter) {
        this.chapter = chapter;
    }


    @NonNull
    public String getContent() {
        return content;
    }

    public void setContent(@NonNull String content) {
        this.content = content;
    }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(chapterId);
        out.writeString(volumeId);
        out.writeString(bookId);
        out.writeString(chapter);
        out.writeString(content);
    }
}
