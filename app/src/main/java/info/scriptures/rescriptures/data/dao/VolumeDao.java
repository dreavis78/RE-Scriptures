package info.scriptures.rescriptures.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import info.scriptures.rescriptures.data.entity.VolumeEntity;

@Dao
public interface VolumeDao {
    @Query("SELECT * FROM volumes ORDER BY volume_order ASC;")
    LiveData<List<VolumeEntity>> loadVolumes();
}
