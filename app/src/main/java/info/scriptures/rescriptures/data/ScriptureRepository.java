package info.scriptures.rescriptures.data;

import android.annotation.SuppressLint;
import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.preference.PreferenceManager;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import info.scriptures.rescriptures.data.dao.BookDao;
import info.scriptures.rescriptures.data.dao.ChapterDao;
import info.scriptures.rescriptures.data.dao.BookmarkDao;
import info.scriptures.rescriptures.data.dao.ParagraphDao;
import info.scriptures.rescriptures.data.dao.VolumeDao;
import info.scriptures.rescriptures.data.dao.BookWithNumChaptersDao;
import info.scriptures.rescriptures.data.entity.BookEntity;
import info.scriptures.rescriptures.data.entity.ChapterEntity;
import info.scriptures.rescriptures.data.entity.BookmarkEntity;
import info.scriptures.rescriptures.data.entity.ParagraphEntity;
import info.scriptures.rescriptures.data.entity.VolumeEntity;
import timber.log.Timber;

public class ScriptureRepository {

    private static ScriptureRepository INSTANCE = null;
    private static int BOOKMARK_LIMIT = 15;

    private final BookDao bookDao;
    private final ChapterDao chapterDao;
    private final BookWithNumChaptersDao bookWithNumChaptersDao;
    private final ParagraphDao paragraphDao;
    private final BookmarkDao bookmarkDao;
    private LiveData<List<VolumeEntity>> volumes;

    private String selectedVolume = null;
    private String selectedBook = null;
    private String selectedChapter = null;
    private Integer selectedParagraph = 0;
    private Float textSize = 52.5f;
    private SharedPreferences sharedPreferences;

    private ScriptureRepository(Application application) {
        ScriptureDatabase db = ScriptureDatabase.getInstance(application.getApplicationContext());
        VolumeDao volumeDao = db.volumeModel();
        bookDao = db.bookModel();
        bookWithNumChaptersDao = db.bookWithNumChaptersDao();
        chapterDao = db.chapterModel();
        paragraphDao = db.paragraphModel();
        bookmarkDao = db.bookmarkModel();
        volumes = volumeDao.loadVolumes();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(application.getApplicationContext());

        loadLastLocation();
    }

    public static ScriptureRepository getInstance(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new ScriptureRepository(application);
        }
        return INSTANCE;
    }

    public LiveData<List<VolumeEntity>> getVolumes() {
        return volumes;
    }

    public LiveData<List<BookWithNumChaptersDao.BookWithNumChapters>> getBooksByVolume(String volumeId) {
        return bookWithNumChaptersDao.loadBooksByVolume(volumeId);
    }

    public LiveData<List<ChapterEntity>> getChaptersByBook(String bookId) {
        return chapterDao.loadChaptersByBook(bookId);
    }

    public void setSelectedVolume(String volumeId) {
        this.selectedVolume = volumeId;
    }

    public String getSelectedVolume() {
        return this.selectedVolume;
    }

    public void setSelectedBook(String bookId) {
        this.selectedBook = bookId;
        this.selectedChapter = "1";
    }

    public String getSelectedBook() {
        return this.selectedBook;
    }

    public void setSelectedChapter(String chapter) {
        this.selectedChapter = chapter;
    }

    public String getSelectedChapter() {
        return this.selectedChapter;
    }

    public void setSelectedParagraph(Integer position) {
        this.selectedParagraph = position;
    }

    public Integer getSelectedParagraph() { return this.selectedParagraph; }

    public LiveData<String> getFirstBookByVolume(String volumeId) {
        return bookDao.getFirstBookInVolume(volumeId);
    }

    public LiveData<ChapterEntity> loadChapterText(String bookId, String chapter) {
        if (chapter != null) {
            return chapterDao.loadChapter(bookId, chapter);
        } else {
            return chapterDao.loadFirstChapter(bookId);
        }
    }

    public void setTextSize(Float size) {
        textSize = size;
    }

    public Float getTextSize() {
        return textSize;
    }

//    public LiveData<ChapterEntity> getCurrentChapter() { return chapterDao.loadCurrentChapter(selectedBook, selectedChapter); }

    public LiveData<ChapterEntity> getNextChapter() { return chapterDao.loadNextChapter(selectedBook, selectedChapter); }

    public LiveData<ChapterEntity> getPreviousChapter() { return chapterDao.loadPreviousChapter(selectedBook, selectedChapter); }

    public LiveData<List<ParagraphEntity>> getKeywordSearchResults(String query, VolumeEntity workFilter, BookEntity bookFilter, String sortOrder, int limit, int offset) {
        return paragraphDao.searchByKeyword(query, workFilter, bookFilter, sortOrder, limit, offset);
    }

    public LiveData<List<BookmarkEntity>> loadBookmarks() {
        return bookmarkDao.loadBookmarks();
    }

    @SuppressLint("StaticFieldLeak")
    public void addBookmark(BookmarkEntity bookmark) {
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                bookmarkDao.insert(bookmark);
                return null;
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public void removeBookmark(int id) {
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                bookmarkDao.remove(id);
                return null;
            }
        }.execute();
    }

    public boolean hasLastLocation() {
        return sharedPreferences.contains("last_location");
    }

    public void persistLastLocation() {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        try {
            JSONObject location = new JSONObject();
            location.put("volume", this.selectedVolume);
            location.put("book", this.selectedBook);
            location.put("chapter", this.selectedChapter);
            location.put("paragraph", this.selectedParagraph);
            prefsEditor.putString("last_location", location.toString());
            prefsEditor.apply();
        } catch (JSONException e) {
            Timber.e("Error persisting last location: " + e.getMessage());
        }
    }

    private void loadLastLocation() {
        try {
            String locationStr = sharedPreferences.getString(
            "last_location",
            "{\"volume\": \"ot\", \"book\": \"genesis\", \"chapter\": \"1\", \"paragraph\": 0 }"
            );
            JSONObject lastLocation = new JSONObject(locationStr);
            selectedVolume = lastLocation.getString("volume");
            selectedBook = lastLocation.getString("book");
            selectedChapter = lastLocation.getString("chapter");
            selectedParagraph = lastLocation.getInt("paragraph");
        } catch(JSONException e) {
            Timber.e("Error loading last location: " + e.getMessage());
        }
    }
}
