package info.scriptures.rescriptures.data.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import info.scriptures.rescriptures.model.Bookmark;

@Entity(tableName = "bookmarks")
public class BookmarkEntity implements Bookmark {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "bookmark_id")
    private int id;

    @NonNull
    @ColumnInfo(name = "volume_id")
    private String volumeId;

    @NonNull
    @ColumnInfo(name = "book_id")
    private String bookId;

    @NonNull
    private String name;

    @NonNull
    private String chapter;

    @NonNull
    private int paragraph;

    public BookmarkEntity(){}

    @Override
    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    @Override
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    @Override
    public String getVolumeId() { return volumeId; }
    public void setVolumeId(String volumeId) { this.volumeId = volumeId; }

    @Override
    public String getBookId() { return bookId; }
    public void setBookId(String bookId) { this.bookId = bookId; }

    @Override
    public String getChapter() {
        return chapter;
    }
    public void setChapter(String chapter) { this.chapter = chapter; }

    @Override
    public int getParagraph() {
        return paragraph;
    }
    public void setParagraph(int paragraph) { this.paragraph = paragraph; }

    public boolean equals(BookmarkEntity a) {
        return a.getBookId().equals(this.bookId) &&
                a.getChapter().equals(this.chapter) &&
                a.getParagraph() == this.paragraph;
    }
}
