package info.scriptures.rescriptures.data.searchFilters;

import java.util.ArrayList;
import java.util.List;

import info.scriptures.rescriptures.data.entity.VolumeEntity;

public class Works {
    public static final VolumeEntity ALL = new VolumeEntity("all", "All Works", 0);
    public static final VolumeEntity OC = new VolumeEntity("oc", "Old Covenants", 1);
    public static final VolumeEntity NT = new VolumeEntity("nt", "New Testament", 2);
    public static final VolumeEntity BOFM = new VolumeEntity("bofm", "Book of Mormon", 3);
    public static final VolumeEntity TC = new VolumeEntity("tc", "Teachings & Comm.", 4);
    public static final VolumeEntity ST = new VolumeEntity("st", "Suppl. Teachings", 4);

    public static final List<VolumeEntity> values = new ArrayList<VolumeEntity>() {{
        add(Works.ALL);
        add(Works.OC);
        add(Works.NT);
        add(Works.BOFM);
        add(Works.TC);
        add(Works.ST);
    }};
}
