package info.scriptures.rescriptures.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import info.scriptures.rescriptures.data.entity.BookmarkEntity;

@Dao
public interface BookmarkDao {
    @Query("SELECT * FROM bookmarks ORDER BY bookmark_id DESC;")
    LiveData<List<BookmarkEntity>> loadBookmarks();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(BookmarkEntity bookmark);

    @Query("DELETE FROM bookmarks WHERE bookmark_id = :id")
    void remove(int id);
}
