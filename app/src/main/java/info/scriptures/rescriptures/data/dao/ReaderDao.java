package info.scriptures.rescriptures.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import info.scriptures.rescriptures.data.entity.ChapterEntity;

@Dao
public interface ReaderDao {
    @Query("SELECT * FROM chapters WHERE book_id = :bookId AND chapter = :chapter;")
    LiveData<List<ChapterEntity>> loadChapter(String bookId, String chapter);
}
