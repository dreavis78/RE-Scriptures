package info.scriptures.rescriptures.data.entity;

public class LibraryEntity<T> {
    private T entity;

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }
}
