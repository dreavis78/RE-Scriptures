package info.scriptures.rescriptures.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import info.scriptures.rescriptures.data.entity.ChapterEntity;

@Dao
public interface ChapterDao {

    @Query("SELECT * FROM chapters WHERE book_id = :bookId;")
    LiveData<List<ChapterEntity>> loadChaptersByBook(String bookId);

    @Query("SELECT * FROM chapters WHERE book_id = :bookId AND chapter = :chapter;")
    LiveData<ChapterEntity> loadChapter(String bookId, String chapter);

    @Query("SELECT * FROM chapters WHERE book_id = :bookId ORDER BY chapter_id ASC LIMIT 1;")
    LiveData<ChapterEntity> loadFirstChapter(String bookId);

    @Query("SELECT * FROM chapters WHERE chapter_id = ((SELECT chapter_id FROM chapters WHERE book_id = :bookId AND chapter = :chapter) + 1);")
    LiveData<ChapterEntity> loadNextChapter(String bookId, String chapter);

    @Query("SELECT * FROM chapters WHERE chapter_id = ((SELECT chapter_id FROM chapters WHERE book_id = :bookId AND chapter = :chapter) - 1);")
    LiveData<ChapterEntity> loadPreviousChapter(String bookId, String chapter);
}
