package info.scriptures.rescriptures.ui.reader;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.entity.BookmarkEntity;
import info.scriptures.rescriptures.data.entity.ChapterEntity;
import timber.log.Timber;

public class ReaderPresenter extends MvpBasePresenter<ReaderView> {

    private ScriptureRepository repository;
    private LiveData<ChapterEntity> readerLiveData;
    private int ttsProgress = 0;

    ReaderPresenter(@NonNull ScriptureRepository repository) {
        this.repository = repository;

//        bookmarkModel = this.repository.getBookmarkModel();
//        if (bookmarkModel != null) {
//            this.bookmarkModel = bookmarkModel;
//        }
    }

    @Override public void attachView(ReaderView view) {
//        repository.loadBookmarks();
        super.attachView(view);
    }

    @Override public void detachView(boolean retainInstance) {
        repository.persistLastLocation();
        readerLiveData.removeObservers(Objects.requireNonNull(getView()));
        super.detachView(retainInstance);
    }

    public void loadChapterText() {
        readerLiveData = repository.loadChapterText(repository.getSelectedBook(), repository.getSelectedChapter());
        readerLiveData.observe(Objects.requireNonNull(getView()), new ReaderObserver());
    }

    public void nextChapter() {
        repository.setSelectedParagraph(0);
        readerLiveData = repository.getNextChapter();
        readerLiveData.observe(Objects.requireNonNull(getView()), new ReaderObserver());
    }

    public void previousChapter() {
        repository.setSelectedParagraph(0);
        readerLiveData = repository.getPreviousChapter();
        readerLiveData.observe(Objects.requireNonNull(getView()), new ReaderObserver());
    }

    public void updateParagraph(int paragraph) {
        repository.setSelectedParagraph(paragraph);
    }

    /*
     * Bookmark Methods
     */
    public void loadBookmark(BookmarkEntity reference) {
        repository.setSelectedBook(reference.getBookId());
        repository.setSelectedChapter(reference.getChapter());
        repository.setSelectedParagraph(reference.getParagraph());
        loadChapterText();
    }

    /*
     * Update tts reading progress
     */
    public void updateReadingProgress(String progress) {
        ttsProgress = Integer.valueOf(progress);
    }

    /*
     * Get tts reading progress
     */
    public int getReadingProgress() {
        return ttsProgress;
    }

    class ReaderObserver implements Observer<ChapterEntity> {
        @Override
        public void onChanged(@NotNull ChapterEntity chapter) {
            if (isViewAttached() & chapter != null) {
                ReaderEntity model = new ReaderEntity( chapter.getVolumeId(), chapter.getBookId(), chapter.getChapter());
                model.setContent( chapter.getContent() );
                model.setPosition( repository.getSelectedParagraph() );

                // Update in case of next/previous chapter
                repository.setSelectedVolume(model.getVolumeId());
                repository.setSelectedBook(model.getBookId());
                repository.setSelectedChapter(model.getChapter());
                repository.persistLastLocation();
                ttsProgress = 0;

                Objects.requireNonNull(getView()).setData(model);
                getView().showContent();
            }
        }
    }
}

