package info.scriptures.rescriptures.ui.library;

public interface ClickListener {

    void onClicked(String id);
}
