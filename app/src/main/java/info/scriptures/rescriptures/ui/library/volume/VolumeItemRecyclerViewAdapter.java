package info.scriptures.rescriptures.ui.library.volume;

import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import info.scriptures.rescriptures.R;
import info.scriptures.rescriptures.data.entity.VolumeEntity;

/**
 * {@link RecyclerView.Adapter} that can display a {@link VolumeEntity} and makes a call to the
 * TODO: Replace the implementation with code for your data type.
 */
public class VolumeItemRecyclerViewAdapter extends RecyclerView.Adapter<VolumeItemRecyclerViewAdapter.LibraryViewHolder> {

    private final List<VolumeEntity> mValues;

//    private final NavigationViewModel viewModel;

    public VolumeItemRecyclerViewAdapter(List<VolumeEntity> entities) {
        mValues = entities;

//        viewModel = ViewModelProviders.of().get(NavigationViewModel.class);
    }

    @Override
    public LibraryViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForItem = R.layout.fragment_library_item;
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForItem, viewGroup, false);
        LibraryViewHolder viewHolder = new LibraryViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final LibraryViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class LibraryViewHolder extends RecyclerView.ViewHolder {
        private final TextView mTextView;

        public LibraryViewHolder(View itemView) {
            super(itemView);

            mTextView = (TextView) itemView.findViewById(R.id.library_item);

            mTextView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                }

            });
        }

        public void bind(int index) {
            mTextView.setText(mValues.get(index).getName());
        }

//        private void setVolume(String volume) {
//
//        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTextView.getText() + "'";
        }
    }
}
