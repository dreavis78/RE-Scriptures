package info.scriptures.rescriptures.ui.reader;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.speech.tts.Voice;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.GestureDetectorCompat;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.MvpLceViewStateActivity;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.data.RetainingLceViewState;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.scriptures.rescriptures.R;
import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.entity.BookmarkEntity;
import info.scriptures.rescriptures.dialogs.AddBookmarkDialogFragment;
import info.scriptures.rescriptures.dialogs.BookmarkDialogFragment;
import info.scriptures.rescriptures.ui.events.ColorSchemeChangedEvent;
import info.scriptures.rescriptures.ui.library.LibraryActivity;
import info.scriptures.rescriptures.ui.settings.SettingsActivity;
import info.scriptures.rescriptures.util.PlayAudioTask;
import info.scriptures.rescriptures.util.ReadingSettingsService;
import info.scriptures.rescriptures.util.ScriptureReferenceService;
import info.scriptures.rescriptures.util.ScriptureWebViewClient;
import info.scriptures.rescriptures.util.ShorthandTitlesService;
import info.scriptures.rescriptures.util.ThemeUtil;
import info.scriptures.rescriptures.util.WebViewJSInterface;
import info.scriptures.rescriptures.viewmodel.BookmarkViewModel;
import timber.log.Timber;

public class ReaderActivity
        extends MvpLceViewStateActivity<WebView, ReaderEntity, ReaderView, ReaderPresenter>
        implements ReaderView,
        View.OnTouchListener,
        BookmarkDialogFragment.BookmarkDialogListener,
        AddBookmarkDialogFragment.AddBookmarkDialogListener,
        TextToSpeech.OnInitListener
{
    @BindView(R.id.contentFrame) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.loadingView) public ProgressBar loadingView;
    @BindView(R.id.errorView) View errorView;
    @BindView(R.id.contentView) WebView view;
    @BindView(R.id.appbar) AppBarLayout appBar;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.toolbar_title) TextView toolbarTitle;
    @BindView(R.id.navigation) BottomNavigationView navigation;
    @BindView(R.id.tts) BottomNavigationItemView playButton;

    public ScriptureRepository repository;
    private EventBus eventBus;
    private ReaderEntity model;

    private String font;
    private int textSize;
    private String colorScheme;
//    private int margin;
//    private int lineSpacing;

    private GestureDetectorCompat gestureDetector;
    private ScriptureWebViewClient webViewClient;

    public TextToSpeech tts;
    public int chunkSize = 0;

    private boolean useDefaultVoice = false;
    private BookmarkViewModel bookmarkModel;
    private Menu menu;

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = item -> {
        switch (item.getItemId()) {
            case R.id.home:
                goToLibrary();
                return true;
            case R.id.search:
                onSearchRequested();
                return true;
            case R.id.tts:
                onReadAloud();
                return true;
            case R.id.settings:
                onOpenSettingsDialog();
                return true;
        }
        return false;
    };

    @Override protected void onCreate(Bundle savedInstanceState) {
        // Get instance of repository
        if (repository == null) {
            repository = ScriptureRepository.getInstance(getApplication());
        }
        super.onCreate(savedInstanceState);

        ReadingSettingsService.init(this.getApplicationContext());
        this.font = ReadingSettingsService.getFont();
        this.textSize = ReadingSettingsService.getTextSize();

        // Get the Bookmarks ViewModel

        BookmarkViewModel.BookmarkViewModelFactory factory =
                new BookmarkViewModel.BookmarkViewModelFactory( getApplication(), repository );
        bookmarkModel = ViewModelProviders.of(this, factory).get(BookmarkViewModel.class);
        bookmarkModel.getBookmarks().observe(this, new BookmarkObserver());

        // Set theme
        this.colorScheme = ReadingSettingsService.getColorScheme();
        switch(this.colorScheme) {
            case "dark":
                ThemeUtil.onActivityCreateSetTheme(this, ThemeUtil.THEME_DARK);
                break;
            case "light":
                ThemeUtil.onActivityCreateSetTheme(this, ThemeUtil.THEME_LIGHT);
                break;
            case "sepia":
            default:
                ThemeUtil.onActivityCreateSetTheme(this, ThemeUtil.THEME_SEPIA);
        }

        setRetainInstance(true);
        setContentView(R.layout.activity_reader);
        ButterKnife.bind(this);

        navigation.setOnNavigationItemSelectedListener(navListener);

        eventBus = EventBus.getDefault();

        GestureListener gestureListener = new GestureListener();
        gestureDetector = new GestureDetectorCompat(this, gestureListener);
        gestureDetector.setOnDoubleTapListener(gestureListener);

        webViewClient = new ScriptureWebViewClient(this, contentView, presenter);
        contentView.setWebViewClient(webViewClient);
        contentView.addJavascriptInterface(new WebViewJSInterface(this, contentView), "handler");
        setSupportActionBar(toolbar);

        // Creating an instance of MediaPlayer
//        mediaPlayer = new MediaPlayer();
//        audioBundle = new Bundle();

        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, TextToSpeech.Engine.CHECK_VOICE_DATA_PASS);
    }

    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        if (requestCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                // success, create the TTS instance
                tts = new TextToSpeech(getApplicationContext(), this);
            } else {
                // missing data, install it
                Intent installIntent = new Intent();
                installIntent.setAction(
                        TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_reader, menu);
        if (model != null) {
            toggleAddBookmarkIcon(model.getPosition());
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.bookmark_list) {
            onOpenBookmarkDialog();
        }

        if (id == R.id.add_bookmark) {
            if(bookmarkModel.isBookmarked(model.getPosition())) {
                String reference = ScriptureReferenceService.getPrettyScriptureReference(
                        model.getBookId(), model.getChapter(), model.getPosition() );
                Toast.makeText(this, reference + " already bookmarked", Toast.LENGTH_SHORT).show();
            } else {
                onOpenAddBookmarkDialog();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        float positionTopView = contentView.getTop();
        float contentHeight = contentView.getContentHeight();
        float currentScrollPosition = contentView.getScrollY();
        float percentWebview = (currentScrollPosition - positionTopView) / contentHeight;
        outState.putString("SCROLL_POSITION", Float.toString(percentWebview));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final float percentScrolled = Float.valueOf(Objects.requireNonNull(savedInstanceState.getString("SCROLL_POSITION")));
        webViewClient.setScrollPosition(percentScrolled);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (tts != null && tts.isSpeaking()) {
            playButton.setSelected(true);
        } else {
            playButton.setSelected(false);
        }
    }

    // View Settings Event Handling
    @Override public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override public void onStop() {
        eventBus.unregister(this);
        playButton.setSelected(false);
        tts.stop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        playButton.setSelected(false);
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    @Override public void onRestart() {
        super.onRestart();

        // Update font
        String newFont = ReadingSettingsService.getFont();
        if (!newFont.equals(this.font)) {
            this.font = newFont;
            onFontChangedEvent();
        }

        // Update text size
        int newTextSize = ReadingSettingsService.getTextSize();
        if (newTextSize != this.textSize) {
            this.textSize = newTextSize;
            onTextSizeChangedEvent();
        }

        // Update color scheme
        String newColorScheme = ReadingSettingsService.getColorScheme();
        if (!newColorScheme.equals(this.colorScheme)) {
            this.colorScheme = newColorScheme;
            onColorSchemeChangedEvent(new ColorSchemeChangedEvent(this.colorScheme));
        }

        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, TextToSpeech.Engine.CHECK_VOICE_DATA_PASS);
    }


    @NonNull
    @Override public LceViewState<ReaderEntity, ReaderView> createViewState() {
        return new RetainingLceViewState<>();
    }

    @Override public void loadData(boolean pullToRefresh) {
//        boolean firstLoad = false;
//        String action = getIntent().getAction();
//        if (action != null && action.equals(Intent.ACTION_MAIN)) {
//            firstLoad = true;
//        }

        presenter.loadChapterText();
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getString(R.string.error_reader);
    }

    @NonNull
    @Override public ReaderPresenter createPresenter() {
        return new ReaderPresenter(repository);
    }

    @Override public void setData(ReaderEntity model) {
        this.model = model;
        this.bookmarkModel.filterActiveBookmarks(model.getBookId(), model.getChapter());
    }

    @SuppressLint({"SetJavaScriptEnabled", "ClickableViewAccessibility" })
    @Override public void showContent() {
        contentView.removeAllViews();
        super.showContent();
        WebView.setWebContentsDebuggingEnabled(true);
        contentView.setWebChromeClient(new WebChromeClient());
        WebSettings settings = contentView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        settings.setJavaScriptEnabled(true);
        webViewClient.setParagraph(model.getPosition());
        String wrapperHtml = "<html><head></head><body style='display:none;'>" + model.getContent() +
                "<script type='text/javascript' src='file:///android_asset/jquery-3.4.1.min.js'></script>" +
                "<script type='text/javascript' src='file:///android_asset/utils.js'></script>" +
                "</body></html>";
        contentView.loadDataWithBaseURL(
            "file:///android_asset/",
                    wrapperHtml,
            "text/html; charset=utf-8",
            null,
            null
        );
        contentView.setOnTouchListener(this);
        toolbarTitle.setText(ShorthandTitlesService.getShorthandTitle(model.getBookId(), model.getChapter()));

        if (menu != null) {
            toggleAddBookmarkIcon(model.getPosition());
        }
    }

    @Override public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
    }

    @Override public ReaderEntity getData() {
        return this.model;
    }

    @Override
    public boolean onSearchRequested() {
        return super.onSearchRequested();
    }

    private void onOpenBookmarkDialog() {
        DialogFragment bookmarkDialog = new BookmarkDialogFragment();
        bookmarkDialog.show(getSupportFragmentManager(), "bookmark");
    }

    @Override
    public void onBookmarkSelected(BookmarkEntity bookmarkItem) {
        presenter.loadBookmark(bookmarkItem);
    }

    public void onOpenAddBookmarkDialog() {
        String reference = ScriptureReferenceService.getPrettyScriptureReference(
                model.getBookId(), model.getChapter(), model.getPosition() );

        DialogFragment addBookmarkDialog = AddBookmarkDialogFragment.newInstance(reference);
        addBookmarkDialog.show(getSupportFragmentManager(), "add_bookmark");
    }

    private void onOpenSettingsDialog() {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    @Override
    public void onBookmarkAdded(@NonNull String name) {
        bookmarkModel.addBookmark(name);
        Toast.makeText(this, "Bookmark '" + name + "' added", Toast.LENGTH_SHORT).show();
    }

    private void onReadAloud() {
        if (tts.isSpeaking()) {
            playButton.setSelected(false);
            tts.stop();
        } else {
            if (tts.getVoice() != null) {
                if (this.useDefaultVoice) {
                    Toast.makeText(
                            this,
                            "No downloaded voice matches the current settings. Using the default voice instead.",
                            Toast.LENGTH_LONG
                    ).show();
                }
                playButton.setSelected(true);
                tts.setOnUtteranceProgressListener(new AudioSavedListener());
                PlayAudioTask task = new PlayAudioTask(this);
                int start = presenter.getReadingProgress();
                task.execute(
                    model.getContent(),
                    (start > 0) ? Integer.toString(start) : Integer.toString(model.getPosition())
                );
            } else {
                Toast.makeText(
                        this,
                    "No voices found. Please download voices under Settings -> System -> Language & Input -> Text-to-speech output.",
                        Toast.LENGTH_LONG
                ).show();
            }
        }
    }

    private void goToLibrary() {
        Intent i = new Intent(this, LibraryActivity.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.StyledAlertDialog))
                    .setMessage("Are you sure you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", (dialog, which) -> {
                        moveTaskToBack(true);
                        System.exit(0);
                    })
                    .setNegativeButton("No", null)
                    .show();
        } else {
            super.onBackPressed();

//            if (repository == null) {
//                repository = ScriptureRepository.getInstance(getApplication());
//            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onFontChangedEvent() {
        this.finish();
        this.startActivity(new Intent(this, this.getClass()));
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onTextSizeChangedEvent() {
        this.finish();
        this.startActivity(new Intent(this, this.getClass()));
    }

//    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
//    public void onMarginsChangedEvent(MarginsChangedEvent event) {
//        Timber.i("Reader Activity margins changed: " + event.getMargin());
//        final int childCount = contentView.getChildCount();
//        for (int i = 0; i < childCount; i++) {
//            View v = contentView.getChildAt(i);
//            if (v instanceof HtmlTextView) {
//                // Set font size (in SP units)
////                ((HtmlTextView) v).setTextSize(TypedValue.COMPLEX_UNIT_SP, event.getMargin());
//            }
//        }
//    }

//    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
//    public void onLineSpacingChangedEvent(LineSpacingChangedEvent event) {
//        Timber.i("Reader Activity line spacing changed: " + event.getLineSpacing());
//        final int childCount = contentView.getChildCount();
//        for (int i = 0; i < childCount; i++) {
//            View v = contentView.getChildAt(i);
//            if (v instanceof HtmlTextView) {
//                // Set font size (in SP units)
////                ((HtmlTextView) v).setTextSize(TypedValue.COMPLEX_UNIT_SP, event.getTextSize());
//            }
//        }
//    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onColorSchemeChangedEvent(ColorSchemeChangedEvent event) {
        String colorScheme = event.getColorScheme();

        // Change Activity Bar colors
        ThemeUtil.changeToTheme(this, colorScheme);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        this.gestureDetector.onTouchEvent(e);
        return super.onTouchEvent(e);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.gestureDetector.onTouchEvent(motionEvent);
        view.performClick();
        return false;
    }

    public void updateParagraph(int paragraph) {
        model.setPosition(paragraph);
        presenter.updateParagraph(paragraph);
        toggleAddBookmarkIcon(paragraph);
    }

    @Override
    public void onInit(int status) {
        if (status != TextToSpeech.ERROR) {
            Voice voice = null;
            tts.setLanguage(new Locale(ReadingSettingsService.getVoiceLocale()));
            tts.setSpeechRate(Float.valueOf(ReadingSettingsService.getVoiceSpeed()));
            Set<Voice> voices = tts.getVoices();

            if (voices != null && voices.size() > 0) {
                for (Voice tmpVoice : voices) {
                    if (
                            tmpVoice.getName().contains(ReadingSettingsService.getVoiceGender()) &&
                            tmpVoice.getName().contains(ReadingSettingsService.getVoiceLocale())
                    ) {
                        voice = tmpVoice;
                        break;
                    }
                }
            }

            if (voice != null) {
                tts.setVoice(voice);
            } else {
                this.useDefaultVoice = true;
            }
        }
    }

    private void toggleAddBookmarkIcon(int paragraph) {
        Context context = this.getBaseContext();
        runOnUiThread(() -> {
            if (bookmarkModel.isBookmarked(paragraph)) {
                menu.findItem(R.id.add_bookmark).setIcon(ContextCompat.getDrawable(context, R.drawable.baseline_bookmark_white_24dp));
            } else {
                menu.findItem(R.id.add_bookmark).setIcon(ContextCompat.getDrawable(context, R.drawable.baseline_bookmark_border_white_24dp));
            }
        });
    }

    class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_DISTANCE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            float distanceX = e2.getX() - e1.getX();
            float distanceY = e2.getY() - e1.getY();
            if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                tts.stop();
                playButton.setSelected(false);
                if (distanceX > 0)
                    presenter.previousChapter();
                else
                    presenter.nextChapter();
                return true;
            }
            return false;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if(toolbar.getVisibility() == View.GONE) {
                toolbar.setVisibility(View.VISIBLE);
                navigation.setVisibility(View.VISIBLE);
            } else {
                toolbar.setVisibility(View.GONE);
                navigation.setVisibility(View.GONE);
            }
            return super.onSingleTapConfirmed(e);
        }
    }

    class BookmarkObserver implements Observer<List<BookmarkEntity>> {
        @Override
        public void onChanged(@NotNull List<BookmarkEntity> bookmarks) {}
    }

    public class AudioSavedListener extends UtteranceProgressListener {

        @Override public void onStart(String s) { }
        @Override public void onError(String s) { }

        @Override
        public void onDone(String s) {
            if(Integer.valueOf(s) >= chunkSize) {
                tts.stop();
                playButton.setSelected(false);
                presenter.updateReadingProgress("0");
            } else {
                presenter.updateReadingProgress(s);
//                runOnUiThread(() -> {
//                    webViewClient.setParagraph(Integer.parseInt(s) - 1);
//                    webViewClient.scrollToNextParagraph();
//                });
            }
        }
    }
}
