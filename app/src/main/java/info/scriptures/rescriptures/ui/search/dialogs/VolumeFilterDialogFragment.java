package info.scriptures.rescriptures.ui.search.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import info.scriptures.rescriptures.R;
import timber.log.Timber;

public class VolumeFilterDialogFragment extends DialogFragment {

    public interface VolumeFilterDialogListener {
        void onVolumeFilterSelected(int selection);
    }

    VolumeFilterDialogListener listener;

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (VolumeFilterDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
            + " must implement VolumeFilterDialogListener");
        }
    }

    @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(R.array.volumes_array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onVolumeFilterSelected(i);
            }
        });

        return builder.create();
    }
}
