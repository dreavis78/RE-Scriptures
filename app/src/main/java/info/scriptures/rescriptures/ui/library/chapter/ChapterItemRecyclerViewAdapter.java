package info.scriptures.rescriptures.ui.library.chapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import info.scriptures.rescriptures.R;
import info.scriptures.rescriptures.data.entity.ChapterEntity;
import timber.log.Timber;

/**
 * {@link RecyclerView.Adapter} that can display a {@link ChapterEntity} and makes a call to the
 * TODO: Replace the implementation with code for your data type.
 */
public class ChapterItemRecyclerViewAdapter extends RecyclerView.Adapter<ChapterItemRecyclerViewAdapter.LibraryViewHolder> {

    private final List<ChapterEntity> mValues;

    public ChapterItemRecyclerViewAdapter(List<ChapterEntity> entities) {
        mValues = entities;
    }

    @Override
    public LibraryViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForItem = R.layout.fragment_library_item;
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForItem, viewGroup, false);
        LibraryViewHolder viewHolder = new LibraryViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final LibraryViewHolder holder, int position) {
        holder.bind(position);

//        holder.mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (null != mListener) {
//                    // Notify the active callbacks interface (the activity, if the
//                    // fragment is attached to one) that an item has been selected.
//                    mListener.onListFragmentInteraction(holder.mItem);
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class LibraryViewHolder extends RecyclerView.ViewHolder {
        private final TextView mTextView;

        public LibraryViewHolder(View itemView) {
            super(itemView);

            mTextView = (TextView) itemView.findViewById(R.id.library_item);

            mTextView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Timber.i("Element " + getAdapterPosition() + " clicked.");
                }

            });
        }

        public void bind(int index) {
            mTextView.setText(mValues.get(index).getChapter() + "");
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTextView.getText() + "'";
        }
    }
}
