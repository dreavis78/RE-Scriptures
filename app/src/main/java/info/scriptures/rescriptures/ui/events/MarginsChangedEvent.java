package info.scriptures.rescriptures.ui.events;

public class MarginsChangedEvent {

    public static final int DECREASE = 0;
    public static final int INCREASE = 1;
    public static final int STEP = 2;
    public static final int MIN_MARGIN = 0;
    public static final int MAX_MARGIN = 8;

    private final int margin;
    
    public MarginsChangedEvent(int margin) {
        this.margin = margin;
//        if(direction == DECREASE &&  margin != MarginsChangedEvent.MIN_MARGIN) {
//            this.margin = margin - STEP;
//        } else if(direction == INCREASE && margin != MarginsChangedEvent.MAX_MARGIN) {
//            this.margin = margin + STEP;
//        } else {
//            this.margin = margin;
//        }
    }
    
    public int getMargin() { return margin; }
}
