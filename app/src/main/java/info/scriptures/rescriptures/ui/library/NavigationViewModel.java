package info.scriptures.rescriptures.ui.library;

import androidx.lifecycle.ViewModel;

public class NavigationViewModel extends ViewModel {

    private String activeVolume = "ot";
    private String activeBook = "genesis";
    private int activeChapter = 1;

    public String getActiveVolume() {
        return activeVolume;
    }

    public void setActiveVolume(String activeVolume) {
        this.activeVolume = activeVolume;
    }

    public String getActiveBook() {
        return activeBook;
    }

    public void setActiveBook(String activeBook) {
        this.activeBook = activeBook;
    }

    public int getActiveChapter() {
        return activeChapter;
    }

    public void setActiveChapter(int activeChapter) {
        this.activeChapter = activeChapter;
    }
}
