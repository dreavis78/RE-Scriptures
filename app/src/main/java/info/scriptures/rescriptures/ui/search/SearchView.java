package info.scriptures.rescriptures.ui.search;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import info.scriptures.rescriptures.data.entity.ParagraphEntity;

public interface SearchView extends MvpLceView<List<ParagraphEntity>>, LifecycleOwner {
    @NonNull @Override Lifecycle getLifecycle();
}
