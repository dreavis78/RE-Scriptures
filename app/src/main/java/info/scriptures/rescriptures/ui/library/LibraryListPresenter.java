package info.scriptures.rescriptures.ui.library;

import info.scriptures.rescriptures.BaseListPresenter;
import info.scriptures.rescriptures.data.LibraryItemRepository;
import info.scriptures.rescriptures.data.ScriptureRepository;
import timber.log.Timber;

public class LibraryListPresenter extends BaseListPresenter<LibraryItemRepository, LibraryItemContract.View> {

    @Override
    protected void updateView() {
        view().setItemName(model.getLabel(), model.isSelected());
        view().setItemId(model.getId());
    }

    @Override
    public void onClicked(String tag) {
        if (setupDone()) {
            view().onClicked(model.getId());
        }
    }
}
