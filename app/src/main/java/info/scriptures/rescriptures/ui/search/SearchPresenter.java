package info.scriptures.rescriptures.ui.search;

import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.entity.BookEntity;
import info.scriptures.rescriptures.data.entity.VolumeEntity;
import info.scriptures.rescriptures.data.searchFilters.Books;
import info.scriptures.rescriptures.data.searchFilters.SortOrder;
import info.scriptures.rescriptures.data.entity.ParagraphEntity;
import info.scriptures.rescriptures.data.searchFilters.Works;
import timber.log.Timber;

public class SearchPresenter extends MvpBasePresenter<SearchView> {

    private static int PRESENTER_ID_FOR_LOGGING = 0;

    private final String TAG = "SearchPresenter" + (PRESENTER_ID_FOR_LOGGING++);
    private String currentQuery = null;
    private VolumeEntity selectedWork = Works.ALL;
    private BookEntity selectedBook = Books.OC_ALL;
    private String sortOrder = SortOrder.RELEVANCE;
    private int limit = 50;
    private int offset = 0;

    private final ScriptureRepository repository;


    SearchPresenter(@NonNull ScriptureRepository repository) {
        this.repository = repository;
    }

    @Override public void attachView(SearchView view) {
        super.attachView(view);
    }

    public void loadSearchResults() { this.loadSearchResults(this.currentQuery); }
    public void loadSearchResults(String query) {
        if (!query.equals(this.currentQuery)) {
            this.currentQuery = query;
            this.offset = 0;
        }

        try {
            LiveData<List<ParagraphEntity>> paragraphEntities = repository.getKeywordSearchResults(this.currentQuery, selectedWork, selectedBook, sortOrder, limit, offset);
            paragraphEntities.observe(Objects.requireNonNull(getView()), new SearchObserver(this.currentQuery));
        } catch(SQLiteException e) {
           e.printStackTrace();
        }
    }

    public void setWorksFilter(VolumeEntity newWork) {
        if (!this.selectedWork.equals(newWork.getId())) {
            this.selectedWork = newWork;
            this.offset = 0;
        }
    }

    public void setBooksFilter(BookEntity newBook) {
        if (!this.selectedBook.equals(newBook.getBookId())) {
            this.selectedBook = newBook;
            this.offset = 0;
        }
    }

    public void setSortOrder(String newSortOrder) {
        if (this.sortOrder != newSortOrder) {
            this.sortOrder = newSortOrder;
            this.offset = 0;
        }
    }

    public int getLimit() { return this.limit; }

    class SearchObserver implements Observer<List<ParagraphEntity>> {
        final protected String query;

        SearchObserver(String query) {
            this.query = query;
        }

        @Override
        public void onChanged(@Nullable List<ParagraphEntity> results) {
            offset += results.size();
            if (results != null) new FormatResultsTask(query).execute(results);
        }
    }

    private class FormatResultsTask extends AsyncTask<List<ParagraphEntity>, Void, List<ParagraphEntity>> {
        final private String query;

        FormatResultsTask(String query) {
            this.query = query.replaceAll("'", "[’']");
        }

        @SafeVarargs
        @Override
        protected final List<ParagraphEntity> doInBackground(List<ParagraphEntity>... params) {
           List<ParagraphEntity> searchResults = params[0];
           for(ParagraphEntity p : searchResults) {
               // Search for smartquotes
               // Replace non-matching sentences with ellipses
               String paragraph = highlightQuery(extractSentences(p.getParagraph(), query), query);
               p.setParagraph(paragraph);
           }

           return searchResults;
        }

        @Override
        protected void onPostExecute(List<ParagraphEntity> results) {
            if (isViewAttached()) {
                Objects.requireNonNull(getView()).setData(results);
                getView().showContent();
            }
        }

        private String extractSentences(String paragraph, String query) {
            LinkedHashSet<String> matches = new LinkedHashSet<>();

            Boolean isPhrase = (query.startsWith("\"") && query.endsWith("\""));
            String[] terms = query.split(" ");
            if (!isPhrase && terms.length > 1) {
                for (int i = 0, len = terms.length; i < len; i++) {
                    String t = terms[i];
                    if (t.startsWith("-")) {
                        terms[i] = "NOT " + t.substring(1);
                    }

                    Pattern p = Pattern.compile("[^.?!]*(?<=[.?\\s!])" + t + "(?=[\\s.?!,;])[^.?!]*[.?!]", Pattern.CASE_INSENSITIVE);
                    Matcher m = p.matcher(paragraph);
                    while(m.find()) {
                        String match = m.group();
                        matches.add(match);
                    }
                }
            } else {
                query = query.replaceAll("\"", "");
                Pattern p = Pattern.compile("[^.?!]*(?<=[.?\\s!])(?i:" + query + ")(?=[\\s.?!,;])[^.?!]*[.?!]", Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(paragraph);
                while(m.find()) {
                    String match = m.group();
                    matches.add(match);
                }
            }

            if (matches.size() == 0) {
                matches.add(paragraph);
            }

            return TextUtils.join(" ...", matches);
        }

        private String highlightQuery(String paragraph, String query) {
            Boolean isPhrase = (query.startsWith("\"") && query.endsWith("\""));
            String[] terms = query.split(" ");
            if (!isPhrase && terms.length > 1) {
                for (int i = 0, len = terms.length; i < len; i++) {
                    String t = terms[i];
                    paragraph = paragraph.replaceAll("(?i)" + t, "<b>" + "$0" + "</b>");
                }

            } else {
                query = query.replaceAll("\"", "");
                paragraph = paragraph.replaceAll("(?i)" + query, "<b>" + "$0" + "</b>");
            }
            return paragraph;
        }
    }
}
