package info.scriptures.rescriptures.ui.library;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import info.scriptures.rescriptures.R;
import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.ui.events.ColorSchemeChangedEvent;
import info.scriptures.rescriptures.ui.reader.ReaderActivity;
import info.scriptures.rescriptures.ui.settings.SettingsActivity;
import info.scriptures.rescriptures.util.ReadingSettingsService;
import info.scriptures.rescriptures.util.ShorthandTitlesService;
import info.scriptures.rescriptures.util.ThemeUtil;
import info.scriptures.rescriptures.util.TimberLogImpl;

public class LibraryActivity
        extends AppCompatActivity
        implements OnTabNavigationListener, OnNavigateToReaderListener {
    @BindView(R.id.mainContent) View mainContent;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.toolbar_title) TextView toolbarTitle;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.container) ViewPager viewPager;

    /**
     * The {@link androidx.viewpager.widget.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private PagerAdapter mPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private ScriptureRepository repository;

    private EventBus eventBus;

    private String font;
    private String colorScheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set logger
        TimberLogImpl.init();

        // Get instance of repository
        repository = ScriptureRepository.getInstance(getApplication());

        // Go to last location on load, if applicable
        String action = getIntent().getAction();
        if (action != null && action.equals(Intent.ACTION_MAIN)) {
            if (repository.hasLastLocation()) {
                onNavigateToReader();
            }
        }

        // Initialize Shared Preferences
        ReadingSettingsService.init(getApplicationContext());

        // Set theme
        this.colorScheme = ReadingSettingsService.getColorScheme();
        switch(this.colorScheme) {
            case "dark":
                ThemeUtil.onActivityCreateSetTheme(this, ThemeUtil.THEME_DARK);
                break;
            case "light":
                ThemeUtil.onActivityCreateSetTheme(this, ThemeUtil.THEME_LIGHT);
                break;
            case "sepia":
            default:
                ThemeUtil.onActivityCreateSetTheme(this, ThemeUtil.THEME_SEPIA);
        }

        setTheme(R.style.AppTheme);

        // Load previously saved state, if available
        if (savedInstanceState != null) {
        }

        setContentView(R.layout.activity_library);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
//        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setAdapter(mPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.volume_tab_text));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.book_tab_text));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.chapter_tab_text));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout) {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mViewPager.setCurrentItem(position);
                mPagerAdapter.notifyDataSetChanged();
                setActionBarTitle(position);
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        eventBus = EventBus.getDefault();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_library, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {
            onSearchRequested();
//        } else if (id == R.id.bookmarks) {
//            DialogFragment bookmarksDialog = new BookmarkDialogFragment(this,this);
//            bookmarksDialog.show(getSupportFragmentManager(), "bookmarks");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabNavigation(int tabPosition) {
        mViewPager.setCurrentItem(tabPosition);
    }

    @Override
    public void onNavigateToReader() {
        Intent i = new Intent(this, ReaderActivity.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        int currentTab = mViewPager.getCurrentItem();
        if (currentTab > 0) {
            mViewPager.setCurrentItem(currentTab - 1);
        } else {
//            super.onBackPressed();
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.baseAlertDialogStyle))
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, which) -> {
                    moveTaskToBack(true);
                    System.exit(0);
                })
                .setNegativeButton("No", null)
                .show();
        }
    }

    @Override
    public void onStart() {
        // Only restore reading place when app first loads
//        if (LibraryActivity.shouldRestoreReadingPlace) {
//            restoreReadingPlace();
//        }

        super.onStart();

        // View Settings Event Handling
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
//        LibraryActivity.shouldRestoreReadingPlace = true;
        super.onDestroy();
    }

    @Override
    public void onRestart() {
//        LibraryActivity.shouldRestoreReadingPlace = false;
        super.onRestart();

        Resources res = getResources();

        // Update font
//        String newFont = ReadingSettingsService.getFont();
//        if (!newFont.equals(this.font)) {
//            this.font = newFont;
//            onFontChangedEvent(new FontChangedEvent(this.font));
//        }

        // Update color scheme
        String newColorScheme = ReadingSettingsService.getColorScheme();
        if (!newColorScheme.equals(this.colorScheme)) {
            this.colorScheme = newColorScheme;
            onColorSchemeChangedEvent(new ColorSchemeChangedEvent(this.colorScheme));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onColorSchemeChangedEvent(ColorSchemeChangedEvent event) {
        String colorScheme = event.getColorScheme();

        // Change Activity Bar colors
        ThemeUtil.changeToTheme(this, colorScheme);
    }

    @Override public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void onOpenSettingsDialog() {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    private void setActionBarTitle(int tabPosition) {
        String title = "";
        switch(tabPosition) {
            case 1:
                title = ShorthandTitlesService.getShorthandVolume(repository.getSelectedVolume());
                break;
            case 2:
                title = ShorthandTitlesService.getShorthandBook(repository.getSelectedBook());
                break;
            case 0:
            default:
                title = getResources().getString(R.string.default_title);
        }

        toolbarTitle.setText(title);
    }
}
