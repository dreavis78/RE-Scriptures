package info.scriptures.rescriptures.ui.library;

public interface LibraryItemContract {

    interface View {

        void setItemId(String itemId);

        void setItemName(String itemName, Boolean selected);

        void onClicked(String libraryItem);
    }
}
