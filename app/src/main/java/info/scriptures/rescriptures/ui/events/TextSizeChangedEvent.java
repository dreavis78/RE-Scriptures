package info.scriptures.rescriptures.ui.events;

public class TextSizeChangedEvent {
    private final int textSize;

    public TextSizeChangedEvent(int textSize) {
        this.textSize = textSize;
    }

    public int getTextSize() { return textSize; }
}
