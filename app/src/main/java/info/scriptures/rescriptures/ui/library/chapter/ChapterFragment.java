package info.scriptures.rescriptures.ui.library.chapter;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import info.scriptures.rescriptures.R;
import info.scriptures.rescriptures.data.LibraryItemRepository;
import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.entity.ChapterEntity;
import info.scriptures.rescriptures.ui.library.ClickListener;
import info.scriptures.rescriptures.ui.library.LibraryContract;
import info.scriptures.rescriptures.ui.library.LibraryListAdapter;
import info.scriptures.rescriptures.ui.library.OnNavigateToReaderListener;
import info.scriptures.rescriptures.util.ShorthandTitlesService;

public class ChapterFragment extends Fragment implements LibraryContract.View<ChapterTabPresenter>{

    private ChapterTabPresenter presenter;

    private View mNoContentView;

    private List<ChapterEntity> chapters;

    private RecyclerView recyclerView;

    private LibraryListAdapter adapter;

    private ScriptureRepository repository;

    private OnNavigateToReaderListener listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        chapters = new ArrayList<ChapterEntity>();
        repository = ScriptureRepository.getInstance(this.getActivity().getApplication());
        presenter = new ChapterTabPresenter(repository, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.bindView(this);

        try {
            listener = (OnNavigateToReaderListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnTabNavigationListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.unbindView();
    }

    /* Called when fragment becomes visible */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && presenter != null) {
            presenter.updateView();
        }
    }

    @Override
    public void setLibraryItems(List chapters) {
        for(ChapterEntity chapter: (List<ChapterEntity>) chapters) {
            this.chapters.add(chapter);
        }
    }

    @Override
    public void showLibraryItems(List items) {
        String selectedChapter = repository.getSelectedChapter();
        List<LibraryItemRepository> repositories = new ArrayList<>();
        for(ChapterEntity chapter: (List<ChapterEntity>) items) {
            Boolean isSelectedChapter = chapter.getChapter().equals(selectedChapter);
            String selectedBook = repository.getSelectedBook();
            LibraryItemRepository repository = new LibraryItemRepository(
                    chapter.getChapter() + "",
                    ShorthandTitlesService.getShorthandLabel(selectedBook, chapter.getChapter()),
                    isSelectedChapter);
            repositories.add(repository);
        }
        adapter.addItems(repositories);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_library, container, false);

        // Set up library view
        recyclerView = root.findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new LibraryListAdapter(tag -> {
            // callback performed on click
            repository.setSelectedChapter(tag);
            repository.setSelectedParagraph(0);
            listener.onNavigateToReader();
            adapter.notifyDataSetChanged();
        });
        recyclerView.setAdapter(adapter);

        setHasOptionsMenu(true);

        return root;
    }
}
