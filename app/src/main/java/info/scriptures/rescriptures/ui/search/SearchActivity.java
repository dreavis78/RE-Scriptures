package info.scriptures.rescriptures.ui.search;

import android.app.ActionBar;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
//import android.provider.SearchRecentSuggestions;
import android.provider.SearchRecentSuggestions;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Space;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.MvpLceViewStateActivity;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.data.RetainingLceViewState;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.preference.PreferenceManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import info.scriptures.rescriptures.R;
import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.entity.BookEntity;
import info.scriptures.rescriptures.data.entity.VolumeEntity;
import info.scriptures.rescriptures.data.searchFilters.Books;
import info.scriptures.rescriptures.data.searchFilters.SortOrder;
import info.scriptures.rescriptures.data.entity.ParagraphEntity;
import info.scriptures.rescriptures.data.searchFilters.Works;
import info.scriptures.rescriptures.ui.reader.ReaderActivity;
import info.scriptures.rescriptures.ui.search.dialogs.BookFilterDialogFragment;
import info.scriptures.rescriptures.ui.search.dialogs.SortOrderFilterDialogFragment;
import info.scriptures.rescriptures.ui.search.dialogs.VolumeFilterDialogFragment;
import info.scriptures.rescriptures.util.ReadingSettingsService;
import info.scriptures.rescriptures.util.SearchSuggestionProvider;
import info.scriptures.rescriptures.util.ShorthandTitlesService;
import info.scriptures.rescriptures.util.ThemeUtil;
import info.scriptures.rescriptures.util.TypefaceCache;
import timber.log.Timber;

public class SearchActivity extends MvpLceViewStateActivity<LinearLayoutCompat, List<ParagraphEntity>, SearchView, SearchPresenter> implements
    SearchView,
    VolumeFilterDialogFragment.VolumeFilterDialogListener,
    BookFilterDialogFragment.BookFilterDialogListener,
    SortOrderFilterDialogFragment.SortOrderDialogListener
{
    @BindView(R.id.search_scroll) ScrollView searchScroll;
    @BindView(R.id.loadingView) View loadingView;
    @BindView(R.id.volume_filter) LinearLayoutCompat volumeFilter;
    @BindView(R.id.volume_filter__value) TextView volumeFilter__value;
    @BindView(R.id.book_filter) LinearLayoutCompat bookFilter;
    @BindView(R.id.book_filter__value) TextView bookFilter__value;
    @BindView(R.id.sort_order_filter) LinearLayoutCompat sortOrderFilter;
    @BindView(R.id.sort_order_filter__value) TextView sortOrderFilter__value;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.toolbar_title) TextView toolbarTitle;

    private ScriptureRepository repository;
    private List<ParagraphEntity> model;
    private BookEntity[] filteredBooks = getFilteredBooks(Works.ALL);
    private SharedPreferences sharedPreferences;

    private DialogFragment sortOrderDialog;

    private String font;
    private int textSize;
    private String colorScheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ReadingSettingsService.init(this.getApplicationContext());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Set theme
        this.colorScheme = ReadingSettingsService.getColorScheme();
        switch(colorScheme) {
            case "dark":
                ThemeUtil.onActivityCreateSetTheme(this, ThemeUtil.THEME_DARK);
                break;
            case "light":
                ThemeUtil.onActivityCreateSetTheme(this, ThemeUtil.THEME_LIGHT);
                break;
            case "sepia":
            default:
                ThemeUtil.onActivityCreateSetTheme(this, ThemeUtil.THEME_SEPIA);
        }

        setRetainInstance(true);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        // Get instance of repository
        repository = ScriptureRepository.getInstance(getApplication());
        setSupportActionBar(toolbar);

        // Set click listeners
        volumeFilter.setOnClickListener(v -> {
            DialogFragment volumeDialog = new VolumeFilterDialogFragment();
            volumeDialog.show(getSupportFragmentManager(), "volumeFilter");
        });

        bookFilter.setOnClickListener(v -> {
            DialogFragment bookDialog = new BookFilterDialogFragment();
            Bundle bundle = new Bundle();

            String[] filteredBookNames = new String[filteredBooks.length];
            int i = 0;
            for(BookEntity b : this.filteredBooks) {
                filteredBookNames[i] = b.getBookName();
                i++;
            }

            bundle.putStringArray("books", filteredBookNames);
            bookDialog.setArguments(bundle);
            bookDialog.show(getSupportFragmentManager(), "bookFilter");
        });

        sortOrderFilter.setOnClickListener(v -> {
            this.sortOrderDialog = new SortOrderFilterDialogFragment();

            Bundle args = new Bundle();
            args.putInt("search_sort_order", sharedPreferences.getInt("search_sort_order", 0));
            this.sortOrderDialog.setArguments(args);
            this.sortOrderDialog.show(getSupportFragmentManager(), "sortOrderFilter");
        });

        String sort_order_pref = SortOrder.values[sharedPreferences.getInt("search_sort_order", 0)];
        sortOrderFilter__value.setText(sort_order_pref);
        presenter.setSortOrder(sort_order_pref);

        if (Intent.ACTION_SEARCH.equals(getIntent().getAction())) {
            handleIntent(getIntent());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {
            onSearchRequested();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        if (Intent.ACTION_SEARCH.equals(getIntent().getAction())) {
            handleIntent(getIntent());
            loadData(false);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BigDecimal percentScrolled;
        BigDecimal range = BigDecimal.valueOf(searchScroll.getChildAt(0).getHeight() - searchScroll.getHeight());
        if (searchScroll.getScrollY() > 0) {
            percentScrolled = BigDecimal.valueOf(searchScroll.getScrollY()).divide(range, 4, RoundingMode.HALF_UP);
        } else {
            percentScrolled = new BigDecimal("0.0");
        }
        outState.putString("SCROLL_POSITION", percentScrolled.toString());
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final String percentScrolled = savedInstanceState.getString("SCROLL_POSITION");
        searchScroll.post(() -> {
            BigDecimal range = BigDecimal.valueOf(searchScroll.getChildAt(0).getHeight() - searchScroll.getHeight());
            BigDecimal scrollY = range.multiply( new BigDecimal(percentScrolled));
            searchScroll.scrollTo(0, scrollY.setScale(0, BigDecimal.ROUND_HALF_UP).intValueExact());
        });
    }

    @Override public LceViewState<List<ParagraphEntity>, SearchView> createViewState() {
        return new RetainingLceViewState<>();
    }

    @Override public void loadData(boolean pullToRefresh) {
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY).replaceAll("[^A-Za-z0-9'\" ]", "");

            presenter.loadSearchResults(query);
            toolbarTitle.setText(query);
        }
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getString(R.string.error_reader);
    }

    @Override public SearchPresenter createPresenter() {
        // Get instance of repository
        if (repository == null) {
            repository = ScriptureRepository.getInstance(getApplication());
        }

        return new SearchPresenter(repository);
    }

    @Override public void setData(List<ParagraphEntity> model) {
        this.model = model;
    }

    @Override public void showContent() {
        super.showContent();
        contentView.removeAllViews();
        buildSearchResultsUI(this.model);
    }

    @Override public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
    }

    @Override public List<ParagraphEntity> getData() {
        return this.model;
    }

    public void navigateToReader(ParagraphEntity paragraphEntity) {
        Timber.i("onListFragmentInteraction");
        repository.setSelectedVolume(paragraphEntity.getVolumeId());
        repository.setSelectedBook(paragraphEntity.getBookId());
        repository.setSelectedChapter(paragraphEntity.getChapter());
        repository.setSelectedParagraph(paragraphEntity.getPosition());
        Intent i = new Intent(this, ReaderActivity.class);
        startActivity(i);
    }

    private void buildSearchResultsUI(final List<ParagraphEntity> paragraphs) {
//        int backgroundColor = getResources().getColor(R.color.sepiaSchemeBackground);
//        int textColor = getResources().getColor(R.color.sepiaSchemeText);
        Context context = getApplicationContext();
        Resources res = getResources();
        String colorScheme = ReadingSettingsService.getColorScheme();
        int textAppearance;
        String textColor;
        int backgroundColor;
        if (colorScheme.equals(res.getString(R.string.color_scheme_dark))) {
            textAppearance = R.style.darkText;
            textColor = res.getString(R.color.darkSchemeText);
            backgroundColor = res.getColor(R.color.darkSchemeBackground);
        } else if (colorScheme.equals(res.getString(R.string.color_scheme_light))) {
            textAppearance = R.style.lightText;
            textColor = res.getString(R.color.colorPrimary);
            backgroundColor = res.getColor(R.color.lightSchemeBackground);
        } else {
            textAppearance = R.style.sepiaText;
            textColor = res.getString(R.color.sepiaSchemeText);
            backgroundColor = res.getColor(R.color.sepiaSchemeBackground);
        }
        Typeface font = TypefaceCache.get(ReadingSettingsService.getFont());
        int textSize = ReadingSettingsService.getTextSize();
        Timber.i("SearchActivity text size: " + textSize);

        runOnUiThread(() -> {
            // Create default textview margins
            LinearLayoutCompat.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 0);

            if (paragraphs.size() > 0) {
                for (ParagraphEntity paragraph : paragraphs) {
                    TextView textView = new AppCompatTextView(getApplicationContext());
                    textView.setPadding(24, 32, 24, 24);

                    // Set line spacing
                    textView.setLineSpacing(0f, 1.0f);

                    // Set color scheme
//                    textView.setBackgroundColor(backgroundColor);
                    textView.setLayoutParams(layoutParams);

                    // Set text color
//                    textView.setTextColor(getResources().getColor(R.color.sepiaSchemeText));
                    // Set text color
                    if (Build.VERSION.SDK_INT < 23 ) {
                        textView.setTextAppearance(context, textAppearance);
                    } else {
                        textView.setTextAppearance(textAppearance);
                    }
                    textView.setBackgroundColor(backgroundColor);

                    // Set font size (in SP units)
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                    textView.setIncludeFontPadding(false);

                    // Set font family
                    textView.setTypeface(font);

                    SpannableStringBuilder sb = new SpannableStringBuilder();
                    sb.append("(");
                    sb.append(ShorthandTitlesService.getShorthandVolume(paragraph.getVolumeId()));
                    sb.append(") ");

                    String bookId = paragraph.getBookId();
                    String chapter = paragraph.getChapter();
                    if (bookId.equals("glossary")) {
                        sb.append(ShorthandTitlesService.getShorthandTitle(bookId, chapter));
                        sb.append(" - ");
                        sb.append(ShorthandTitlesService.getShorthandLabel(bookId, chapter));
                    } else {
                        sb.append(ShorthandTitlesService.getShorthandTitle(bookId, chapter));
                        int position = paragraph.getPosition();
                        if (position > 0) {
                            sb.append(":");
                            sb.append(Integer.toString(position));
                        }
                    }
                    String linkText = sb.toString();
                    String paragraphText = "\n\n" + paragraph.getParagraph() + "\n";
                    SpannableString spannable = new SpannableString(Html.fromHtml(linkText + paragraphText));
                    spannable.setSpan(new SearchClickableSpan(), 0, linkText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spannable.setSpan(new StyleSpan(Typeface.BOLD), 0, linkText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//                    textView.setLinkTextColor(getResources().getColor(R.color.colorPrimary));

                    textView.setText(spannable, TextView.BufferType.SPANNABLE);
                    textView.setTag(paragraph);
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    contentView.addView(textView, layoutParams);

                    Space sp = new Space(getApplicationContext());
                    LinearLayoutCompat.LayoutParams spaceLayout = new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    sp.setLayoutParams(spaceLayout);
                    contentView.addView(sp);
                }

                if (paragraphs.size() >= presenter.getLimit()) {
                    Button addMore = new Button(this);
                    addMore.setText(R.string.add_more);
                    addMore.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
                    addMore.setOnClickListener(onMoreSearchResults());
                    contentView.addView(addMore);

                    Space sp = new Space(getApplicationContext());
                    LinearLayoutCompat.LayoutParams spaceLayout = new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    sp.setLayoutParams(spaceLayout);
                    contentView.addView(sp);

                }
            } else {
                // No results
                TextView textView = new AppCompatTextView(getApplicationContext());
                textView.setPadding(24, 32, 24, 24);
                LinearLayoutCompat.LayoutParams emptyLayoutParams = new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                // Set font family
                textView.setTypeface(font);

                // Set font size (in SP units)
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                textView.setIncludeFontPadding(false);

                // Set line spacing
                textView.setLineSpacing(0f, 1.0f);

                // Set color scheme
//                textView.setBackgroundColor(backgroundColor);
                if (Build.VERSION.SDK_INT < 23 ) {
                    textView.setTextAppearance(context, textAppearance);
                } else {
                    textView.setTextAppearance(textAppearance);
                }
                textView.setLayoutParams(emptyLayoutParams);
                textView.setGravity(Gravity.CENTER_HORIZONTAL);
                textView.setText(Html.fromHtml("<b><font color='" + textColor + "'>No results found</font></b>"));
                contentView.addView(textView);
            }

            searchScroll.scrollTo(0,0);

            // Add padding on top & bottom
            Display display = getWindow().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            float scale = getResources().getDisplayMetrics().density;
            int sidePadding = (int) (16 * scale + 0.5f);
            contentView.setPadding(sidePadding, 0, sidePadding, 0);
            contentView.setVisibility(View.VISIBLE);
        });
    }

    private BookEntity[] getFilteredBooks(VolumeEntity selectedWork) {
        return Books.getBooksByWork(selectedWork);
    }

    private View.OnClickListener onMoreSearchResults() {
        return v -> {
            presenter.loadSearchResults();
        };
    }

    private void handleIntent(Intent intent) {
        String query = intent.getStringExtra(SearchManager.QUERY).replaceAll("[^A-Za-z0-9'\" ]", "");
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(
                this,
                SearchSuggestionProvider.AUTHORITY,
                SearchSuggestionProvider.MODE);

        suggestions.saveRecentQuery(query, null);
    }

    private class SearchClickableSpan extends ClickableSpan {
        @Override
        public void onClick(View view) {
            navigateToReader((ParagraphEntity) view.getTag());
        }
    }

    @Override public void onVolumeFilterSelected(int selection) {
        VolumeEntity selectedWork = Works.values.get(selection);
        volumeFilter__value.setText(selectedWork.getName());
        this.filteredBooks = this.getFilteredBooks(selectedWork);
        BookEntity selectedBook = this.filteredBooks[0];
        bookFilter__value.setText(selectedBook.getBookName());
        presenter.setWorksFilter(selectedWork);
        presenter.setBooksFilter(selectedBook);
        this.showLoading(false);
        presenter.loadSearchResults();
    }

    @Override public void onBookFilterSelected(int selection) {
        BookEntity selectedBook = this.filteredBooks[selection];
        bookFilter__value.setText(selectedBook.getBookName());
        presenter.setBooksFilter(selectedBook);
        this.showLoading(false);
        presenter.loadSearchResults();
    }

    @Override public void onSortOrderFilterSelected(int selection) {
        String sortOrder = SortOrder.values[selection];
        sortOrderFilter__value.setText(sortOrder);
        presenter.setSortOrder(sortOrder);
        this.showLoading(false);
        presenter.loadSearchResults();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("search_sort_order", selection);
        editor.commit();
        this.sortOrderDialog.dismiss();
    }
}
