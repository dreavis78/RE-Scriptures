package info.scriptures.rescriptures.ui.library;

public interface OnTabNavigationListener {
    public void onTabNavigation(int tabPosition);
}
