package info.scriptures.rescriptures.ui.library.book;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import info.scriptures.rescriptures.BasePresenter;
import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.dao.BookWithNumChaptersDao;
import info.scriptures.rescriptures.ui.library.LibraryContract;

import static com.google.common.base.Preconditions.checkNotNull;

public class BookTabPresenter extends BasePresenter {

    private final ScriptureRepository repository;

    private final LibraryContract.View libraryView;

    private final List<BookWithNumChaptersDao.BookWithNumChapters> entities;

    public BookTabPresenter(@NonNull ScriptureRepository repository, @NonNull LibraryContract.View bookView) {

        this.repository = repository;
        setRepository(this.repository);
        
        libraryView = checkNotNull(bookView, "LibraryView cannot be null");

        entities = new ArrayList<BookWithNumChaptersDao.BookWithNumChapters>();
    }

    @Override
    public void updateView() {
        String selectedVolume = repository.getSelectedVolume();
        LiveData<List<BookWithNumChaptersDao.BookWithNumChapters>> books = repository.getBooksByVolume(selectedVolume);
        books.observe(libraryView, new Observer<List<BookWithNumChaptersDao.BookWithNumChapters>>() {
            @Override
            public void onChanged(@Nullable List<BookWithNumChaptersDao.BookWithNumChapters> books) {
                entities.clear();
                for(BookWithNumChaptersDao.BookWithNumChapters b : books) {
                    entities.add(b);
                }
                libraryView.showLibraryItems(entities);
            }
        });
    }

    public BookWithNumChaptersDao.BookWithNumChapters getBookEntity(String bookId) {
        BookWithNumChaptersDao.BookWithNumChapters entity;

        for(BookWithNumChaptersDao.BookWithNumChapters ent : entities) {
            if (ent.book_id.equals(bookId)) {
                return ent;
            }
        }

        return null;
    }

    public void setBook(String bookId) {
        repository.setSelectedBook(bookId);
        repository.setSelectedChapter(null);
        repository.setSelectedParagraph(0);
    }
}
