package info.scriptures.rescriptures.ui.library.volume;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import info.scriptures.rescriptures.BasePresenter;
import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.entity.VolumeEntity;
import info.scriptures.rescriptures.ui.library.LibraryContract;

import static com.google.common.base.Preconditions.checkNotNull;

//public class VolumeTabPresenter extends BasePresenter<ScriptureRepository, VolumeFragment> implements LifecycleObserver {
public class VolumeTabPresenter extends BasePresenter<ScriptureRepository, VolumeFragment>  {

    private final ScriptureRepository repository;

    private final LibraryContract.View libraryView;

    private final List<VolumeEntity> entities;

    public VolumeTabPresenter(@NonNull ScriptureRepository repository, @NonNull LibraryContract.View volumeView) {
        this.repository = repository;
        setRepository(this.repository);

        libraryView = checkNotNull(volumeView, "volumeView cannot be null");

        entities = new ArrayList<>();
    }

    @Override
    protected void updateView() {
        LiveData<List<VolumeEntity>> volumes = repository.getVolumes();
        volumes.observe(libraryView, volumes1 -> {
            entities.clear();
            for(VolumeEntity v : volumes1) {
                entities.add(v);
            }
            libraryView.showLibraryItems(entities);
        });
    }

    public void setVolume(String volumeId) {
        repository.setSelectedVolume(volumeId);

        // Reset selected book & chapter
        LiveData<String> bookLiveData = repository.getFirstBookByVolume(volumeId);
        bookLiveData.observe(libraryView, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String bookId) {
                repository.setSelectedBook(bookId);
                repository.setSelectedChapter(null);
                repository.setSelectedParagraph(0);
            }
        });
    }
}
