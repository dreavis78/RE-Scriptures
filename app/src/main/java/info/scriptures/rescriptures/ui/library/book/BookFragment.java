package info.scriptures.rescriptures.ui.library.book;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import info.scriptures.rescriptures.R;
import info.scriptures.rescriptures.data.LibraryItemRepository;
import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.dao.BookWithNumChaptersDao;
import info.scriptures.rescriptures.data.entity.BookEntity;
import info.scriptures.rescriptures.ui.library.ClickListener;
import info.scriptures.rescriptures.ui.library.LibraryContract;
import info.scriptures.rescriptures.ui.library.LibraryListAdapter;
import info.scriptures.rescriptures.ui.library.OnNavigateToReaderListener;
import info.scriptures.rescriptures.ui.library.OnTabNavigationListener;

import static com.google.common.base.Preconditions.checkNotNull;

public class BookFragment extends Fragment implements LibraryContract.View<BookTabPresenter>{

    private BookTabPresenter presenter;

    private View mNoContentView;

    private List<BookEntity> books;

    private RecyclerView recyclerView;

    private LibraryListAdapter adapter;

    private ScriptureRepository repository;

    private OnTabNavigationListener onTabNavigationListener;

    private OnNavigateToReaderListener onNavigateToReaderListener;

    private OnNavigateToReaderListener navigateListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        books = new ArrayList<BookEntity>();
        repository = ScriptureRepository.getInstance(this.getActivity().getApplication());
        presenter = new BookTabPresenter(repository, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.bindView(this);

        try {
            onTabNavigationListener = (OnTabNavigationListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnTabNavigationListener");
        }

        try {
            onNavigateToReaderListener = (OnNavigateToReaderListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnTabNavigationListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.unbindView();
    }

    /* Called when fragment becomes visible */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && presenter != null) {
            presenter.updateView();
        }
    }

    @Override
    public void setLibraryItems(List books) {
        for(BookEntity book: (List<BookEntity>) books) {
            this.books.add(book);
        }
    }

    @Override
    public void showLibraryItems(List items) {
        String selectedBook = repository.getSelectedBook();
        List<LibraryItemRepository> repositories = new ArrayList<>();
        for(BookWithNumChaptersDao.BookWithNumChapters book: (List<BookWithNumChaptersDao.BookWithNumChapters>) items) {
            Boolean isSelectedBook = book.book_id.equals(selectedBook);
            LibraryItemRepository repository = new LibraryItemRepository(book.book_id, book.name, isSelectedBook);
            repositories.add(repository);
        }
        adapter.addItems(repositories);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_library, container, false);

        // Set up library view
        recyclerView = root.findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new LibraryListAdapter(tag -> {
            // callback performed on click
            presenter.setBook(tag);
            navigate(tag);
        });
        recyclerView.setAdapter(adapter);

        setHasOptionsMenu(true);

        return root;
    }

    private void navigate(String bookId) {
        BookWithNumChaptersDao.BookWithNumChapters entity = presenter.getBookEntity(bookId);
        if (entity != null) {
            if (entity.num_chapters == 1) {
                onNavigateToReaderListener.onNavigateToReader();
            } else {
                onTabNavigationListener.onTabNavigation(2);
                adapter.notifyDataSetChanged();
            }
        }

    }
}
