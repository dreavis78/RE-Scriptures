package info.scriptures.rescriptures.ui.library.volume;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.scriptures.rescriptures.R;
import info.scriptures.rescriptures.data.LibraryItemRepository;
import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.entity.VolumeEntity;
import info.scriptures.rescriptures.ui.events.ColorSchemeChangedEvent;
import info.scriptures.rescriptures.ui.library.ClickListener;
import info.scriptures.rescriptures.ui.library.LibraryContract;
import info.scriptures.rescriptures.ui.library.LibraryListAdapter;
import info.scriptures.rescriptures.ui.library.OnTabNavigationListener;
import info.scriptures.rescriptures.util.ReadingSettingsService;
import timber.log.Timber;

public class VolumeFragment extends Fragment implements LibraryContract.View<VolumeTabPresenter> {
    private VolumeTabPresenter presenter;

    private View mNoContentView;

    private List<VolumeEntity> volumes;

    private RecyclerView recyclerView;

    private LibraryListAdapter adapter;

    private ScriptureRepository repository;

    private OnTabNavigationListener listener;

    private EventBus eventBus;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        volumes = new ArrayList<VolumeEntity>();
        repository = ScriptureRepository.getInstance(this.getActivity().getApplication());
        presenter = new VolumeTabPresenter(repository, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_library, container, false);

        // Set up library view
        recyclerView = root.findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new LibraryListAdapter(tag -> {
            // callback performed on click
            presenter.setVolume(tag);
            listener.onTabNavigation(1);
            adapter.notifyDataSetChanged();
        });
        recyclerView.setAdapter(adapter);

        setHasOptionsMenu(true);

        return root;
    }

    // View Settings Event Handling
    @Override public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

//    @Override public void onRestart() {
//        Timber.i("onRestart");
//        super.onRestart();
//
//        Resources res = getResources();
//
//        // Update font
//        String newFont = getFont();
//        if (!newFont.equals(this.font)) {
//            this.font = newFont;
//            onFontChangedEvent(new FontChangedEvent(this.font));
//        }
//
//        // Update text size
//        int newTextSize = getTextSize();
//        if (newTextSize != this.textSize) {
//            this.textSize = newTextSize;
//            onTextSizeChangedEvent(new TextSizeChangedEvent(this.textSize));
//        }
//
//        // Update color scheme
//        String newColorScheme = getColorScheme();
//        if (!newColorScheme.equals(this.colorScheme)) {
//            this.colorScheme = newColorScheme;
//            onColorSchemeChangedEvent(new ColorSchemeChangedEvent(this.colorScheme));
//        }
//    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.bindView(this);

        try {
            listener = (OnTabNavigationListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnTabNavigationListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.unbindView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onColorSchemeChangedEvent(ColorSchemeChangedEvent event) {
        Timber.i("Reader Activity color scheme changed: " + event.getColorScheme());
        String colorScheme = event.getColorScheme();
        Resources res = getResources();

        // Change Activity Bar colors
//        ThemeUtil.changeToTheme(this, colorScheme);

        if (colorScheme.equals(res.getString(R.string.color_scheme_dark))) {
//            toolbar.setBackgroundColor(res.getColor(R.color.darkSchemeToolbar));
//            toolbar.setTitleTextColor(res.getColor(R.color.darkSchemeText));
//            tabLayout.setBackgroundColor(res.getColor(R.color.darkSchemeTabs));
            recyclerView.setBackgroundColor(res.getColor(R.color.darkSchemeBackground));

        } else if (colorScheme.equals(res.getString(R.string.color_scheme_light))) {
//            toolbar.setBackgroundColor(res.getColor(R.color.lightSchemeToolbar));
//            toolbar.setTitleTextColor(res.getColor(R.color.lightSchemeText));

            recyclerView.setBackgroundColor(res.getColor(R.color.lightSchemeBackground));
        } else {
//            toolbar.setBackgroundColor(res.getColor(R.color.sepiaSchemeToolbar));
//            toolbar.setTitleTextColor(res.getColor(R.color.sepiaSchemeText));

            recyclerView.setBackgroundColor(res.getColor(R.color.sepiaSchemeBackground));
        }

        View v;
//        HtmlTextView htv;
//        for (int i = 0; i < childCount; i++) {
//            v = contentView.getChildAt(i);
//            if (v instanceof HtmlTextView) {
//                // Set font size (in SP units)
//                htv = (HtmlTextView) v;
//                if (colorScheme.equals(res.getString(R.string.color_scheme_dark))) {
//                    htv.setTextColor(getResources().getColor(R.color.darkSchemeText));
//                } else if (colorScheme.equals(res.getString(R.string.color_scheme_light))) {
//                    htv.setTextColor(getResources().getColor(R.color.lightSchemeText));
//                } else {
//                    htv.setTextColor(getResources().getColor(R.color.sepiaSchemeText));
//                }
//            }
//        }
    }

    /* Called when fragment becomes visible */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && presenter != null) {
            presenter.updateView();
        }
    }

    @Override
    public void setLibraryItems(List volumes) {
        for(VolumeEntity volume : (List<VolumeEntity>) volumes) {
            this.volumes.add(volume);
        }
    }

    @Override
    public void showLibraryItems(List items) {
        String selectedVolume = repository.getSelectedVolume();
        List<LibraryItemRepository> repositories = new ArrayList<>();
        for(VolumeEntity volume: (List<VolumeEntity>) items) {
            Boolean isSelectedVolume = volume.getId().equals(selectedVolume);
            LibraryItemRepository  repository = new LibraryItemRepository(volume.getId(), volume.getName(), isSelectedVolume);
            repositories.add(repository);
        }
        adapter.addItems(repositories);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }
}
