package info.scriptures.rescriptures.ui.reader;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

public interface ReaderView extends MvpLceView<ReaderEntity>, LifecycleOwner {
    @NonNull @Override Lifecycle getLifecycle();
}
