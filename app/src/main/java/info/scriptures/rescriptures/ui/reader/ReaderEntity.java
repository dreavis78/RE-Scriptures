package info.scriptures.rescriptures.ui.reader;

public class ReaderEntity {

    private String volumeId;

    private String bookId;

    private String chapter;

    private int position;

    private String content;

    public ReaderEntity() {}

    public ReaderEntity( String volumeId, String bookId, String chapter ) {
        this.volumeId = volumeId;
        this.bookId = bookId;
        this.chapter = chapter;
    }

    public String getVolumeId() {
        return volumeId;
    }

    public void setVolumeId(String volumeId) {
        this.volumeId = volumeId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
