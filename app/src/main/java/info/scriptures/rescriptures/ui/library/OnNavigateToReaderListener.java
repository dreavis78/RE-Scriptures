package info.scriptures.rescriptures.ui.library;

public interface OnNavigateToReaderListener {
    public void onNavigateToReader();
}
