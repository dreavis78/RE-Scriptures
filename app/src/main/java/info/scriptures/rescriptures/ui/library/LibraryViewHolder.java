package info.scriptures.rescriptures.ui.library;

import android.graphics.Color;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import info.scriptures.rescriptures.BaseListPresenter;
import info.scriptures.rescriptures.R;
import timber.log.Timber;

public class LibraryViewHolder<P extends BaseListPresenter> extends RecyclerView.ViewHolder implements LibraryItemContract.View, View.OnClickListener {
    protected P presenter;

    private final TextView textView;

    private final WeakReference<ClickListener> listenerRef;

    LibraryViewHolder(
            View itemView,
            final ClickListener listener
    ) {
        super(itemView);
        Timber.tag("LibraryViewHolder");

        textView = itemView.findViewById(R.id.library_item);

        listenerRef = new WeakReference<>(listener);
        textView.setOnClickListener(this);
    }

    // onClick Listener for textView
    @Override
    public void onClick(View v) {
        listenerRef.get().onClicked((String) v.getTag());
    }

    @Override
    public void setItemId(String itemId) {
        textView.setTag(itemId);
    }

    @Override
    public void setItemName(String itemName, Boolean selected) {
        if (selected) {
            SpannableString sString = new SpannableString(itemName);
            sString.setSpan(new StyleSpan(Typeface.BOLD), 0, sString.length(), 0);
            textView.setText(sString);
        } else {
            textView.setText(itemName);
        }
    }

    @Override
    public void onClicked(String item) { }

    void bindPresenter(P presenter) {
        this.presenter = presenter;
        presenter.bindView(this);
    }

    void unbindPresenter() {
        presenter = null;
    }
}
