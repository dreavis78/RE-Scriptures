package info.scriptures.rescriptures.ui.search;

import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import info.scriptures.rescriptures.data.entity.ParagraphEntity;

public interface SearchContract {

    interface View<SearchPresenter> extends LifecycleOwner {

        void showSearchResults(List<ParagraphEntity> searchResults, Float textSize);
    }

    interface Presenter {

        void loadSearchResults(String query);
    }
}
