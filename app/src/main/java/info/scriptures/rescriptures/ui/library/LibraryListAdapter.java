package info.scriptures.rescriptures.ui.library;

import androidx.annotation.NonNull;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import info.scriptures.rescriptures.R;
import info.scriptures.rescriptures.data.LibraryItemRepository;
import timber.log.Timber;

public class LibraryListAdapter extends LibraryRecyclerAdapter<LibraryItemRepository, LibraryListPresenter, LibraryViewHolder<LibraryListPresenter>> {
    private final List<LibraryItemRepository> models;

    private final ClickListener listener;

    public LibraryListAdapter(ClickListener listener) {
        Timber.tag("LibraryListAdapter");
        models = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public LibraryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LibraryViewHolder(
            LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_library_item, parent, false),
            listener
        );
    }

    @NonNull
    @Override
    public LibraryListPresenter createPresenter(@NonNull LibraryItemRepository repository) {
        LibraryListPresenter presenter = new LibraryListPresenter();
        presenter.setRepository(repository);
        return presenter;
    }

    @NonNull
    @Override
    protected Object getModelId(@NonNull LibraryItemRepository repository) {
        return repository.getId();
    }


    public void addItems(Collection<LibraryItemRepository> data) {
        models.clear();
        presenters.clear();

        for (LibraryItemRepository item: data) {
            addInternal(item);
        }

        notifyDataSetChanged();
    }

    private void addInternal(LibraryItemRepository item) {
        models.add(item);
        presenters.put(getModelId(item), createPresenter(item));
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    protected LibraryItemRepository getItem(int position) {
        return models.get(position);
    }
}
