package info.scriptures.rescriptures.ui.library.chapter;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import info.scriptures.rescriptures.BasePresenter;
import info.scriptures.rescriptures.data.ScriptureRepository;
import info.scriptures.rescriptures.data.entity.ChapterEntity;
import info.scriptures.rescriptures.ui.library.LibraryContract;

import static com.google.common.base.Preconditions.checkNotNull;

//public class ChapterTabPresenter extends BasePresenter implements LifecycleObserver {
public class ChapterTabPresenter extends BasePresenter {

    private final ScriptureRepository repository;

    private final LibraryContract.View libraryView;

    private final List<ChapterEntity> entities;

    public ChapterTabPresenter(@NonNull ScriptureRepository repository, @NonNull LibraryContract.View chapterView) {
        this.repository = repository;
        setRepository(this.repository);

        libraryView = checkNotNull(chapterView, "chapterView cannot be null");

        entities = new ArrayList<>();
    }

    @Override
    public void updateView() {
        String selectedBook = repository.getSelectedBook();
        LiveData<List<ChapterEntity>> chapters = repository.getChaptersByBook(selectedBook);
        chapters.observe(libraryView, new Observer<List<ChapterEntity>>() {
            @Override
            public void onChanged(@Nullable List<ChapterEntity> chapters) {
                    entities.clear();
                    for(ChapterEntity c : chapters) {
                        entities.add(c);
                    }
                    libraryView.showLibraryItems(entities);
            }
        });
    }

    public void setChapter(String chapter) {
        repository.setSelectedChapter(chapter);
        repository.setSelectedParagraph(0);
    }
}
