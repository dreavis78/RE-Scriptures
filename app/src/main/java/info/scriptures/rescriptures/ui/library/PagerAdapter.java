package info.scriptures.rescriptures.ui.library;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import info.scriptures.rescriptures.BasePresenter;
import info.scriptures.rescriptures.ui.library.book.BookFragment;
import info.scriptures.rescriptures.ui.library.chapter.ChapterFragment;
import info.scriptures.rescriptures.ui.library.volume.VolumeFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {

    private FragmentManager fm;

    private BasePresenter mPresenter;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
        this.fm = fm;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new VolumeFragment();
            case 1:
                return new BookFragment();
            case 2:
                return new ChapterFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
