package info.scriptures.rescriptures.ui.search.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class BookFilterDialogFragment extends DialogFragment {

    public interface BookFilterDialogListener {
        void onBookFilterSelected(int selection);
    }

    BookFilterDialogListener listener;
    String[] filteredBooks;

    @Override public void setArguments(Bundle savedInstanceBundle) {
        filteredBooks = (String[]) savedInstanceBundle.get("books");
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (BookFilterDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement BookFilterDialogListener");
        }
    }

    @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(filteredBooks, (dialogInterface, i) -> listener.onBookFilterSelected(i));

        return builder.create();
    }
}
