package info.scriptures.rescriptures.ui.search.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import info.scriptures.rescriptures.R;
import timber.log.Timber;

public class SortOrderFilterDialogFragment extends DialogFragment {

    public interface SortOrderDialogListener {
        void onSortOrderFilterSelected(int selection);
    }

    SortOrderDialogListener listener;

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (SortOrderDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement SortOrderDialogListener");
        }
    }

    @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        int sort_order = getArguments().getInt("search_sort_order");
        builder.setSingleChoiceItems(R.array.sort_order_array, sort_order, (dialogInterface, i) -> listener.onSortOrderFilterSelected(i));

        return builder.create();
    }
}
