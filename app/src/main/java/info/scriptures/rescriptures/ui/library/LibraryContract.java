package info.scriptures.rescriptures.ui.library;

import androidx.lifecycle.LifecycleOwner;

import java.util.List;

import info.scriptures.rescriptures.BasePresenter;

public interface LibraryContract {

    interface View<P extends BasePresenter> extends LifecycleOwner {

//        void setAdapter(LibraryRecyclerAdapter adapter);

        void setLibraryItems(List items);

        void showLibraryItems(List items);

//        void goToSection(String tag);
    }

//    interface Presenter extends BasePresenter {

        // Lifecycle events method
//        void onStart();
//
//        void onLibraryItemClicked();
//    }
}
