package info.scriptures.rescriptures.ui.events;

public class ColorSchemeChangedEvent {

    private final String colorScheme;

    public ColorSchemeChangedEvent(String colorScheme) {
        this.colorScheme = colorScheme;
    }

    public String getColorScheme() { return colorScheme; }
}
