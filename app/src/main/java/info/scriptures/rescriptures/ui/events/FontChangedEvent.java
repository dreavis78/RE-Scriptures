package info.scriptures.rescriptures.ui.events;

public class FontChangedEvent {

    private final String font;

    public FontChangedEvent(String font) {
        this.font = font;
    }

    public String getFont() {
        return font;
    }
}
