package info.scriptures.rescriptures.ui.events;

public class LineSpacingChangedEvent {

    public static final int DECREASE = 0;
    public static final int INCREASE = 1;
    public static final float STEP = 0.4f;
    public static final float MIN_LINE_SPACING = 0.0f;
    public static final float MAX_LINE_SPACING = 2.0f;
    
    private final float lineSpacing;
    
    public LineSpacingChangedEvent(float lineSpacing) {
        this.lineSpacing = lineSpacing;
//        if(direction == DECREASE &&  lineSpacing != LineSpacingChangedEvent.MIN_LINE_SPACING) {
//            this.lineSpacing = lineSpacing - STEP;
//        } else if(direction == INCREASE && lineSpacing != LineSpacingChangedEvent.MAX_LINE_SPACING) {
//            this.lineSpacing = lineSpacing + STEP;
//        } else {
//            this.lineSpacing = lineSpacing;
//        }
    }

    public float getLineSpacing() { return lineSpacing; }
}
