package info.scriptures.rescriptures;

import androidx.annotation.NonNull;

import java.lang.ref.WeakReference;

public abstract class BaseListPresenter<M, V> {
   protected M model;

   private WeakReference<V> view;

   public void setRepository(M model) {
      this.model = model;
      if (setupDone()) {
         updateView();
      }
   }

   public void bindView(@NonNull V view) {
      this.view = new WeakReference<>(view);
      if (setupDone()) {
         updateView();
      }
   }

   public void unbindView() {
      this.view = null;
   }

   protected V view() {
      if (view == null) {
         return null;
      } else {
         return view.get();
      }
   }

   protected abstract void updateView();

   protected boolean setupDone() {
      return view() != null && model != null;
   }

   public abstract void onClicked(String id);

}
