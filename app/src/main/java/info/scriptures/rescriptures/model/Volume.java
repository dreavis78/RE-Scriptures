package info.scriptures.rescriptures.model;

public interface Volume {

    String getId();

    String getName();

    int getOrder();

}
