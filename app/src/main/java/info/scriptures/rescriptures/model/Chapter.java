package info.scriptures.rescriptures.model;

import info.scriptures.rescriptures.data.entity.LibraryEntity;

public interface Chapter {

    String getVolumeId();

    String getBookId();

    String getChapter();

    String getContent();
}
