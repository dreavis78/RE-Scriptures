package info.scriptures.rescriptures.model;

public interface Paragraph {

    String getVolumeId();

    String getBookId();

    String getChapter();

    Integer getPosition();

    String getParagraph();
}
