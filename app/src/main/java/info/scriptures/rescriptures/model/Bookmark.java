package info.scriptures.rescriptures.model;

public interface Bookmark {
    int getId();
    String getName();
    String getVolumeId();
    String getBookId();
    String getChapter();
    int getParagraph();
}
