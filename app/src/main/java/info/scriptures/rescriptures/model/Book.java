package info.scriptures.rescriptures.model;

public interface Book {

    String getVolumeId();

    String getBookId();

    String getBookName();

    int getBookOrder();
}
