package info.scriptures.rescriptures.model;

public interface ChapterIntro {
    String getVolumeId();
    String getBookId();
    int getChapterId();
    int getPosition();
    String getIntro();
}
