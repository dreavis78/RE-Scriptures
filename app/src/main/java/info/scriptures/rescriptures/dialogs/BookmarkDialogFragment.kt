package info.scriptures.rescriptures.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import info.scriptures.rescriptures.R
import info.scriptures.rescriptures.data.entity.BookmarkEntity
import info.scriptures.rescriptures.util.ReadingSettingsService
import info.scriptures.rescriptures.util.ThemeUtil
import info.scriptures.rescriptures.viewmodel.BookmarkViewModel
import java.lang.ClassCastException

class BookmarkDialogFragment : DialogFragment() {
    private var bookmarkModel: BookmarkViewModel? = null
    private var colorTheme: String = ReadingSettingsService.getColorScheme()
    private var themeId: Int = 0
    private var builder: AlertDialog.Builder? = null
    private var bookmarks: List<BookmarkEntity>? = emptyList()
    private lateinit var alertDialog: AlertDialog

    interface BookmarkDialogListener {
        fun onBookmarkSelected(reference: BookmarkEntity?)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        when(colorTheme) {
            "dark" -> themeId = R.style.darkAlertDialogTheme
            "light" -> themeId = R.style.baseAlertDialogStyle
            "sepia" -> themeId = R.style.baseAlertDialogStyle
        }

        builder = AlertDialog.Builder(requireActivity(), themeId)

        bookmarkModel = ViewModelProviders.of(requireActivity()).get(BookmarkViewModel::class.java)
        bookmarks = bookmarkModel?.bookmarks?.value as List<BookmarkEntity>
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val listener: BookmarkDialogListener
        try {
            listener = context as BookmarkDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$activity must implement BookmarkDialogListener")
        }

        builder?.setTitle("Bookmarks")
        val bookmarkItems: MutableList<BookmarkAdapterItem?> = ArrayList()

        if (bookmarks?.isNullOrEmpty() != true) {
            bookmarks?.forEach {
                val bm: BookmarkEntity? = it
                val item = BookmarkAdapterItem(
                        bm?.id ?: -1,
                        bm?.name ?: ""
                )
                bookmarkItems.add(item)
            }

            val adapter = context?.let { BookmarkArrayAdapter(it, 0, bookmarkItems) }

            with(builder) {
                this?.setAdapter(adapter) { _, i: Int ->
                    bookmarkModel?.bookmarks?.value?.get(i).let(listener::onBookmarkSelected)
                }
            }
        } else {
            builder?.setMessage("No bookmarks")
        }

         alertDialog = (builder!!.create())
        return alertDialog
    }

    inner class BookmarkArrayAdapter(
            context: Context,
            resource: Int,
            private val bookmarks: MutableList<BookmarkAdapterItem?>
    ) : ArrayAdapter<BookmarkAdapterItem>( context, resource, bookmarks ) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val listItem: View =
                    convertView ?:
                    LayoutInflater.from(context).inflate(R.layout.bookmark_item, parent,false)

            val bm: BookmarkAdapterItem? = bookmarks[position]
            val textView: TextView? = listItem.findViewById(R.id.bookmark_name)
            textView?.text = bm?.name

            val button: ImageView? = listItem.findViewById(R.id.remove_icon)
            button?.isSelected = when {
                (colorTheme == ThemeUtil.THEME_DARK) -> true
                else -> false
            }

            val removeIcon: ImageView? = listItem.findViewById(R.id.remove_icon)
            removeIcon?.tag = bm?.id
            removeIcon?.setOnClickListener { _ ->
                bookmarks.remove(bm)
                bookmarkModel?.removeBookmark(removeIcon.tag as Int)
                parent.removeViewInLayout(listItem)
                notifyDataSetChanged()

                if (bookmarks.size == 0) {
                    alertDialog.dismiss()
                }

                Toast.makeText(context, "Bookmark '" + bm?.name + "' removed", Toast.LENGTH_SHORT).show()
            }

            return listItem
        }
    }

}
