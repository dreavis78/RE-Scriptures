package info.scriptures.rescriptures.dialogs

data class BookmarkAdapterItem(val id: Int, val name: String)