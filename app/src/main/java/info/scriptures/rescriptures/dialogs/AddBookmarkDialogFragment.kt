package info.scriptures.rescriptures.dialogs

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import info.scriptures.rescriptures.R
import info.scriptures.rescriptures.util.ReadingSettingsService
import kotlinx.android.synthetic.main.dialog_add_bookmark.view.*
import timber.log.Timber
import java.lang.ClassCastException

class AddBookmarkDialogFragment: DialogFragment() {
    private lateinit var layout: View
    private var themeId: Int = 0
    private lateinit var builder: AlertDialog.Builder
    private var reference: String? = null
    private lateinit var imm: InputMethodManager

    companion object {
        @JvmStatic
        fun newInstance(reference: String) = AddBookmarkDialogFragment().apply {
            arguments = Bundle().apply {
                putString("reference", reference)
            }
        }
    }

    interface AddBookmarkDialogListener {
        fun onBookmarkAdded(name: String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        when(ReadingSettingsService.getColorScheme()) {
            "dark" -> themeId = R.style.darkAlertDialogTheme
            "light" -> themeId = R.style.baseAlertDialogStyle
            "sepia" -> themeId = R.style.baseAlertDialogStyle
        }

        builder = AlertDialog.Builder(requireActivity(), themeId)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        layout = View.inflate(requireContext(), R.layout.dialog_add_bookmark, null)

        val listener: AddBookmarkDialogListener
        try {
            listener = context as AddBookmarkDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$activity must implement AddBookmarkDialogListener")
        }

        arguments?.getString("reference")?.let {
            reference = it
        }

        builder.setView(layout)

        val et: EditText = layout.add_bookmark_input
        et.maxLines = 1
        et.setText(reference)
        et.selectAll()

        imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)

        builder.setCancelable(false)
        builder.setPositiveButton("OK") { _, _ ->
            listener.onBookmarkAdded(et.text.toString())
        }
        builder.setNegativeButton("Cancel") { _, _ -> }

        return builder.create()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }
}