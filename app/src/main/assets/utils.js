$(() => {
    var debounce = null;
    $(window).scroll(() => {
        clearTimeout(debounce);
        debounce = setTimeout(() => {
            handler.updateParagraph(parseInt(getParagraphId(), 10));
        }, 300);
    });
});

function getParagraphId() {
    if (($('body').scrollTop() + 60) < ($('p,li')[0].offsetTop)) return 0;

    var e = findTopVisibleParagraph();
    var id = e.id;
    if (!id) {
        id = $('ol p').index(e) + 1;
    }
    return id;
}

function findTopVisibleParagraph() {
    let elements = $('li,p');
    for (let e of elements) {
       if(isInViewport(e)) {
            return e;
       }
    };
}

function isInViewport(elem) {
    var bounding = elem.getBoundingClientRect(),
        windowHeight = window.innerHeight;

    return (bounding.top <= windowHeight && bounding.bottom >= 60);
}

function scrollToParagraph(paragraphId) {
    $(() => {
        setTimeout(() => {
            let body = $('body'),
                paragraph = $('#' + paragraphId)[0];

            if (!paragraph) {
                paragraph = $('p')[paragraphId - 1];
            }
            $('body').animate({
                scrollTop: paragraph.offsetTop - 60
            }, 1000);
        }, 500);
    });
}

function finish() {
    document.getElementsByTagName('body').item(0).style.display = '';
}
