Read and search the complete text of the Restoration Edition of the 
scriptures. Quickly locate scripture content by volume, book, and 
chapter. The full text of the scriptures is included for offline reading.

The following volumes of Restoration scriptures are included:

• Volume 1: Old Covenants (containing the Old Testament)

• Volume 2: New Covenants (contains the New Testament and Book of Mormon)

• Volume 3: Teachings & Commandments (containing the full Joseph Smith 
History, the Lectures on Faith, the Book of Abraham, the Testimony 
of St. John, as well as history, revelations, letters, talks and 
documents of value since the beginning of the Restoration of the Gospel.) 
This volume also includes an Appendix with a Glossary of Terms (offered 
as helpful inspired commentary on different terms used throughout the 
text), maps, and other helpful items.

In addition, this app allows quick keyword searches in the complete text. 
Search for single or multiple words that all appear in a paragraph. 
Alternatively, search for exact phrases by surrounding the search terms 
with quotes. (For example, "the holy church of God" or "Laman & Lemuel".) 
Then, click on a search result to be taken to the exact chapter and 
paragraph where it appears.

The Restoration Edition text represents thousands of hours of careful 
work by a committee of volunteers. Their purpose is to compile the most 
accurate and complete set of scriptures based on the restoration of the 
gospel of Jesus Christ that began in the early 1800's through Joseph 
Smith, Jr.

The committee responsible for this compilation of scriptures, and the 
developer of this app, have done this work as an expression of faith in 
Jesus Christ, and not as representing any church or organization. The 
purpose of this app is to serve the loose association of fellowships of 
believers in Jesus Christ and all those seeking the welfare of Zion. 
More information about the beliefs of those involved in this work can 
be found at the following websites:

• http://scriptures.info/,
• https://www.zionsreturn.org/, and
• https://www.restorationarchives.com/.

This version of the app contains the text available as of September 2018. 
Later versions of the app will update the text as it is updated at 
http://scriptures.info. 

#### Update (April 2019)

The latest update includes the final text released by the Scripture
Committee as of April 2019. Filters for keyword 
searches, and display settings are also now available (see the 
CHANGELOG.md for details).
