Change Log
==========

## 1.0

- Full text of Restoration Scriptures (as of September 2018)
- Includes the Appendix (Glossary of Terms, Maps, etc.)
- Quickly navigate between work, book, and chapter
- Basic keyword searches

## 1.1

- Official logo & splash screen
- Final text of Restoration Scriptures (as of April 2019)
- Highlighting & copying of text is now supported
- Filter keyword searches by work and book
- Sort keyword searches by relevance or traditional order
- Keyword searches are limited to 50 results for performance reasons. 
A "More..." button will appear at the bottom of the result set for 
loading the next 50 results.
- Floating next/previous chapter buttons replaced with a bottom
navigation bar.
- Top and bottom bars slide away when scrolling down to improve the
reading experience, but reappear when scrolling up.
- Bottom nav bar also includes buttons to return to main library 
navigation, perform a keyword search, or adjust display settings.
- Settings include the following:
  - Choose between a serif and sans-serif font.
  - Choose between three color schemes (Light, Dark, & Sepia)
  - Adjust the size of the text
- Includes the text for all currently available "Denver Snuffer 
Podcast" transcripts.
